#!/usr/bin/env bash

trap 'kill -TERM $PID' TERM INT
./vhal.py &
PID=$!
wait $PID
trap - TERM INT
wait $PID
EXIT_STATUS=$?
