# encoding: utf-8
from functools import partial
import random

import gevent

from engine import Resources, shex
from engine import Puzzle
from engine import DummyDevice, StekloliftDevice, KluchDevice, ZvezdyDevice
from engine import PutDevice

from common import Svet, reverse_profile
from room1 import ProemLatch

import logging
log = logging.getLogger('[rm2]')
log.addHandler(logging.NullHandler())


class Vitrag(Puzzle):
    glossary_name = u"Витраж"
    glossary_code = u"G1R2O2-{0}"

    def __init__(self, number, device_addr, *args, **kwargs):
        super(Vitrag, self).__init__(*args, **kwargs)
        self.glossary_code = self.glossary_code.format(number)
        self.device_addr = device_addr
        self.number = number

        self.port_main = kwargs.get('main')
        self.port_part = kwargs.get('part')
        self.main_level = 60

        self.scheduler = Resources().require('scheduler')

    def on_start(self):
        self.svet = Svet.start(self.device_addr).proxy()
        self.svet.subscribe('fail', [
            lambda data: self.PUB.log(u"""
Девайс c адресом {0} ({1}) провалился.
В принципе для решение задачи Человек достаточно двух витражей, поэтому если сломалось не больше двух витражей игру можно продолжить, главное скажите, что некоторые витражи не работают.
Положение конечностей Человека, которые изображены на неактивных Витражах можно просто подобрать, но для этого нужно правильно выставить хотя бы две конечности использую подсказки на витражах и Лиру.
""".format(self.device_addr, unicode(self)), log_level='error'
),
            #lambda data: self.proxy().trigger('fail', data)
        ])


    def init(self):
        self.svet.init().get()

    def setup(self):
        self.svet.setup()
        self.main(0)
        self.part(0)

    def main(self, output, time=0, alg=None):
        main = self.port_main
        self.svet.set_channel(main, output, time, alg)
        self.light()

    def part(self, output, time=0, alg=None):
        part = self.port_part
        self.svet.set_channel(part, output, time, alg)
        self.light()

    def part_off(self):
        self.part(0, 10)

    def light(self):
        self.svet.shim().get()

    def main_light(self):
        self.main(self.main_level, 50)

    def main_off(self):
        self.main(0, 50)

    def akkord(self):
        self.part(100, 10)
        #self.scheduler.schedule(
            #lambda: self.part(0, 10),
            #1100,
            #once=True
        #)

    def wrong_akkord(self):
        self.main(self.main_level - 20, 10)
        self.scheduler.schedule(
            lambda: self.main(self.main_level, 10),
            1100,
            once=True
        )

    def stopall(self):
        self.svet.stopall().get()

class KluchOtTanka(Puzzle):
    glossary_name = u"Ключ от танка"
    glossary_code = u"G1R2O5"

    def __init__(self, device_addr, *args, **kwargs):
        super(KluchOtTanka, self).__init__(*args, **kwargs)
        self.device_addr = device_addr

    def on_start(self):
        self.device = KluchDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])


    def init(self):
        self.device.init().get()

    def setup(self):
        self.channels = [False]*4
        periods = self.device.read_regs('stat.ch1_period', 4).get()
        #print periods
        self.channels = map(lambda period: period > 0, periods)
        self.device.svet(False)

    def svet(self, svet=True):
        self.device.svet(svet)

    def activate(self):
        self.device.subscribe(
            'stat.ch1_period',
            lambda data: self.proxy().channel(1, data[0]),
            500
        ).get()
        self.device.subscribe(
            'stat.ch2_period',
            lambda data: self.proxy().channel(2, data[0]),
            500
        ).get()
        self.device.subscribe(
            'stat.ch3_period',
            lambda data: self.proxy().channel(3, data[0]),
            500
        ).get()
        self.device.subscribe(
            'stat.ch4_period',
            lambda data: self.proxy().channel(4, data[0]),
            500
        ).get()

    def channel(self, number, period):
        #print "Number: {0} Period: {1}".format(number, period)
        if period != 0:
            self.channels[number-1] = True
        else:
            self.channels[number-1] = False

        self.all_ch_status()

    def all_ch_status(self):
        kluch_count = len(filter(None, self.channels))
        #print "Kluch count: {0}".format(kluch_count)
        if kluch_count == 4:
            self.svet()
        self.trigger('kluch_{0}'.format(kluch_count))


    def deactivate(self):
        self.device.unsubscribe('stat.ch1_period').get()
        self.device.unsubscribe('stat.ch2_period').get()
        self.device.unsubscribe('stat.ch3_period').get()
        self.device.unsubscribe('stat.ch4_period').get()
        self.unsubscribe('solved')

    def stopall(self):
        self.device.stop()


class Knopka(Puzzle):
    glossary_name = u"Кнопка под столом"
    glossary_code = u""

    def __init__(self, device_addr, *args, **kwargs):
        super(Knopka, self).__init__(*args, **kwargs)
        self.device_addr = device_addr

    def on_start(self):
        self.device = DummyDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.as_input()

    def activate(self):
        self.device.subscribe(
            'stat.inputs',
            lambda data: self.proxy().pushed(data[0]),
            100
        ).get()

    def deactivate(self):
        self.device.unsubscribe('stat.inputs').get()

    def pushed(self, state):
        #print state
        if state > 0:
            self.trigger('pushed')

    def stopall(self):
        self.device.stop()


class Gorod(Puzzle):
    glossary_name = u"Город"
    glossary_code = u"G1R2O1-1"

    def __init__(self, svet_addr, mech_addr, lift_addr, *args, **kwargs):
        super(Gorod, self).__init__(*args, **kwargs)
        self.svet_addr = svet_addr
        self.mech_addr = mech_addr
        self.lift_addr = lift_addr

        self.port_mech_koleso = 0
        self.port_mech_kran = 1

    def on_start(self):
        self.device = DummyDevice.start(self.mech_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1} Механизмы) провалился.".format(
                self.mech_addr,
                unicode(self)
            )),
            #lambda data: self.proxy().trigger('fail', data)
        ])
        self.svet = Svet.start(self.svet_addr).proxy()
        self.svet.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) Подсветка провалился.".format(
                self.svet_addr,
                unicode(self)
            )),
            #lambda data: self.proxy().trigger('fail', data)
        ])
        self.lift = StekloliftDevice.start(self.lift_addr).proxy()
        self.lift.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) Лебедка провалился.".format(
                self.lift_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.lift.init().get()
        self.svet.init().get()
        self.device.init().get()

    def LIFT_UP_PROFILE(self, speed):
        return [
            shex(64000),# время действия этого сегмента
            shex(speed),# начальная скорость
            shex(speed),# конечная скорость
        ]


    def setup(self):
        self.lift.write_regs('conf.direction',
            [
                shex(1),# нарпавление движения
                shex(0),# номер профиля движения
                shex(1), # вкл датчик 1
                shex(1), # вкл датчик 2
                shex(70),# ограничение перегрузки
                shex(15000), # максимальное время движения
            ]
        )
        LIFT_DOWN_PROFILE = [
            shex(22000),# время действия этого сегмента
            shex(100),# начальная скорость
            shex(100),# конечная скорость
        ]
        self.lift.set_profile(0, self.LIFT_UP_PROFILE(10)).get()
        self.lift.set_profile(1, LIFT_DOWN_PROFILE).get()
        self.svet.setup()
        self.device.as_output().get()

    def activate(self):
        self.lift.subscribe(
            'stat.stop_reason',
            lambda data: self.proxy().stopped(data[0])
        ).get()

    def deactivate(self):
        self.lift.unsubscribe('stat.stop_reason').get()

    def stopped(self, reason):
        # при открытии под датчика остановка
        # при закрытии под перегузку по току
        # 1 - сработал датчик
        log.debug('Device stopped: {0}'.format(reason))
        if reason in set([1, 2]):
            self.trigger('solved')
        if reason > 2:
            self.trigger('stopped_not_by_sensor')
            if reason == 3: # конец профиля движения
                self.PUB.log(u"У Лебедки Города закончился профиль движения.", log_level='error')
            elif reason == 4: # перегрузка по току
                self.PUB.log(u"У Лебедки Города сработала перегрузка по току.", log_level='error')
            elif reason == 5: # перегрузка по току
                self.PUB.log(u"У Лебедки Города сработалo ограничение максимального времени.", log_level='error')
            elif reason == 6: # датчик перегрева
                self.PUB.log(u"У Лебедки Города сработал датчик перегрева.", log_level='error')

    def lift_speed(self, speed):
        if speed == 0:
            self.lift.send_cmd('stop').get()
        else:
            self.lift.set_profile(0, self.LIFT_UP_PROFILE(speed)).get()
            self.lift.send_cmd('start').get()

    def lift_up(self):
        self.lift.write_regs('conf.direction', [shex(1),]).get()
        self.lift.write_regs('conf.profile_number', [shex(0),]).get()
        self.lift.send_cmd('start').get()

    def lift_up_fast(self):
        self.lift_speed(80)
        self.lift_up()

    def lift_stop(self):
        self.lift.send_cmd('stop').get()

    def lift_down(self):
        self.lift.write_regs('conf.direction', [shex(0),]).get()
        self.lift.write_regs('conf.profile_number', [shex(1),]).get()
        self.lift.send_cmd('start').get()

    def lift_down_slow(self):
        self.lift_speed(40)
        self.lift_down()

    def svet_on(self, channels='all', brigth=100, time=20):
        if channels == 'all':
            channels = range(0,8)

        for chnl in channels:
            self.svet.set_channel(chnl, brigth, time)

        self.svet.shim().get()

    def svet_off(self, channels='all', time=20):
        if channels == 'all':
            channels = range(0,8)

        for chnl in channels:
            self.svet.set_channel(chnl, 0, time)

        self.svet.shim().get()

    def svet_animation(self):
        for chnl in range(1,8):
            brigth = random.choice([0,100])
            time = random.randrange(5,30,5)
            self.svet.set_channel(chnl, brigth, time)
        self.svet.shim().get()

    def koleso_on(self):
        self.set_one(self.port_mech_koleso)

    def koleso_off(self):
        self.set_zero(self.port_mech_koleso)

    def kran_on(self):
        self.set_one(self.port_mech_kran)

    def kran_off(self):
        self.set_zero(self.port_mech_kran)

    def set_zero(self, number):
        data = [
            shex(0),
            shex(0),
        ]
        self.device.set_port(number, data).get()

    def set_one(self, number):
        data = [
            shex(0),
            shex(1),
        ]
        self.device.set_port(number, data).get()

    def stopall(self):
        self.lift.stop()
        self.device.stop()
        self.svet.stop()


class Diafragma(Puzzle):
    glossary_name = u"Диафрагма загадки"
    glossary_code = u"G1R2O1-2-{0}"

    def __init__(self, number, device_addr, *args, **kwargs):
        super(Diafragma, self).__init__(*args, **kwargs)
        self.glossary_code = self.glossary_code.format(number)
        self.device_addr = device_addr
        self.number = number

    def on_start(self):
        self.device = StekloliftDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.write_regs('conf.direction',
            [
                shex(1),# нарпавление движения
                shex(0),# номер профиля движения
                shex(1), # вкл датчик 1
                shex(1), # вкл датчик 2
                shex(30),# ограничение перегрузки
                shex(15000), # максимальное время движения
            ]
        )

    def open(self):
        #import pudb; pu.db
        self.device.write_regs('conf.direction', [shex(0),]).get()
        self.device.send_cmd('start').get()

    def close(self):
        self.device.write_regs('conf.direction', [shex(1),]).get()
        self.device.send_cmd('start').get()

    def stop_move(self):
        self.device.send_cmd('stop').get()

    def deactivate(self):
        pass

    def stopall(self):
        self.device.stop()

class DiafrSvet(Puzzle):
    glossary_name = u"Свет диафрагм"
    glossary_code = u""

    def __init__(self, svet_addr, *args, **kwargs):
        super(DiafrSvet, self).__init__(*args, **kwargs)
        self.svet_addr = svet_addr

        self.port_gorgona = 4
        self.port_chel = 5
        self.port_zvezdi = 6
        self.port_put = 7

    def on_start(self):
        self.svet = Svet.start(self.svet_addr).proxy()
        self.svet.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            #lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.svet.init().get()

    def setup(self):
        self.svet.setup()

    def activate(self):
        pass

    def deactivate(self):
        pass

    def gorgona(self, on=True):
        if on is True:
            self.svet_on([self.port_gorgona])
        elif on is False:
            self.svet_off([self.port_gorgona])
        else:
            self.svet_blur([self.port_gorgona])

    def chel(self, on=True):
        if on is True:
            self.svet_on([self.port_chel])
        elif on is False:
            self.svet_off([self.port_chel])
        else:
            self.svet_blur([self.port_chel])

    def zvezdi(self, on=True):
        if on is True:
            self.svet_on([self.port_zvezdi])
        elif on is False:
            self.svet_off([self.port_zvezdi])
        else:
            self.svet_blur([self.port_zvezdi])

    def put(self, on=True):
        if on is True:
            self.svet_on([self.port_put])
        elif on is False:
            self.svet_off([self.port_put])
        else:
            self.svet_blur([self.port_put])

    def svet_on(self, channels='all'):
        if channels == 'all':
            channels = range(0,8)

        for chnl in channels:
            self.svet.set_channel(chnl, 100, 30)

        self.svet.shim().get()

    def svet_blur(self, channels='all'):
        if channels == 'all':
            channels = range(0,8)

        for chnl in channels:
            self.svet.set_channel(chnl, 40, 30)

        self.svet.shim().get()

    def svet_off(self, channels='all'):
        if channels == 'all':
            channels = range(0,8)

        for chnl in channels:
            self.svet.set_channel(chnl, 0, 30)

        self.svet.shim().get()


    def stopall(self):
        self.svet.stop()


class Gorgona(Puzzle):
    #22
    #port3 1 - non-solved
    #port4 1 - solved
    glossary_name = u"Загадка Горгона"
    glossary_code = u"G1R2O1-3"

    def __init__(self, device_addr, *args, **kwargs):
        super(Gorgona, self).__init__(*args, **kwargs)
        self.device_addr = device_addr

    def on_start(self):
        self.device = DummyDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.as_input().get()

    def activate(self):
        self.device.subscribe(
            'stat.inputs',
            lambda data: self.proxy().inputs(data[0]),
            500
        ).get()

    def deactivate(self):
        self.device.unsubscribe('stat.inputs').get()

    def inputs(self, state):
        #print bin(state)
        if state == int('1000', 2):
            #print 'solved'
            self.trigger('solved')

    def solve_puzzle(self):
        self.trigger('solved')

    def stopall(self):
        self.device.stop()


class Puteshestvie(Puzzle):
    #52
    glossary_name = u"Загадка Путешествие"
    glossary_code = u"G1R2O1-4"

    def __init__(self, device_addr, *args, **kwargs):
        super(Puteshestvie, self).__init__(*args, **kwargs)
        self.device_addr = device_addr

    def on_start(self):
        self.device = PutDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])


    def init(self):
        self.device.init().get()

    def reset(self):
        self.device.send_cmd('reset').get()

    def setup(self):
        self.device.send_cmd('setup').get()

    def activate(self):
        self.device.send_cmd('start').get()
        self.device.subscribe(
            'stat.solved',
            lambda data: self.proxy().stat_solved(data[0]),
            200
        ).get()

    def deactivate(self):
        self.device.unsubscribe('stat.solved').get()

    def stat_solved(self, state):
        print bin(state)
        if state == 1:
            print 'solved'
            self.trigger('solved')

    def solve_puzzle(self):
        self.trigger('solved')

    def stopall(self):
        self.device.stop()




class Zvezdi(Puzzle):
    #53
    glossary_name = u"Загадка Звезды"
    glossary_code = u"G1R2O1-5"

    def __init__(self, device_addr, *args, **kwargs):
        super(Zvezdi, self).__init__(*args, **kwargs)
        self.device_addr = device_addr

    def on_start(self):
        self.device = ZvezdyDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])


    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.send_cmd('start').get()

    def reset(self):
        self.device.send_cmd('off').get()

    def activate(self):
        self.device.subscribe(
            'stat.stage',
            lambda data: self.proxy().stage(data[0]),
            200
        ).get()

    def deactivate(self):
        self.device.unsubscribe('stat.stage').get()

    def stage(self, state):
        print bin(state)
        if state == 2:
            print 'solved'
            self.trigger('solved')

    def solve_puzzle(self):
        self.trigger('solved')

    def stopall(self):
        self.device.stop()



class Chelovek(Puzzle):
    #27
    #port7 1 - non-solved
    #port8 1 - solved
    glossary_name = u"Загадка Человек"
    glossary_code = u"G1R2O1-6"

    def __init__(self, device_addr, *args, **kwargs):
        super(Chelovek, self).__init__(*args, **kwargs)
        self.device_addr = device_addr

    def on_start(self):
        self.device = DummyDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])


    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.as_input().get()

    def activate(self):
        self.device.subscribe(
            'stat.inputs',
            lambda data: self.proxy().inputs(data[0]),
            500
        ).get()

    def deactivate(self):
        self.device.unsubscribe('stat.inputs').get()

    def inputs(self, state):
        #print bin(state)
        if state == int('10000000', 2):
            #print 'solved'
            self.trigger('solved')

    def solve_puzzle(self):
        self.trigger('solved')

    def stopall(self):
        self.device.stop()



TUMBA1_OPEN_PRF = [
    shex(500),# время действия этого сегмента
    shex(70),# начальная скорость
    shex(70),# конечная скорость

    shex(1000),# время действия этого сегмента
    shex(60),# начальная скорость
    shex(60),# конечная скорость

    shex(9000),# время действия этого сегмента
    shex(60),# начальная скорость
    shex(60),# конечная скорость

    shex(2000),# время действия этого сегмента
    shex(100),# начальная скорость
    shex(100),# конечная скорость
]

TUMBA1_CLOSE_PRF = [
    shex(1000),# время действия этого сегмента
    shex(100),# начальная скорость
    shex(100),# конечная скорость

    shex(1000),# время действия этого сегмента
    shex(100),# начальная скорость
    shex(100),# конечная скорость

    shex(500),# время действия этого сегмента
    shex(100),# начальная скорость
    shex(100),# конечная скорость

    shex(500),# время действия этого сегмента
    shex(100),# начальная скорость
    shex(100),# конечная скорость
]

TUMBA2_OPEN_PRF = [
    shex(500),# время действия этого сегмента
    shex(70),# начальная скорость
    shex(70),# конечная скорость

    shex(1000),# время действия этого сегмента
    shex(40),# начальная скорость
    shex(40),# конечная скорость

    shex(12000),# время действия этого сегмента
    shex(40),# начальная скорость
    shex(40),# конечная скорость

    shex(2000),# время действия этого сегмента
    shex(100),# начальная скорость
    shex(100),# конечная скорость
]

TUMBA2_CLOSE_PRF = [
    shex(1000),# время действия этого сегмента
    shex(100),# начальная скорость
    shex(100),# конечная скорость

    shex(1000),# время действия этого сегмента
    shex(100),# начальная скорость
    shex(100),# конечная скорость

    shex(500),# время действия этого сегмента
    shex(100),# начальная скорость
    shex(100),# конечная скорость

    shex(500),# время действия этого сегмента
    shex(100),# начальная скорость
    shex(100),# конечная скорость
]


TUMBA3_OPEN_PRF = [
    shex(500),# время действия этого сегмента
    shex(70),# начальная скорость
    shex(70),# конечная скорость

    shex(2000),# время действия этого сегмента
    shex(60),# начальная скорость
    shex(60),# конечная скорость

    shex(12000),# время действия этого сегмента
    shex(60),# начальная скорость
    shex(60),# конечная скорость

    shex(2000),# время действия этого сегмента
    shex(100),# начальная скорость
    shex(100),# конечная скорость

]
TUMBA3_CLOSE_PRF = [
    shex(1000),# время действия этого сегмента
    shex(100),# начальная скорость
    shex(100),# конечная скорость

    shex(1000),# время действия этого сегмента
    shex(100),# начальная скорость
    shex(100),# конечная скорость

    shex(500),# время действия этого сегмента
    shex(100),# начальная скорость
    shex(100),# конечная скорость

    shex(500),# время действия этого сегмента
    shex(100),# начальная скорость
    shex(100),# конечная скорость
]

TUMBA4_OPEN_PRF = [
    shex(500),# время действия этого сегмента
    shex(70),# начальная скорость
    shex(70),# конечная скорость

    shex(2000),# время действия этого сегмента
    shex(60),# начальная скорость
    shex(60),# конечная скорость

    shex(8000),# время действия этого сегмента
    shex(60),# начальная скорость
    shex(60),# конечная скорость

    shex(2000),# время действия этого сегмента
    shex(100),# начальная скорость
    shex(100),# конечная скорость

]

TUMBA4_CLOSE_PRF = [
    shex(1000),# время действия этого сегмента
    shex(100),# начальная скорость
    shex(100),# конечная скорость

    shex(500),# время действия этого сегмента
    shex(100),# начальная скорость
    shex(100),# конечная скорость

    shex(500),# время действия этого сегмента
    shex(100),# начальная скорость
    shex(100),# конечная скорость

    shex(500),# время действия этого сегмента
    shex(100),# начальная скорость
    shex(100),# конечная скорость
]

class Tumba(Puzzle):
    glossary_name = u"Изобретение"
    glossary_code = u"G1R2O4-{0}"

    def __init__(self, number, device_addr, open_prf, close_prf, shim=20, *args, **kwargs):
        Puzzle.__init__(self, *args, **kwargs)
        self.glossary_code = self.glossary_code.format(number)
        self.device_addr = device_addr
        self.number = number
        self.open_prf = open_prf
        self.close_prf = close_prf
        self.shim = shim

    def on_start(self):
        self.device = StekloliftDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])


    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.write_regs('conf.direction',
            [
                shex(1),# нарпавление движения
                shex(0),# номер профиля движения
                shex(1), # вкл датчик 1
                shex(1), # вкл датчик 2
                shex(100),# ограничение перегрузки
                shex(15000), # максимальное время движения
            ]
        )

        self.device.set_profile(0, self.open_prf).get()
        self.device.set_profile(1, self.close_prf).get()

    def activate(self):
        self.device.subscribe(
            'stat.stop_reason',
            lambda data: self.proxy().stopped(data[0])
        ).get()

    #def set_profile(self, number, data):
        #self.device.set_profile(number, data)

    def svet(self, svet=True):
        if svet:
            #print "Tumba {0} set shim {1}".format(self.device_addr, self.shim )
            self.set_shim(self.shim)
            self.device.send_cmd('lock_on').get()
        else:
            self.device.send_cmd('lock_off').get()

    def set_shim(self, shim):
        self.device.mb_write('0xE006', [shim]).get()

    def open(self):
        self.device.write_regs('conf.direction', [shex(0),]).get()
        self.device.write_regs('conf.profile_number', [shex(0),]).get()
        self.svet(True)
        self.device.send_cmd('start').get()

    def close(self, svet_off=True):
        self.device.write_regs('conf.direction', [shex(1),]).get()
        self.device.write_regs('conf.profile_number', [shex(1),]).get()
        if svet_off:
            self.svet(False)
        self.device.send_cmd('start').get()

    def stop_move(self):
        self.device.send_cmd('stop').get()

    def stopped(self, reason):
        # при открытии под датчика остановка
        # при закрытии под перегузку по току
        # 1 - сработал датчик
        # 3 - конец профиля движения
        log.debug('Device stopped: {0}'.format(reason))
        if reason in (1, 2):
            self.trigger('stopped_by_sensor')
        if reason > 2:
            self.trigger('stopped_not_by_sensor')
            if reason == 3: # конец профиля движения
                self.PUB.log(u"У девайса с адресом {0} закончился профиль движения.".format(self.device_addr), log_level='error')
            elif reason == 4: # перегрузка по току
                self.PUB.log(u"У девайса с адресом {0} сработала перегрузка по току.".format(self.device_addr), log_level='error')
            elif reason == 5: # перегрузка по току
                self.PUB.log(u"У девайса с адресом {0} сработалo ограничение максимального времени.".format(self.device_addr), log_level='error')
            elif reason == 6: # датчик перегрева
                self.PUB.log(u"У девайса с адресом {0} сработал датчик перегрева.".format(self.device_addr), log_level='error')

    def deactivate(self):
        self.device.unsubscribe('stat.stop_reason')


class Tank(Puzzle):
    #14
    glossary_name = u"Танк"
    glossary_code = u"G1R2O7"

    def __init__(self, device_addr, *args, **kwargs):
        super(Tank, self).__init__(*args, **kwargs)
        self.device_addr = device_addr

        self.port_vihod = 0
        self.port_vhod = 1

    def on_start(self):
        self.device = DummyDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])


    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.as_output().get()

    def set_zero(self, number):
        data = [
            shex(0),
            shex(0),
        ]
        self.device.set_port(number, data).get()

    def set_one(self, number):
        data = [
            shex(0),
            shex(1),
        ]
        self.device.set_port(number, data).get()

    def open_vhod(self):
        self.set_one(self.port_vhod)

    def close_vhod(self):
        self.set_zero(self.port_vhod)

    def open_vihod(self):
        self.set_one(self.port_vihod)

    def close_vihod(self):
        self.set_zero(self.port_vihod)

    def deactivate(self):
        pass

    def stopall(self):
        self.device.stop()




