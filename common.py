# encoding: utf-8
from pprint import pprint
import gevent
import pykka

from utils import debug
from engine import Resources, shex
from engine import Puzzle
from engine import DummyDevice, StekloliftDevice


class Svet(Puzzle):
    glossary_name = u"Подсветка"
    glossary_code = u""

    def __init__(self, device_addr, *args, **kwargs):
        super(Svet, self).__init__(*args, **kwargs)
        self.device_addr = device_addr
        self.glossary_code = device_addr

    def on_start(self):
        self.device = DummyDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def reset_state(self):
        #сбросить регистры настройки в ноль
        self.device.write_regs('conf.direction',
            [ shex(0) for _ in range(0, 33) ]
        ).get()

    def init(self):
        self.device.init().get()

    def setup(self):
        self.reset_state()
        self.device.as_output()

    def set_channel(self, number, output, time=0, alg=None):
        data = [
            shex(1),
            shex(output),
            shex(time),
            shex({
                'lin': 0,
                'log': 1,
                None: 1,
            }[alg])
        ]
        self.device.set_port(number, data).get()

    def shim(self, cmd='start', port='all'):
        if port == 'all':
            self.device.send_cmd('all_shim_{0}'.format(cmd)).get()
        else:
            self.device.send_cmd('port{0}_shim_{1}'.format(port, cmd)).get()


    def stopall(self):
        #TODO: сделать нормальный reset устройтсваЖ
        #debug()
        #self.reset_state()
        self.device.stop().get()


def reverse_profile(profile):
    profile = profile[:]
    reversed_profile = []
    while len(profile):
        segment = [profile.pop(-3), profile.pop(), profile.pop()]
        reversed_profile += segment
    return reversed_profile

