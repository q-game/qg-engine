#! ../env/bin/python
import zmq
import logging
import socket
host = socket.gethostbyname

def sub_logger(adr, port, level):
    ctx = zmq.Context.instance()
    sock = ctx.socket(zmq.SUB)
    sock.setsockopt(zmq.SUBSCRIBE, '')
    logging.basicConfig(level=level)
    sock.bind('tcp://{0}:{1}'.format(adr, port))
    poller = zmq.Poller()
    poller.register(sock, zmq.POLLIN)

    print "Logger started."
    print "Start polling for logs..."

    while True:
        socks = dict(poller.poll(100))
        if socks:
            message = sock.recv()
            print message
            #if message.endswith('\n'):
                #message = message[:-1]
            #log = getattr(logging, level.lower(), 'INFO')
            #log(message)

if __name__ == '__main__':
    try:
        sub_logger(host('qg_logger'), 25001, 'DEBUG')
    except KeyboardInterrupt:
        print "System interrupt..."
    finally:
        print "Termination of zmq context..."
        zmq.Context.instance().destroy()
        logging.shutdown()

