# encoding: utf-8
from functools import partial

import gevent

from engine import Resources, shex
from engine import Puzzle
from engine import DummyDevice, StekloliftDevice

from common import Svet
from room1 import ProemLatch

class Okoshko(Puzzle):
    #15
    glossary_name = u"Окно с решеткой"
    glossary_code = u"G1R3O4"

    def __init__(self, device_addr, *args, **kwargs):
        super(Okoshko, self).__init__(*args, **kwargs)
        self.device_addr = device_addr

        self.port_tainik = 1
        self.port_svet = 0

    def on_start(self):
        self.device = DummyDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])


    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.as_output().get()

    def _close_lock(self, number):
        data = [
            shex(0),
            shex(0),
        ]
        self.device.set_port(number, data).get()

    def _open_lock(self, number):
        data = [
            shex(0),
            shex(1),
        ]
        self.device.set_port(number, data).get()

    def open_tainik(self):
        self._open_lock(self.port_tainik)

    def close_tainik(self):
        self._close_lock(self.port_tainik)

    def svet_on(self):
        self._open_lock(self.port_svet)

    def svet_off(self):
        self._close_lock(self.port_svet)

    def stopall(self):
        self.device.stop()


class Fakels(Puzzle):
    #port6 1 - left
    #port7 1 - rigth
    #FIXME:
    # если правый факел оттянуть и шатать из стороны в сторону
    # то идут ложные срабатывания
    glossary_name = u"Факелы"
    glossary_code = u"G1R3O5"

    def __init__(self, device_addr, *args, **kwargs):
        super(Fakels, self).__init__(*args, **kwargs)
        self.device_addr = device_addr

    def on_start(self):
        self.device = DummyDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])


    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.as_input()

    def activate(self):
        self.device.subscribe(
            'stat.inputs',
            lambda data: self.proxy().inputs(data[0]),
            500
        ).get()

    def inputs(self, state):
        print bin(state)
        if state == int('10000000', 2):
            print 'rigth'
            self.trigger('rigth')
        elif state == int('01000000', 2):
            print 'left'
            self.trigger('left')
        else:
            print 'stop'
            self.trigger('stop')

    def deactivate(self):
        self.device.unsubscribe('stat.inputs')

    def stopall(self):
        self.device.stop()

class Ricar(Puzzle):
    # 11
    glossary_name = u"Рыцарь"
    glossary_code = u"G1R3O1"

    def __init__(self, device_addr, *args, **kwargs):
        super(Ricar, self).__init__(*args, **kwargs)
        self.device_addr = device_addr

    def on_start(self):
        self.device = DummyDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])


    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.as_input().get()

    def activate(self):
        self.device.subscribe(
            'stat.inputs',
            lambda data: self.proxy().inputs(data[0]),
            400
        ).get()

    def inputs(self, state):
        print bin(state)
        if state & int('10000000', 2):
            self.trigger('solved')

    def deactivate(self):
        self.device.unsubscribe('stat.inputs')

    def stopall(self):
        self.device.stop()
