# encoding: utf-8
from pprint import pprint
import json
import signal
from datetime import datetime

import pykka
import pykka.debug
import gevent
import zmq.green as zmq
from pykka.gevent import GeventActor as Actor
from pykka.gevent import GeventFuture as Future

from net import Requester, Subscriber
from net import zmq_ctx

from eng_settings import ADRS

import logging
logging.basicConfig(level=logging.DEBUG)

class PassHandler(object):
    def tell(self, d):
        pass

def cmd(cmd, *args, **kwargs):
    msg = {
        "type": "cmd",
        "cmd": cmd,
        "args": args,
        "kwargs": kwargs,
        "time": str(datetime.now())
    }
    return msg

if __name__ == '__main__':
    #zmq_ctx = zmq.Context()
    #import pudb; pu.db
    try:
        address = "tcp://{ip}:{port}".format(**ADRS['CLI_SRV'])
        cli_req = Requester.start(address).proxy()
        address = "tcp://{ip}:{port}".format(**ADRS['CLI_PUB'])
        cli_sub = Subscriber.start(address, PassHandler()).proxy()

        print cli_req.request(cmd('system::ping')).get()
        gevent.sleep(1)
        print cli_req.request(cmd('game::init')).get()
        gevent.sleep(3)
        print cli_req.request(cmd('game::start')).get()
        gevent.sleep(25)
        print cli_req.request(cmd('game::pause')).get()
        gevent.sleep(1)
        print cli_req.request(cmd('game::restart')).get()
        gevent.sleep(1)
        print cli_req.request(cmd('game::stop')).get()
        gevent.sleep(1)

    except (KeyboardInterrupt, SystemExit):
        print "System interrupt..."
    finally:
        pykka.ActorRegistry.stop_all(True)
        zmq_ctx.term()

#{"type": "cmd", "cmd": "start", "args": {//тут доп аргументы}, “time”: “dd:MM::YY hh:mm:ss” }
#{ “status”: “success” , “time” : “dd.MM.YY hh:mm:ss” }
#Команды:
#system::ping – Проверка соединения с сервером
#game::init – Инициализация игры
#game::start – Запуск игры
#game::pause – Пауза игры
#game::restart – Сброс игры в начало.
