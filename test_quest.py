#!/usr/bin/env python
# encoding: utf-8
from functools import partial
from pprint import pprint

import gevent
from pykka.gevent import GeventActor as Actor
import pykka

from engine import Resources, run_game
from engine import Resources, shex
from engine import Puzzle
from engine import DummyDevice, StekloliftDevice

from net import Requester, Responder, Publisher
from eng_settings import ADRS
from quest import init
from net import zmq_ctx

import pudb
pudb.DEFAULT_SIGNAL = None

class StekloLiftProto(Puzzle):
    glossary_name = u"Стеклолифт (Тестовый девайс)"
    glossary_code = u"ST-P-{0}"

    def __init__(self, device_addr, *args, **kwargs):
        super(StekloLiftProto, self).__init__(*args, **kwargs)
        self.glossary_code = self.glossary_code.format(device_addr)
        self.device_addr = device_addr

    def on_start(self):
        self.device = StekloliftDevice.start(self.device_addr).proxy()

    def init(self):
        self.device.init().get()
        #self.device.mb_write(
            #self.device.REG.get()['conf']['direction'], # начало регистров настроек
            #[
                #shex(0), # направление движения
            #]
        #).get()

    def stopped(self, reason):
        # 1 - сработал датчик
        if int(reason) > 0:
            self.trigger('openned')

    def open(self):
        self.device.subscribe(
            self.device.REG.get()['stat']['stop_reason'],
            lambda data: self.proxy().stopped(data[0]),
            1000
        ).get()

        #self.device.mb_write(self.device.REG.get()['conf']['direction'], [shex(0),])
        self.device.mb_command(self.device.CMD.get()['start'])

    def stopall(self):
        self.device.stop()


class DummyDeviceProtoV1(Puzzle):
    glossary_name = u"Дамми (Протоип v1)"
    glossary_code = u"DM-P-{0}"

    def __init__(self, device_addr, *args, **kwargs):
        super(DummyDeviceProtoV1, self).__init__(*args, **kwargs)
        self.glossary_code = self.glossary_code.format(device_addr)
        self.device_addr = device_addr

    def on_start(self):
        self.device = DummyDevice.start(self.device_addr).proxy()

    def init(self):
        self.device.init().get()
        self.device.mb_write('0x0004', ['0xC1CA']).get()

    def led_on(self):
        self.device.mb_write('0x0004', ['0xFFFF']).get()

    def led_off(self):
        self.device.mb_write('0x0004', ['0xC1CA']).get()

    def stopall(self):
        self.led_off()
        self.device.stop()



class Game(Actor):

    def __init__(self):
        super(Game, self).__init__()
        self.PUB = Resources().require('PUB')

    def proxy(self):
        return self.actor_ref.proxy()

    def init(self):
        self.st_proto = init(StekloLiftProto, '0x01')
        self.dummy2 = init(DummyDeviceProtoV1, '0x96')
        self.dummy1 = init(DummyDeviceProtoV1, '0xEA')
        self.PUB.log("Game inited.")

    def start_game(self):
        self.PUB.log("Game starting.")
        self.st_proto.subscribe('openned', [
            lambda: pprint('Openned!'),
            lambda: self.dummy2.led_on()
        ], once=True)
        self.dummy1.led_on()
        self.st_proto.open()

    def stop_game(self):
        self.st_proto.stop()
        self.dummy1.stop()
        self.dummy2.stop()


def interactive():
    address = "tcp://{ip}:{port}".format(**ADRS['VHAL'])
    Resources().register('HAL.default_modbus', Requester.start(address).proxy())

def exit_interactive():
    HAL = Resources().require('HAL.default_modbus')
    HAL.request({'type': 'system', 'cmd': 'shutdown'})
    HAL.stop()
    pykka.ActorRegistry.stop_all(True)
    print "Termination of zmq context..."
    zmq_ctx.term()


if __name__ == '__main__':
    try:
        address = "tcp://{ip}:{port}".format(**ADRS['VHAL'])
        Resources().register('HAL.default_modbus', Requester.start(address).proxy())
        HAL = Resources().require('HAL.default_modbus')
        HAL.request({
            'type': 'system',
            'cmd': 'config',
            'kwargs': {
                'engine_ip': ADRS['ENG']['ip'],
            }
        })
        address = "tcp://{ip}:{port}".format(**ADRS['CLI_PUB'])
        PUB = Publisher.start(address).proxy()
        Resources().register('PUB', PUB)

        game = Game.start().proxy()
        gevent.sleep(1)
        #pu.db
        gevent.sleep(1)
        game.init()
        gevent.sleep(1)
        #pu.db
        gevent.sleep(1)
        game.start_game()
        gevent.sleep(1)
        #pu.db
        gevent.sleep(7)
        game.stop_game()
        gevent.sleep(2)
        #pu.db
        gevent.sleep(1)
        game.stop()

        HAL = Resources().require('HAL.default_modbus')
        HAL.request({'type': 'system', 'cmd': 'shutdown'})
        HAL.stop()
        PUB.stop()

    except (KeyboardInterrupt, SystemExit):
        print "System interrupt..."
    finally:
        pykka.ActorRegistry.stop_all(True)
        print "Termination of zmq context..."
        zmq_ctx.term()

