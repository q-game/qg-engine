#! ../env/bin/python
# encoding: utf-8
from pprint import pprint
from gevent import sleep

from utils import Resources, Scheduler, shex, load_device_addrs
from quest import Game

dv_addrs = load_device_addrs()['davinchi']

class DummyPUB(object):
    def log(self, msg, log_level=None):
        print msg

    def event(self, msg):
        print msg

    def step_event(self, msg):
        print msg


Resources().register('PUB', DummyPUB())

def test_game():
    Resources().register('PUB', DummyPUB())
    game = Game.start().proxy()
    game.game_init().get()
    return game

from engine import run_game
def test_game_with_cli():
    run_game(Game)
    return lambda: Resources().require('game')


from engine import shex
from quest import init
from room1 import Lira, Velosiped, Dveri, Dama, Avtoportret

def test_lira():
    lira = init(Lira, 35)
    lira.setup()
    lira.activate()

    #lira.subscribe(
        #'akkord_wrong',
        #lambda: pprint('hit!'),
    #).get()
    #lira.subscribe(
        #'akkord_2',
        #lambda: pprint('akkord_2 for subscriber'),
    #).get()
    return lira

def test_portret():
    portret = init(Avtoportret, 32)
    portret.setup()
    portret.activate()
    return portret

def test_velosiped():
    velo = init(Velosiped, 30)
    velo.setup()
    velo.activate()
    return velo

def test_dveri():
    dveri = init(Dveri, 43)
    dveri.setup()
    return dveri

def test_dama():
    dama = init(Dama, 34)
    dama.setup()
    return dama


from room1 import Proem, ProemLatch, LATCH6_OPEN_PRF, LATCH5_OPEN_PRF, reverse_profile

def test_latch6():
    latch6 = ProemLatch.start(5, 6).proxy()
    latch6.init().get()
    latch6.setup()
    latch6.set_profile(0, LATCH6_OPEN_PRF)
    latch6.set_profile(1, reverse_profile(LATCH6_OPEN_PRF + [
        shex(1000),# время действия этого сегмента
        shex(5),# начальная скорость
        shex(0),# конечная скорость
    ]))
    return latch6

def test_latch5():
    latch5 = ProemLatch.start(5, 5).proxy()
    latch5.init().get()
    latch5.setup()
    latch5.set_profile(0, LATCH5_OPEN_PRF)
    latch5.set_profile(1, reverse_profile(LATCH5_OPEN_PRF + [
        shex(1000),# время действия этого сегмента
        shex(5),# начальная скорость
        shex(0),# конечная скорость
    ]))
    return latch5



def test_proem():
    proem = Proem.start(addrs=[1, 2, 3, 4, 5, 6]).proxy()
    proem.init().get()
    proem.setup().get()
    proem.subscribe('openned', [
        lambda: pprint('Proem openned!'),
    ], once=True)
    proem.setup()
    return proem

    #from room1 import reverse_profile
    #from room1 import LATCH1_OPEN_PRF, LATCH3_OPEN_PRF

    #latch1 = init(ProemLatch, 0, '0x01')
    #latch1.set_profile(0, LATCH1_OPEN_PRF).get()
    #latch1.set_profile(1, reverse_profile(LATCH1_OPEN_PRF)).get()
    #latch3 = init(ProemLatch, 0, '0x03')
    #latch3.set_profile(0, LATCH3_OPEN_PRF).get()
    #latch3.set_profile(1, reverse_profile(LATCH3_OPEN_PRF)).get()
    #latch1.subscribe('open-close', [
        #lambda: pprint('Proem openned!'),
    #], once=True)


from engine import DummyDevice

def test_dummy():
    dv = DummyDevice.start(shex(25)).proxy()
    dv.init().get()

    dv.set_port(7, [
            shex(2)
        ])
    dv.set_port(6, [
            shex(2)
        ])
    dv.set_port(5, [
            shex(2)
        ])
    dv.set_port(4, [
            shex(2)
        ])
    handler = dv.subscribe(
        'stat.inputs',
        lambda data: pprint(bin(data[0]))
    )

from room2 import Tumba
from room2 import TUMBA1_OPEN_PRF, TUMBA1_CLOSE_PRF
from room2 import TUMBA2_OPEN_PRF, TUMBA2_CLOSE_PRF
from room2 import TUMBA3_OPEN_PRF, TUMBA3_CLOSE_PRF
from room2 import TUMBA4_OPEN_PRF, TUMBA4_CLOSE_PRF
def test_tumba(addr=7, open_prf=TUMBA1_OPEN_PRF, close_prf=TUMBA1_CLOSE_PRF):
    t = Tumba.start(1, shex(addr), open_prf, close_prf).proxy()
    t.init().get()
    t.setup().get()
    return t

def test_tumbi():
    t1 = test_tumba(7, TUMBA1_OPEN_PRF, TUMBA1_CLOSE_PRF)
    t2 = test_tumba(8, TUMBA2_OPEN_PRF, TUMBA2_CLOSE_PRF)
    t3 = test_tumba(9, TUMBA3_OPEN_PRF, TUMBA3_CLOSE_PRF)
    t4 = test_tumba(10, TUMBA4_OPEN_PRF, TUMBA4_CLOSE_PRF)
    return t1, t2, t3, t4

from room2 import Diafragma
def test_diafr(addr=57):
    d = Diafragma.start(1,shex(addr)).proxy()
    d.init().get()
    d.setup().get()
    return d

from room2 import DiafrSvet
def test_diafr_svet(addr=19):
    ds = DiafrSvet.start(addr).proxy()
    ds.init().get()
    ds.setup().get()
    return ds

from room2 import Knopka
def test_knopka():
    t = Knopka.start(shex(28)).proxy()
    t.init().get()
    t.setup().get()
    t.activate().get()
    return t

from room2 import Gorod
def test_gorod():
    g = Gorod.start(
        dv_addrs['room2']['gorod']['svet'],
        dv_addrs['room2']['gorod']['mechanic'],
        dv_addrs['room2']['gorod']['lift'],
    ).proxy()
    g.init().get()
    g.setup().get()
    return g

from room2 import Gorgona
def test_gorgona():
    gorgona = init(Gorgona, 22)
    gorgona.setup()
    gorgona.activate()
    return gorgona

from room2 import Chelovek
def test_chel():
    chel = init(Chelovek, 27)
    chel.setup()
    chel.activate()
    return chel

from room2 import Zvezdi
def test_zvezdi():
    zv = init(Zvezdi, 53)
    zv.setup()
    zv.activate()
    return zv

from room2 import Puteshestvie
def test_put():
    pt = init(Puteshestvie, 52)
    pt.setup()
    pt.activate()
    return pt


from room2 import Tank
def test_tank():
    t = init(Tank, 14)
    t.setup()
    return t


from common import Svet
from room2 import Vitrag, KluchOtTanka

def test_vitrag():
    v1 = Vitrag.start(1, shex(20), main=1, part=0).proxy()
    v1.init().get()
    v1.setup().get()

    v1.main_light().get()
    sleep(5)
    v1.wrong_akkord().get()
    sleep(3)
    v1.akkord().get()
    sleep(3)
    return v1

def test_vitragy():
    lira = test_lira()

    v1 = Vitrag.start(1, shex(20), main=1, part=0).proxy()
    v2 = Vitrag.start(2, shex(21), main=2, part=1).proxy()
    v3 = Vitrag.start(3, shex(23), main=2, part=3).proxy()
    v4 = Vitrag.start(4, shex(24), main=1, part=2).proxy()

    v1.init().get()
    v1.setup().get()
    v2.init().get()
    v2.setup().get()
    v3.init().get()
    v3.setup().get()
    v4.init().get()
    v4.setup().get()

    v1.main_light().get()
    v2.main_light().get()
    v3.main_light().get()
    v4.main_light().get()

    lira.subscribe(
        'akkord_1',[
            lambda: v1.akkord()
        ]
    ).get()
    lira.subscribe(
        'akkord_2',[
            lambda: v2.akkord()
        ]
    ).get()
    lira.subscribe(
        'akkord_3',[
            lambda: v3.akkord()
        ]
    ).get()
    lira.subscribe(
        'akkord_4',[
            lambda: v4.akkord()
        ]
    ).get()

    lira.subscribe(
        'akkord_wrong', [
            lambda: v1.wrong_akkord(),
            lambda: v2.wrong_akkord(),
            lambda: v3.wrong_akkord(),
            lambda: v4.wrong_akkord()
        ]
    ).get()


def test_svet(addr):
    svet = Svet.start(shex(addr)).proxy()
    svet.init().get()
    svet.setup().get()
    svet.set_channel(0, 100).get()
    svet.set_channel(1, 100).get()
    svet.set_channel(2, 100).get()
    svet.set_channel(3, 100).get()
    svet.set_channel(4, 100).get()
    svet.set_channel(5, 100).get()
    svet.set_channel(6, 100).get()
    svet.set_channel(7, 100).get()
    svet.shim()
    return svet

    #svet.light(True)
    #svet.set_channel(1, 'fade', 0, 250, 5000)
    #sleep(0.5)
    #svet.set_channel(0, 'fade', 0, 100, 3000)
    #sleep(4)
    #svet.set_channel(0, 'fade', 100, 0, 5000)
    #sleep(0.5)
    #svet.set_channel(1, 'fade', 250, 0, 5000)
    #sleep(4)
    #sleep(4)


def svet_stress_test():
    svet = Svet.start(shex(35)).proxy()
    svet.init().get()
    svet.light(True)
    svet.set_channel(1, 'shim', 20)
    for _ in range(0,10, 1):
        svet.set_channel(0, 'shim', 100)
        svet.set_channel(0, 'shim', 0)
        #svet.set_channel(1, 'shim', 100)
        #svet.set_channel(1, 'shim', 0)

    svet.stop().get()

def kislota():
    vitrs = []

    v1 = Vitrag.start(1, shex(14), main=1, part=0).proxy()
    v1.init().get()
    vitrs.append(v1)

    v2 = Vitrag.start(2, shex(15), main=0, part=1).proxy()
    v2.init().get()
    vitrs.append(v2)

    v3 = Vitrag.start(3, shex(11), main=2, part=3).proxy()
    v3.init().get()
    vitrs.append(v3)

    v4 = Vitrag.start(4, shex(13), main=1, part=2).proxy()
    v4.init().get()
    vitrs.append(v4)

    for _ in range(0,50, 1):
        for v in vitrs:
            v.part('shim', 100)
            v.part('shim', 0)
            v.main('shim', 100)
            v.main('shim', 0)

def test_kluch():
    k = KluchOtTanka.start(shex(50)).proxy()
    k.init().get()
    k.setup().get()
    return k

from room3 import Okoshko, Fakels, Ricar

def test_okoshko():
    o = Okoshko.start(shex(15)).proxy()
    o.init().get()
    o.setup().get()
    return o

def test_ricar():
    r = Ricar.start(shex(11)).proxy()
    r.init().get()
    r.setup().get()
    r.activate().get()
    return r


def test_fakels():
    f = Fakels.start(shex(13)).proxy()
    f.init().get()
    f.setup().get()
    f.activate().get()
    return f


from room4 import Lustra, TimeMachine

def test_lustra():
    l = Lustra.start(shex(42)).proxy()
    l.init().get()
    l.setup().get()
    return l


def test_timem():
    t = TimeMachine.start(shex(18)).proxy()
    t.init().get()
    t.setup().get()
    t.activate().get()
    return t
