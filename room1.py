# encoding: utf-8
from functools import partial

import gevent

from engine import Resources, shex
from engine import Puzzle
from engine import DummyDevice, StekloliftDevice

from common import reverse_profile

from utils import shex

import logging
log = logging.getLogger('[rm1]')
log.addHandler(logging.NullHandler())


LATCH1_OPEN_PRF = [
    shex(1000),# время действия этого сегмента
    shex(0),# начальная скорость
    shex(10),# конечная скорость

    shex(1000),# время действия этого сегмента
    shex(10),# начальная скорость
    shex(20),# конечная скорость

    shex(1000),# время действия этого сегмента
    shex(20),# начальная скорость
    shex(40),# конечная скорость

    shex(10000),# время действия этого сегмента
    shex(40),# начальная скорость
    shex(40),# конечная скорость

    shex(5000),# время действия этого сегмента
    shex(100),# начальная скорость
    shex(100),# конечная скорость
]

LATCH2_OPEN_PRF = [
    shex(1000),# время действия этого сегмента
    shex(0),# начальная скорость
    shex(10),# конечная скорость

    shex(1000),# время действия этого сегмента
    shex(10),# начальная скорость
    shex(20),# конечная скорость

    shex(1000),# время действия этого сегмента
    shex(20),# начальная скорость
    shex(40),# конечная скорость

    shex(10000),# время действия этого сегмента
    shex(40),# начальная скорость
    shex(40),# конечная скорость
]


LATCH3_OPEN_PRF = [
    shex(1000),# время действия этого сегмента
    shex(0),# начальная скорость
    shex(8),# конечная скорость

    shex(1000),# время действия этого сегмента
    shex(8),# начальная скорость
    shex(13),# конечная скорость

    shex(1000),# время действия этого сегмента
    shex(11),# начальная скорость
    shex(17),# конечная скорость

    shex(9300),# время действия этого сегмента
    shex(17),# начальная скорость
    shex(17),# конечная скорость
]

LATCH4_OPEN_PRF = [
    shex(1000),# время действия этого сегмента
    shex(0),# начальная скорость
    shex(10),# конечная скорость

    shex(1000),# время действия этого сегмента
    shex(10),# начальная скорость
    shex(20),# конечная скорость

    shex(1000),# время действия этого сегмента
    shex(20),# начальная скорость
    shex(20),# конечная скорость

    shex(9300),# время действия этого сегмента
    shex(20),# начальная скорость
    shex(20),# конечная скорость
]

LATCH5_OPEN_PRF = [
    shex(1000),# время действия этого сегмента
    shex(0),# начальная скорость
    shex(8),# конечная скорость

    shex(1000),# время действия этого сегмента
    shex(8),# начальная скорость
    shex(13),# конечная скорость

    shex(1000),# время действия этого сегмента
    shex(11),# начальная скорость
    shex(17),# конечная скорость

    shex(9300),# время действия этого сегмента
    shex(17),# начальная скорость
    shex(17),# конечная скорость
]

LATCH6_OPEN_PRF = [
    shex(1000),# время действия этого сегмента
    shex(0),# начальная скорость
    shex(8),# конечная скорость

    shex(1000),# время действия этого сегмента
    shex(8),# начальная скорость
    shex(11),# конечная скорость

    shex(1000),# время действия этого сегмента
    shex(11),# начальная скорость
    shex(15),# конечная скорость

    shex(9300),# время действия этого сегмента
    shex(15),# начальная скорость
    shex(15),# конечная скорость
]


class ProemLatch(Puzzle):
    glossary_name = u"Проем(задвижка)"
    glossary_code = u"G1R1O1-{0}"

    def __init__(self, latch_number, device_addr, *args, **kwargs):
        super(ProemLatch, self).__init__(*args, **kwargs)
        self.glossary_code = self.glossary_code.format(latch_number)
        self.device_addr = device_addr
        self.latch_number = latch_number

    def on_start(self):
        self.device = StekloliftDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.write_regs('conf.direction',
            [
                shex(1),# нарпавление движения
                shex(0),# номер профиля движения
                shex(1), # вкл датчик 1
                shex(1), # вкл датчик 2
                shex(80),# ограничение перегрузки
                shex(25000), # максимальное время движения
            ]
        )

    def set_profile(self, number, data):
        self.device.set_profile(number, data)

    def lock(self, locked):
        if locked:
            self.device.send_cmd('lock_on')
        else:
            self.device.send_cmd('lock_off')

    def activate(self):
        self.device.subscribe(
            'stat.stop_reason',
            lambda data: self.proxy().stopped(data[0])
        ).get()

    def deactivate(self):
        self.device.unsubscribe('stat.stop_reason')

    def stopped(self, reason):
        # при открытии под датчика остановка
        # при закрытии под перегузку по току
        # 1 - сработал датчик
        # 3 - конец профиля движения
        log.debug('Device stopped: {0}'.format(reason))
        if reason in (1, 2):
            self.trigger('stopped_by_sensor')
        if reason > 2:
            self.trigger('stopped_not_by_sensor')
            if reason == 3: # конец профиля движения
                self.PUB.log(u"У девайса с адресом {0} закончился профиль движения.".format(self.device_addr), log_level='error')
            elif reason == 4: # перегрузка по току
                self.PUB.log(u"У девайса с адресом {0} сработала перегрузка по току.".format(self.device_addr), log_level='error')
            elif reason == 5: # перегрузка по току
                self.PUB.log(u"У девайса с адресом {0} сработалo ограничение максимального времени.".format(self.device_addr), log_level='error')
            elif reason == 6: # датчик перегрева
                self.PUB.log(u"У девайса с адресом {0} сработал датчик перегрева.".format(self.device_addr), log_level='error')


    def open(self):
        #import pudb; pu.db
        #stop_reason_handler = self.device.subscribe(
            #'stat.stop_reason',
            #lambda data: self.proxy().stopped(data[0])
        #).get()
        #openned_handler = self.subscribe('open-close',[
            #lambda: self.device.unsubscribe(
                #'stat.stop_reason',
                #stop_reason_handler
            #),
            #lambda: self.proxy().unsubscribe('open-close', openned_handler)
        #])
        self.lock(True)
        self.device.write_regs('conf.direction', [shex(1),])
        self.device.write_regs('conf.profile_number', [shex(0),])
        self.device.send_cmd('start')

    def close(self):
        #stop_reason_handler = self.device.subscribe(
            #'stat.stop_reason',
            #lambda data: self.proxy().stopped(data[0])
        #).get()
        #stopped_handler = self.subscribe('open-close',[
            #lambda: self.device.unsubscribe(
                #'stat.stop_reason',
                #stop_reason_handler
            #),
            #lambda: self.proxy().unsubscribe('open-close', stopped_handler)
        #])
        self.lock(True)
        self.device.write_regs('conf.direction', [shex(0),])
        self.device.write_regs('conf.profile_number', [shex(1),])
        self.device.send_cmd('start')

    def move_stop(self):
        self.device.send_cmd('stop')

    def stopall(self):
        self.device.stop()


class Proem(Puzzle):
    glossary_name = u"Проем"
    glossary_code = u"G1R1O1"

    def __init__(self, *args, **kwargs ):
        super(Proem, self).__init__(*args, **kwargs)
        self.latches = [False, False, False, False, False, False]
        self.addrs = kwargs.get('addrs')


    def on_start(self):
        self.latch1 = ProemLatch.start(1, self.addrs[0]).proxy()
        self.latch2 = ProemLatch.start(2, self.addrs[1]).proxy()
        self.latch3 = ProemLatch.start(3, self.addrs[2]).proxy()
        self.latch4 = ProemLatch.start(4, self.addrs[3]).proxy()
        self.latch5 = ProemLatch.start(5, self.addrs[4]).proxy()
        self.latch6 = ProemLatch.start(6, self.addrs[5]).proxy()
        for latch in [
            self.latch1,
            self.latch2,
            self.latch3,
            self.latch4,
            self.latch5,
            self.latch6,
        ]:
            latch.subscribe('fail', lambda data: self.proxy().trigger('fail', data))

    def init(self):
        self.latch1.init().get()
        self.latch2.init().get()
        self.latch3.init().get()
        self.latch4.init().get()
        self.latch5.init().get()
        self.latch6.init().get()
        log.debug(u'{0} Started'.format(self))

    def setup(self):
        self.latch1.setup()
        self.latch1.set_profile(0, LATCH1_OPEN_PRF)
        self.latch1.set_profile(1, reverse_profile(LATCH1_OPEN_PRF + [
            shex(1000),# время действия этого сегмента
            shex(5),# начальная скорость
            shex(0),# конечная скорость
        ]))

        self.latch2.setup()
        self.latch2.set_profile(0, [
            shex(3000),# время действия этого сегмента
            shex(0),# начальная скорость
            shex(0),# конечная скорость
            ] + LATCH2_OPEN_PRF)
        self.latch2.set_profile(1, reverse_profile(LATCH2_OPEN_PRF + [
            shex(2000),# время действия этого сегмента
            shex(5),# начальная скорость
            shex(0),# конечная скорость
        ]))

        self.latch3.setup()
        self.latch3.set_profile(0, [
            shex(6000),# время действия этого сегмента
            shex(0),# начальная скорость
            shex(0),# конечная скорость
            ] + LATCH3_OPEN_PRF)
        self.latch3.set_profile(1, reverse_profile(LATCH3_OPEN_PRF + [
            shex(1000),# время действия этого сегмента
            shex(5),# начальная скорость
            shex(0),# конечная скорость
        ]))

        self.latch4.setup()
        self.latch4.set_profile(0, [
            shex(9000),# время действия этого сегмента
            shex(0),# начальная скорость
            shex(0),# конечная скорость
            ] + LATCH4_OPEN_PRF)
        self.latch4.set_profile(1, reverse_profile(LATCH4_OPEN_PRF + [
            shex(1000),# время действия этого сегмента
            shex(5),# начальная скорость
            shex(0),# конечная скорость
        ]))

        self.latch5.setup()
        self.latch5.set_profile(0, [
            shex(15000),# время действия этого сегмента
            shex(0),# начальная скорость
            shex(0),# конечная скорость
            ] +  LATCH5_OPEN_PRF)
        self.latch5.set_profile(1, reverse_profile(LATCH5_OPEN_PRF + [
            shex(1500),# время действия этого сегмента
            shex(20),# начальная скорость
            shex(0),# конечная скорость
        ]))

        self.latch6.setup()
        self.latch6.set_profile(0, [
            shex(15000),# время действия этого сегмента
            shex(0),# начальная скорость
            shex(0),# конечная скорость
            ] +  LATCH6_OPEN_PRF)
        self.latch6.set_profile(1, reverse_profile(LATCH6_OPEN_PRF + [
            shex(1000),# время действия этого сегмента
            shex(5),# начальная скорость
            shex(0),# конечная скорость
        ]))


    def latch_state(self, latch, state):
        self.latches[latch-1] = state
        log.debug('Latch state: %s %s' % (latch, state))
        if all(self.latches):
            self.trigger('openned')

    def open_proem(self):
        self.latch1.subscribe('stopped_by_sensor', lambda : self.proxy().latch_state(1, True) )
        self.latch2.subscribe('stopped_by_sensor', lambda : self.proxy().latch_state(2, True) )
        self.latch3.subscribe('stopped_by_sensor', lambda : self.proxy().latch_state(3, True) )
        self.latch4.subscribe('stopped_by_sensor', lambda : self.proxy().latch_state(4, True) )
        self.latch5.subscribe('stopped_by_sensor', lambda : self.proxy().latch_state(5, True) )
        self.latch6.subscribe('stopped_by_sensor', lambda : self.proxy().latch_state(6, True) )

        self.latch1.subscribe('stopped_not_by_sensor', lambda: self.trigger('stopped_not_by_sensor'))
        self.latch2.subscribe('stopped_not_by_sensor', lambda: self.trigger('stopped_not_by_sensor'))
        self.latch3.subscribe('stopped_not_by_sensor', lambda: self.trigger('stopped_not_by_sensor'))
        self.latch4.subscribe('stopped_not_by_sensor', lambda: self.trigger('stopped_not_by_sensor'))
        self.latch5.subscribe('stopped_not_by_sensor', lambda: self.trigger('stopped_not_by_sensor'))
        self.latch6.subscribe('stopped_not_by_sensor', lambda: self.trigger('stopped_not_by_sensor'))

        self.latch1.activate()
        self.latch2.activate()
        self.latch3.activate()
        self.latch4.activate()
        self.latch5.activate()
        self.latch6.activate()

        self.latch1.open()
        self.latch2.open()
        self.latch3.open()
        self.latch4.open()
        self.latch5.open()
        self.latch6.open()

    def close_proem(self):
        self.latch1.close()
        self.latch2.close()
        self.latch3.close()
        self.latch4.close()
        self.latch5.close()
        self.latch6.close()

    def move_stop(self):
        self.latch1.move_stop()
        self.latch2.move_stop()
        self.latch3.move_stop()
        self.latch4.move_stop()
        self.latch5.move_stop()
        self.latch6.move_stop()

    def stopall(self):
        self.latch1.stop()
        self.latch2.stop()
        self.latch3.stop()
        self.latch4.stop()
        self.latch5.stop()
        self.latch6.stop()

    def deactivate(self):
        self.latch1.deactivate()
        self.latch2.deactivate()
        self.latch3.deactivate()
        self.latch4.deactivate()
        self.latch5.deactivate()
        self.latch6.deactivate()


class Avtoportret(Puzzle):
    #32
    #port6 1 - non-solved
    #port7 1 - solved
    glossary_name = u"Автопортрет"
    glossary_code = u"G1R1O2"

    def __init__(self, device_addr, *args, **kwargs):
        super(Avtoportret, self).__init__(*args, **kwargs)
        self.device_addr = device_addr

    def on_start(self):
        self.device = DummyDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.as_input()

    def activate(self):
        self.device.subscribe(
            'stat.inputs',
            lambda data: self.proxy().inputs(data[0]),
            300
        ).get()

    def deactivate(self):
        self.device.unsubscribe('stat.inputs')

    def inputs(self, state):
        print bin(state)
        if state & int('10000000', 2):
            self.trigger('solved')

    def stopall(self):
        self.device.stop()


class Dveri(Puzzle):
    glossary_name = u"Входная и выходная двери"
    glossary_code = u"G1R1O7"

    def __init__(self, device_addr, *args, **kwargs):
        super(Dveri, self).__init__(*args, **kwargs)
        self.device_addr = device_addr

        self.port_vihod = 0
        self.port_vhod = 1

    def on_start(self):
        self.device = DummyDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.as_output().get()

    def _close_lock(self, number):
        data = [
            shex(0),
            shex(0),
        ]
        self.device.set_port(number, data).get()

    def _open_lock(self, number):
        data = [
            shex(0),
            shex(1),
        ]
        self.device.set_port(number, data).get()

    def open_vhod(self):
        print "Open vhod"
        self._open_lock(self.port_vhod)

    def close_vhod(self):
        print "Close vhod"
        self._close_lock(self.port_vhod)

    def open_vihod(self):
        print "Open vihod"
        self._open_lock(self.port_vihod)

    def close_vihod(self):
        print "Close vihod"
        self._close_lock(self.port_vihod)

    def deactivate(self):
        pass

    def stopall(self):
        self.device.stop()


class Dama(Puzzle):
    glossary_name = u"Дама с горностаем"
    glossary_code = u"G1R1O3"

    def __init__(self, device_addr, *args, **kwargs):
        super(Dama, self).__init__(*args, **kwargs)
        self.device_addr = device_addr

        #self.port_?? = 0 - что на этом порту? что-то щелкает
        self.port_lock = 1

    def on_start(self):
        self.device = DummyDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.as_output().get()

    def close_lock(self):
        data = [
            shex(0),
            shex(0),
        ]
        self.device.set_port(self.port_lock, data).get()
    close_lock.gui_control = True

    def open_lock(self):
        u"""Открыть даму"""
        data = [
            shex(0),
            shex(1),
        ]
        self.device.set_port(self.port_lock, data).get()
        scheduler = Resources().require('scheduler')
        scheduler.schedule(lambda: self.proxy().close_lock(), 1500, once=True)
    open_lock.gui_control = True

    def stopall(self):
        self.device.stop()

    def deactivate(self):
        pass


class Lira(Puzzle):
    glossary_name = u"Лира"
    glossary_code = u"G1R1O5"

    def __init__(self, device_addr, *args, **kwargs):
        super(Lira, self).__init__(*args, **kwargs)
        self.device_addr = device_addr
        self.akkord = False #какой верный аккорд сейчас выставлен

    def on_start(self):
        self.device = DummyDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.as_input()

    def activate(self):
        self.device.subscribe(
            'stat.inputs',
            lambda data: self.proxy().hit(data[0]),
            500
        ).get()

    def deactivate(self):
        self.device.unsubscribe('stat.inputs')
        self.unsubscribe('akkord_1')
        self.unsubscribe('akkord_2')
        self.unsubscribe('akkord_3')
        self.unsubscribe('akkord_4')
        self.unsubscribe('akkord_wrong')

    def hit(self, state):
        if state & int('10000', 2):
            print bin(state)
            if state & int('1', 2):
                self.trigger('akkord_2')
            elif state & int('10', 2):
                self.trigger('akkord_1')
            elif state & int('100', 2):
                self.trigger('akkord_3')
            elif state & int('1000', 2):
                self.trigger('akkord_4')
            else:
                self.trigger('akkord_wrong')

    def akkord_1(self):
        self.trigger('akkord_1')

    def akkord_2(self):
        self.trigger('akkord_2')

    def akkord_3(self):
        self.trigger('akkord_3')

    def akkord_4(self):
        self.trigger('akkord_4')

    def akkord_wrong(self):
        self.trigger('akkord_wrong')

    def stopall(self):
        self.device.stop()


class Velosiped(Puzzle):
    glossary_name = u"Велосипед"
    glossary_code = u"G1R1O6"

    def __init__(self, device_addr, *args, **kwargs):
        super(Velosiped, self).__init__(*args, **kwargs)
        self.device_addr = device_addr

    def on_start(self):
        self.device = DummyDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])

    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.as_input()

    def activate(self):
        self.device.subscribe(
            'stat.inputs',
            lambda data: self.proxy().charged(data[0]),
            200
        ).get()

    def deactivate(self):
        self.device.unsubscribe('stat.inputs')

    def charged(self, state):
        if state != int('11111111', 2):
            states = {
                0   : 8,
                1   : 7,
                3   : 6,
                7   : 5,
                15  : 4,
                31  : 3,
                63  : 2,
                127 : 1,
            }
            print 'charged_' + str(states[state])
            self.trigger('charged_' + str(states[state]))

    def stopall(self):
        self.device.stop()

