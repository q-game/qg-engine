#! ../env/bin/python
# encoding: utf-8
import time
from datetime import datetime, timedelta
from pprint import pprint
import gevent

from pykka.gevent import GeventActor as Actor

from engine import Events, Resources, run_game, DummyDevice, StekloliftDevice
from utils import shex
from room1 import Dveri, Avtoportret, Proem, Dama, Lira, Velosiped
from room2 import (
    Gorod, Diafragma, Gorgona, Puteshestvie, Zvezdi, Chelovek,
    Vitrag, Tumba, Tank, KluchOtTanka, Knopka, DiafrSvet
)
from room2 import TUMBA1_OPEN_PRF, TUMBA1_CLOSE_PRF
from room2 import TUMBA2_OPEN_PRF, TUMBA2_CLOSE_PRF
from room2 import TUMBA3_OPEN_PRF, TUMBA3_CLOSE_PRF
from room2 import TUMBA4_OPEN_PRF, TUMBA4_CLOSE_PRF
from room3 import Okoshko, Fakels, Ricar
from room4 import Lustra, TimeMachine

from utils import ModifyFilter, PUBHandler, log_formatter, load_device_addrs
import logging
log = logging.getLogger('ENG')
log.setLevel(logging.DEBUG)
log.addFilter(ModifyFilter())

rm1_log = logging.getLogger('[rm1]')
rm1_log.setLevel(logging.DEBUG)
rm1_log.addFilter(ModifyFilter(prefix='[rm1]', name='ENG'))

rm2_log = logging.getLogger('[rm2]')
rm2_log.setLevel(logging.DEBUG)
rm2_log.addFilter(ModifyFilter(prefix='[rm2]', name='ENG'))

console_handler = logging.StreamHandler()
console_handler.setFormatter(log_formatter)
log.addHandler(console_handler)
rm1_log.addHandler(console_handler)
rm2_log.addHandler(console_handler)

def init(puzzle, *args, **kwargs):
    proxy = puzzle.start(*args, **kwargs).proxy()
    proxy.init().get()
    return proxy


dv_addrs = load_device_addrs()['davinchi']

def get_gui_controls(proxy):
    ret = []
    controls = [method for method in dir(proxy._actor) if (callable(getattr(proxy._actor, method)) and getattr(getattr(proxy._actor, method), 'gui_control', None))]
    for control in controls:
        ret.append({
            'method': control,
            'descr': getattr(proxy._actor, control).__doc__
        })

    return ret



class Room1(object):
    def __init__(self):
        dv = dv_addrs['room1']
        self.inited = False
        self.puzzles = []
        self.dveri = Dveri.start(dv['dveri']).proxy()
        self.proem = Proem.start(addrs=[
            dv['proem']['latch1'],
            dv['proem']['latch2'],
            dv['proem']['latch3'],
            dv['proem']['latch4'],
            dv['proem']['latch5'],
            dv['proem']['latch6'],
        ]).proxy()
        self.avtoportret = Avtoportret.start(dv['avtoportret']).proxy()
        self.dama = Dama.start(dv['dama']).proxy()
        self.lira = Lira.start(dv['lira']).proxy()
        self.velo = Velosiped.start(dv['velo']).proxy()
        self.puzzles = [
            self.dveri, self.proem, self.avtoportret,
            self.dama, self.lira, self.velo
        ]

    def init(self):
        #ACHTUNG: в этом методе только инициализировать загадки и девайсы
        # настроку проводить в методе setup_logic
        if self.inited:
            pass
        self.inited = True

        for puzzle in self.puzzles:
            puzzle.init().get()

    def stop(self):
        for puzzle in self.puzzles:
            puzzle.stop()


class Room2(object):
    def __init__(self):
        #import pudb; pu.db
        dv = dv_addrs['room2']

        self.inited = False
        self.puzzles = []

        self.gorod = Gorod.start(
            dv['gorod']['svet'],
            dv['gorod']['mechanic'],
            dv['gorod']['lift'],
        ).proxy()

        self.vitrag1 = Vitrag.start(1, dv['vitrag1'], main=1, part=0).proxy()
        self.vitrag2 = Vitrag.start(2, dv['vitrag2'], main=2, part=1).proxy()
        self.vitrag3 = Vitrag.start(3, dv['vitrag3'], main=2, part=3).proxy()
        self.vitrag4 = Vitrag.start(4, dv['vitrag4'], main=1, part=2).proxy()

        self.tumba1 = Tumba.start(1, dv['tumba1'], TUMBA1_OPEN_PRF, TUMBA1_CLOSE_PRF).proxy()
        self.tumba2 = Tumba.start(2, dv['tumba2'], TUMBA2_OPEN_PRF, TUMBA2_CLOSE_PRF).proxy()
        self.tumba3 = Tumba.start(3, dv['tumba3'], TUMBA3_OPEN_PRF, TUMBA3_CLOSE_PRF, 100).proxy()
        self.tumba4 = Tumba.start(4, dv['tumba4'], TUMBA4_OPEN_PRF, TUMBA4_CLOSE_PRF).proxy()

        self.kluch = KluchOtTanka.start(dv['kluchi_ot_tanka']).proxy()

        self.knopka = Knopka.start(dv['knopka_pod_stolom']).proxy()

        self.tank = Tank.start(dv['tank']).proxy()
        self.gorgona = Gorgona.start(dv['gorgona']).proxy()
        self.diafrag1 = Diafragma.start(1, dv['diafrag1']).proxy()
        self.put = Puteshestvie.start(dv['puteshestvie']).proxy()
        self.diafrag2 = Diafragma.start(2, dv['diafrag2']).proxy()
        self.zvezdi = Zvezdi.start(dv['zvezdi']).proxy()
        self.diafrag3 = Diafragma.start(3, dv['diafrag3']).proxy()
        self.chelovek = Chelovek.start(dv['chelovek']).proxy()
        self.diafrag4 = Diafragma.start(4, dv['diafrag4']).proxy()

        self.diafr_svet = DiafrSvet.start(dv['diafr_svet']).proxy()

        self.puzzles = [
            self.gorod,
            self.vitrag1, self.vitrag2, self.vitrag3, self.vitrag4,
            self.tumba1, self.tumba2, self.tumba3, self.tumba4,
            self.kluch, self.knopka,
            self.tank,
            self.gorgona, self.diafrag1,
            self.put, self.diafrag2,
            self.chelovek, self.diafrag3,
            self.zvezdi, self.diafrag4,
            self.diafr_svet
        ]

    def init(self):
        #ACHTUNG: в этом методе только инициализировать загадки и девайсы
        # настроку проводить в методе setup_logic
        if self.inited:
            pass
        self.inited = True

        for puzzle in self.puzzles:
            puzzle.init().get()


    def stop(self):
        for puzzle in self.puzzles:
            puzzle.stop()


class Room3(object):
    def __init__(self):
        dv = dv_addrs['room3']

        self.inited = False
        self.puzzles = []

        self.ricar = Ricar.start(dv['ricar']).proxy()
        self.fakels = Fakels.start(dv['fakels']).proxy()
        self.okoshko = Okoshko.start(dv['okoshko']).proxy()

        self.puzzles = [self.ricar, self.fakels, self.okoshko]


    def init(self):
        #ACHTUNG: в этом методе только инициализировать загадки и девайсы
        # настроку проводить в методе setup_logic
        if self.inited:
            pass
        self.inited = True

        for puzzle in self.puzzles:
            puzzle.init().get()


    def stop(self):
        for puzzle in self.puzzles:
            puzzle.stop()




class Room4(object):
    def __init__(self):
        dv = dv_addrs['room4']

        self.inited = False
        self.puzzles = []

        self.lustra = Lustra.start(dv['lustra']).proxy()
        self.time_machine = TimeMachine.start(dv['time_machine']).proxy()

        self.puzzles = [self.lustra, self.time_machine]


    def init(self):
        #ACHTUNG: в этом методе только инициализировать загадки и девайсы
        # настроку проводить в методе setup_logic
        if self.inited:
            pass
        self.inited = True

        for puzzle in self.puzzles:
            puzzle.init().get()


    def stop(self):
        for puzzle in self.puzzles:
            puzzle.stop()

DEBUG = True

class StepBase(Actor, Events):
    def __init__(self):
        #import pudb; pu.db
        super(StepBase, self).__init__()
        Events.__init__(self)
        self.failed = None
        self.solved = None
        self.PUB = Resources().require('PUB')
        self.scheduler = Resources().require('scheduler')
        self.solved_clbk = lambda: self.proxy().step_solved()

    def proxy(self):
        return self.actor_ref.proxy()

    def init(self):
        raise NotImplemented()

    def start_step(self):
        if not self.failed:
            try:
                self.setup_logic()
            except:
                self.trigger('fail')
                raise

    def setup_logic(self):
        raise NotImplemented()

    def log_that_failed(self):
        self.PUB.log(u"""
Кажется на этом шаге({0}), что-то может не работать, но на всякий случай пробуем его запустить.
Если ничего не будет происходить, попробуйте нажать кнопку 'Повторить шаг', а если это не поможет - кнопку 'След. шаг'.
""".format(self.name))

    def fail(self, pzl=None, resp=None):
        if not self.failed:
            self.PUB.log(u"Шаг провалился: {0}".format(self.name))
            self.PUB.log(u"""
Кажется на этом шаге({0}), что-то может не работать, но на всякий случай в процессе игры попробуем его запустить.
""".format(self.name))
            self.PUB.step_event({'id': self.step, 'status': 'failed'})
            self.failed = True
            self.trigger('fail')

    def step_solved(self):
        if self.failed:
            pass

        if not self.solved:
            #self.PUB.log(u"Шаг решен: {0}".format(self.name))
            self.PUB.step_event({'id': self.step, 'status': 'solved'})
            self.solved = True
            self.trigger('solved')

    def finishing(self):
        self.deactivate()

    def repeat(self):
        pass

    def deactivate(self):
        pass

    def _set_fail_handler(self, pzl):
        pzl.subscribe('fail', lambda data: self.proxy().fail(*data))


class Step(StepBase):
    pass


class StepTest(Step):
    name = u'Тестовый шаг'
    def __init__(self, step, room1, room2, room3, room4):
        super(Step, self).__init__()
        self.step = step
        self.room1 = room1
        self.room2 = room2
        self.room3 = room3
        self.room4 = room4

    def on_start(self):
        pass
        #for puzzle in [
            #self.room1.dama
        #]:
            #self._set_fail_handler(puzzle)

    def init(self):
        #self.room1.dama.init().get()
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        #self.PUB.controls('Управление Дамой с горностаем', [
            #{'method': 'room1.dama.open_lock', 'descr': u'Открыть тайник'},
            #{'method': 'room1.dama.close_lock', 'descr': u'Закрыть тайник'},
        #])

        self.PUB.controls('Изменение языка', [
            {'method': 'set_game_config_lang_rus', 'descr': u'Русский'},
            {'method': 'set_game_config_lang_eng', 'descr': u'Английский'},
        ])

        self.PUB.controls('Диктор: Человек', [
            {'method': 'play_chel', 'descr': u'Проиграть'},
        ])
        self.PUB.controls('Диктор: Горгона', [
            {'method': 'play_gorgona', 'descr': u'Проиграть'},
        ])
        self.PUB.controls('Диктор: Город', [
            {'method': 'play_gorod', 'descr': u'Проиграть'},
        ])
        self.PUB.controls('Диктор: Приветствие', [
            {'method': 'play_hello', 'descr': u'Проиграть'},
        ])
        self.PUB.controls('Диктор: Лира', [
            {'method': 'play_lira', 'descr': u'Проиграть'},
        ])
        self.PUB.controls('Диктор: Путешествие', [
            {'method': 'play_put', 'descr': u'Проиграть'},
        ])
        self.PUB.controls('Диктор: Рыцарь', [
            {'method': 'play_ricar', 'descr': u'Проиграть'},
        ])
        self.PUB.controls('Диктор: Звезды', [
            {'method': 'play_zvezdi', 'descr': u'Проиграть'},
        ])

        self.PUB.log(u"""
Если выше не вывелось никаких ошибок(красные записи), то можно переходить к следующему этапу 'Подготовка игры'.

Если ошибки были, то нужно связаться с кем-нибудь из электроннщиков или программистом.
""", log_level='tip')

        self.PUB.manual_controls('Управление Витражем 1', [
            {'method': 'room2.vitrag1.setup', 'descr': u'Активировать'},
            {'method': 'room2.vitrag1.main_light', 'descr': u'(вкл) Подсветка'},
            {'method': 'room2.vitrag1.main_off', 'descr': u'(выкл) Подсветка'},
            {'method': 'room2.vitrag1.akkord', 'descr': u'(вкл) Часть'},
            {'method': 'room2.vitrag1.part_off', 'descr': u'(выкл) Часть'},
        ])
        self.PUB.manual_controls('Управление Витражем 2', [
            {'method': 'room2.vitrag2.setup', 'descr': u'Активировать'},
            {'method': 'room2.vitrag2.main_light', 'descr': u'(вкл) Подсветка'},
            {'method': 'room2.vitrag2.main_off', 'descr': u'(выкл) Подсветка'},
            {'method': 'room2.vitrag2.akkord', 'descr': u'(вкл) Часть'},
            {'method': 'room2.vitrag2.part_off', 'descr': u'(выкл) Часть'},
        ])
        self.PUB.manual_controls('Управление Витражем 3', [
            {'method': 'room2.vitrag3.setup', 'descr': u'Активировать'},
            {'method': 'room2.vitrag3.main_light', 'descr': u'(вкл) Подсветка'},
            {'method': 'room2.vitrag3.main_off', 'descr': u'(выкл) Подсветка'},
            {'method': 'room2.vitrag3.akkord', 'descr': u'(вкл) Часть'},
            {'method': 'room2.vitrag3.part_off', 'descr': u'(выкл) Часть'},
        ])
        self.PUB.manual_controls('Управление Витражем 4', [
            {'method': 'room2.vitrag4.setup', 'descr': u'Активировать'},
            {'method': 'room2.vitrag4.main_light', 'descr': u'(вкл) Подсветка'},
            {'method': 'room2.vitrag4.main_off', 'descr': u'(выкл) Подсветка'},
            {'method': 'room2.vitrag4.akkord', 'descr': u'(вкл) Часть'},
            {'method': 'room2.vitrag4.part_off', 'descr': u'(выкл) Часть'},
        ])
        self.PUB.manual_controls('Управление Тумбой (напротив Путешествия)', [
            {'method': 'room2.tumba4.setup', 'descr': u'Активировать'},
            {'method': 'room2.tumba4.open', 'descr': u'Поднять'},
            {'method': 'room2.tumba4.stop_move', 'descr': u'Остановить'},
            {'method': 'room2.tumba4.close', 'descr': u'Опустить'},
        ])
        self.PUB.manual_controls('Управление Тумбой (напротив Звезд)', [
            {'method': 'room2.tumba3.setup', 'descr': u'Активировать'},
            {'method': 'room2.tumba3.open', 'descr': u'Поднять'},
            {'method': 'room2.tumba3.stop_move', 'descr': u'Остановить'},
            {'method': 'room2.tumba3.close', 'descr': u'Опустить'},
        ])
        self.PUB.manual_controls('Управление Тумбой (напротив Горгоны)', [
            {'method': 'room2.tumba2.setup', 'descr': u'Активировать'},
            {'method': 'room2.tumba2.open', 'descr': u'Поднять'},
            {'method': 'room2.tumba2.stop_move', 'descr': u'Остановить'},
            {'method': 'room2.tumba2.close', 'descr': u'Опустить'},
        ])
        self.PUB.manual_controls('Управление Тумбой (напротив Человека)', [
            {'method': 'room2.tumba1.setup', 'descr': u'Активировать'},
            {'method': 'room2.tumba1.open', 'descr': u'Поднять'},
            {'method': 'room2.tumba1.stop_move', 'descr': u'Остановить'},
            {'method': 'room2.tumba1.close', 'descr': u'Опустить'},
        ])
        self.PUB.manual_controls('Управление Люстрой', [
            {'method': 'room4.lustra.setup', 'descr': u'Активировать'},
            {'method': 'room4.lustra.left', 'descr': u'По часовой(левый факел)'},
            {'method': 'room4.lustra.move_stop', 'descr': u'Стоп'},
            {'method': 'room4.lustra.rigth', 'descr': u'Против часовой(правый факел)'},
        ])
        self.PUB.manual_controls('Управление подъемом Города', [
            {'method': 'room2.gorod.setup', 'descr': u'Активировать'},
            {'method': 'room2.gorod.lift_up_fast', 'descr': u'Поднять'},
            {'method': 'room2.gorod.lift_stop', 'descr': u'Остановить'},
            {'method': 'room2.gorod.lift_down_slow', 'descr': u'Опустить'},
        ])
        self.PUB.manual_controls('Управление светом в Танке', [
            {'method': 'room2.tank.setup', 'descr': u'Активировать'},
            {'method': 'room2.tank.open_vihod', 'descr': u'Зажечь'},
            {'method': 'room2.tank.close_vihod', 'descr': u'Погасить'},
        ])
        self.PUB.manual_controls('Изменение языка', [
            {'method': 'set_game_config_lang_rus', 'descr': u'Русский'},
            {'method': 'set_game_config_lang_eng', 'descr': u'Английский'},
        ])
        self.PUB.manual_controls('Диктор: Приветствие', [
            {'method': 'play_hello', 'descr': u'Проиграть'},
        ])
        self.PUB.manual_controls('Диктор: Горгона', [
            {'method': 'play_gorgona', 'descr': u'Проиграть'},
        ])
        self.PUB.manual_controls('Диктор: Человек', [
            {'method': 'play_chel', 'descr': u'Проиграть'},
        ])
        self.PUB.manual_controls('Диктор: Лира', [
            {'method': 'play_lira', 'descr': u'Проиграть'},
        ])
        self.PUB.manual_controls('Диктор: Путешествие', [
            {'method': 'play_put', 'descr': u'Проиграть'},
        ])
        self.PUB.manual_controls('Диктор: Звезды', [
            {'method': 'play_zvezdi', 'descr': u'Проиграть'},
        ])
        self.PUB.manual_controls('Диктор: Рыцарь', [
            {'method': 'play_ricar', 'descr': u'Проиграть'},
        ])
        self.PUB.manual_controls('Диктор: Город', [
            {'method': 'play_gorod', 'descr': u'Проиграть'},
        ])
        self.PUB.event({'enable': 'prepare_game'})

    def setup_logic(self):
        self.proxy().step_solved()


class StepInit(Step):
    name = u'Стартуем девайсы'
    def __init__(self, step, room1, room2, room3, room4):
        super(Step, self).__init__()
        self.step = step
        self.room1 = room1
        self.room2 = room2
        self.room3 = room3
        self.room4 = room4

    def on_start(self):
        for room in [self.room1, self.room2, self.room3, self.room4]:
            for puzzle in room.puzzles:
                self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.sound('soundtrack', repeat=True, volume=0.5)
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room1.init()
        self.room2.init()
        self.room3.init()
        self.room4.init()
        self.PUB.log(u"""
Если выше не вывелось никаких ошибок(красные записи), то можно переходить к следующему этапу 'Подготовка игры'.

Если ошибки были, то нужно связаться с кем-нибудь из электроннщиков или программистом.
""", log_level='tip')

        self.PUB.manual_controls('Управление Витражем 1', [
            {'method': 'room2.vitrag1.setup', 'descr': u'Активировать'},
            {'method': 'room2.vitrag1.main_light', 'descr': u'(вкл) Подсветка'},
            {'method': 'room2.vitrag1.main_off', 'descr': u'(выкл) Подсветка'},
            {'method': 'room2.vitrag1.akkord', 'descr': u'(вкл) Часть'},
            {'method': 'room2.vitrag1.part_off', 'descr': u'(выкл) Часть'},
        ])
        self.PUB.manual_controls('Управление Витражем 2', [
            {'method': 'room2.vitrag2.setup', 'descr': u'Активировать'},
            {'method': 'room2.vitrag2.main_light', 'descr': u'(вкл) Подсветка'},
            {'method': 'room2.vitrag2.main_off', 'descr': u'(выкл) Подсветка'},
            {'method': 'room2.vitrag2.akkord', 'descr': u'(вкл) Часть'},
            {'method': 'room2.vitrag2.part_off', 'descr': u'(выкл) Часть'},
        ])
        self.PUB.manual_controls('Управление Витражем 3', [
            {'method': 'room2.vitrag3.setup', 'descr': u'Активировать'},
            {'method': 'room2.vitrag3.main_light', 'descr': u'(вкл) Подсветка'},
            {'method': 'room2.vitrag3.main_off', 'descr': u'(выкл) Подсветка'},
            {'method': 'room2.vitrag3.akkord', 'descr': u'(вкл) Часть'},
            {'method': 'room2.vitrag3.part_off', 'descr': u'(выкл) Часть'},
        ])
        self.PUB.manual_controls('Управление Витражем 4', [
            {'method': 'room2.vitrag4.setup', 'descr': u'Активировать'},
            {'method': 'room2.vitrag4.main_light', 'descr': u'(вкл) Подсветка'},
            {'method': 'room2.vitrag4.main_off', 'descr': u'(выкл) Подсветка'},
            {'method': 'room2.vitrag4.akkord', 'descr': u'(вкл) Часть'},
            {'method': 'room2.vitrag4.part_off', 'descr': u'(выкл) Часть'},
        ])
        self.PUB.manual_controls('Управление Тумбой (напротив Путешествия)', [
            {'method': 'room2.tumba4.setup', 'descr': u'Активировать'},
            {'method': 'room2.tumba4.open', 'descr': u'Поднять'},
            {'method': 'room2.tumba4.close', 'descr': u'Опустить'},
            {'method': 'room2.tumba4.stop_move', 'descr': u'Остановить'},
        ])
        self.PUB.manual_controls('Управление Тумбой (напротив Звезд)', [
            {'method': 'room2.tumba3.setup', 'descr': u'Активировать'},
            {'method': 'room2.tumba3.open', 'descr': u'Поднять'},
            {'method': 'room2.tumba3.close', 'descr': u'Опустить'},
            {'method': 'room2.tumba3.stop_move', 'descr': u'Остановить'},
        ])
        self.PUB.manual_controls('Управление Тумбой (напротив Горгоны)', [
            {'method': 'room2.tumba2.setup', 'descr': u'Активировать'},
            {'method': 'room2.tumba2.open', 'descr': u'Поднять'},
            {'method': 'room2.tumba2.close', 'descr': u'Опустить'},
            {'method': 'room2.tumba2.stop_move', 'descr': u'Остановить'},
        ])
        self.PUB.manual_controls('Управление Тумбой (напротив Человека)', [
            {'method': 'room2.tumba1.setup', 'descr': u'Активировать'},
            {'method': 'room2.tumba1.open', 'descr': u'Поднять'},
            {'method': 'room2.tumba1.close', 'descr': u'Опустить'},
            {'method': 'room2.tumba1.stop_move', 'descr': u'Остановить'},
        ])
        self.PUB.manual_controls('Управление Люстрой', [
            {'method': 'room4.lustra.setup', 'descr': u'Активировать'},
            {'method': 'room4.lustra.left', 'descr': u'По часовой(левый факел)'},
            {'method': 'room4.lustra.rigth', 'descr': u'Против часовой(правый факел)'},
            {'method': 'room4.lustra.move_stop', 'descr': u'Стоп'},
        ])
        self.PUB.manual_controls('Управление подъемом Города', [
            {'method': 'room2.gorod.setup', 'descr': u'Активировать'},
            {'method': 'room2.gorod.lift_up_fast', 'descr': u'Поднять'},
            {'method': 'room2.gorod.lift_down_slow', 'descr': u'Опустить'},
            {'method': 'room2.gorod.lift_stop', 'descr': u'Остановить'},
        ])
        self.PUB.manual_controls('Управление светом в Танке', [
            {'method': 'room2.tank.setup', 'descr': u'Активировать'},
            {'method': 'room2.tank.open_vihod', 'descr': u'Зажечь'},
            {'method': 'room2.tank.close_vihod', 'descr': u'Погасить'},
        ])
        self.PUB.manual_controls('Изменение языка', [
            {'method': 'set_game_config_lang_rus', 'descr': u'Русский'},
            {'method': 'set_game_config_lang_eng', 'descr': u'Английский'},
        ])
        self.PUB.manual_controls('Диктор: Приветствие', [
            {'method': 'play_hello', 'descr': u'Проиграть'},
        ])
        self.PUB.manual_controls('Диктор: Горгона', [
            {'method': 'play_gorgona', 'descr': u'Проиграть'},
        ])
        self.PUB.manual_controls('Диктор: Человек', [
            {'method': 'play_chel', 'descr': u'Проиграть'},
        ])
        self.PUB.manual_controls('Диктор: Лира', [
            {'method': 'play_lira', 'descr': u'Проиграть'},
        ])
        self.PUB.manual_controls('Диктор: Путешествие', [
            {'method': 'play_put', 'descr': u'Проиграть'},
        ])
        self.PUB.manual_controls('Диктор: Звезды', [
            {'method': 'play_zvezdi', 'descr': u'Проиграть'},
        ])
        self.PUB.manual_controls('Диктор: Рыцарь', [
            {'method': 'play_ricar', 'descr': u'Проиграть'},
        ])
        self.PUB.manual_controls('Диктор: Город', [
            {'method': 'play_gorod', 'descr': u'Проиграть'},
        ])
        self.PUB.event({'enable': 'prepare_game'})

    def setup_logic(self):
        self.proxy().step_solved()


class StepIdle(Step):
    name = u'Режим ожидания'
    def __init__(self, step, room1):
        super(Step, self).__init__()
        self.step = step

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})

    def setup_logic(self):
        self.PUB.log(u"""
Комната находится в режиме ожидания. Т.е. на этом шаге ничего не будет происходить и сам собой он никогда не решится. Нажмите 'След. шаг', чтобы продолжить когда будете готовы.
""", log_level='legend')


    def deactivate(self):
        pass

    def repeat(self):
        pass


class StepVhod(Step):
    name = u'Вход'
    def __init__(self, step, room1):
        super(Step, self).__init__()
        self.step = step
        self.room1 = room1

    def on_start(self):
        for puzzle in [
            self.room1.dveri
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room1.dveri.init()

    def setup_logic(self):
        self.room1.dveri.setup()
        self.room1.dveri.close_vhod()
        self.room1.dveri.close_vihod()
        self.scheduler.schedule(self.solved_clbk, 1000, once=True)
        self.PUB.log(u"""
Входная и выходная дверь закрылись. Ничего не нажимайте, через 1 секунду систему перейдет к следующему шагу сама.
""", log_level='legend')
        self.PUB.controls('Изменение языка', [
            {'method': 'set_game_config_lang_rus', 'descr': u'Русский'},
            {'method': 'set_game_config_lang_eng', 'descr': u'Английский'},
        ])

    def deactivate(self):
        pass

    def repeat(self):
        self.scheduler.unschedule(self.solved_clbk)
        self.deactivate()
        self.setup_logic()


def strfdelta(tdelta, fmt="{h:02}:{m:02}:{s:02}"):
    d = {}
    d["h"], rem = divmod(tdelta.seconds, 3600)
    d["m"], d["s"] = divmod(rem, 60)
    return fmt.format(**d)

def time_now():
    return datetime.now().strftime("%H:%M:%S")

class StepStartTimer(Step):
    name = u'Старт таймера'
    def __init__(self, step):
        super(Step, self).__init__()
        self.step = step

    def init(self):
        self.start_time = datetime.now()
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})

    def setup_logic(self):
        self.PUB.log(u"""
Таймер начал отсчет. Через час, если игроки не успеют выйти из комнаты, вам будет предложено закончить игру и открыть вход.

Т.е. игра автоматически не завершится! Это должны сделать вы, если видно, что игроки точно не успеют выбраться за время чуть большее чем 1 час.

Через полчаса в интерфейс начнут выводится подсказки, сколько времени осталось.
""", log_level='legend')
        self.PUB.log(u"""

Время начала игры: {0}

""".format(self.start_time.strftime("%H:%M:%S")), log_level='legend')
        GAME = Resources().require('game')
        GAME.set_start_time(self.start_time)

        def timer_minutes(t):
            self.scheduler.schedule(lambda: self.PUB.log("""
[{0}] До конца игры осталось {1} минут.
""".format(time_now(), 60 - t)), t * 60 * 1000, once=True)

        timer_minutes(30)
        timer_minutes(45)
        timer_minutes(55)
        timer_minutes(59)

        def timer_seconds(t):
            self.scheduler.schedule(lambda: self.PUB.log("""
[{0}] До конца игры осталось {1} секунд.
""".format(time_now(), 60 - t)), 59 * 60 * 1000 + t * 1000, once=True)

        timer_seconds(30)

        GAME = Resources().require('game')
        self.scheduler.schedule(lambda: GAME.game_over(), 60 * 60 * 1000, once=True)
        self.scheduler.schedule(self.solved_clbk, 1000, once=True)

    def deactivate(self):
        pass

    def repeat(self):
        pass


class StepAvtoportret(Step):
    name = u'Автопортрет'
    def __init__(self, step, room1):
        super(Step, self).__init__()
        self.step = step
        self.room1 = room1

    def on_start(self):
        for puzzle in [
            self.room1.avtoportret
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room1.avtoportret.init()

    def setup_logic(self):
        self.PUB.log(u"""
Чтобы открыть Проем. Игрокам нужно решить Автопортрет - правильно выставить круги.
""", log_level='legend')
        self.room1.avtoportret.setup()
        self.room1.avtoportret.activate()
        self.room1.avtoportret.subscribe('solved',[
                self.solved_clbk
            ],
            once=True
        )


    def deactivate(self):
        self.room1.avtoportret.deactivate()

    def repeat(self):
        self.room1.avtoportret.unsubscribe('solved')
        self.deactivate()
        self.setup_logic()


class StepProem(Step):
    name = u'Открытие проема'
    def __init__(self, step, room1):
        super(Step, self).__init__()
        self.step = step
        self.room1 = room1
        self.stopped_not_by_sensor_clbk = lambda: self.PUB.log(u"""
В проеме не сработал датчик при открытии! Если проем открыт не до конца - нажмите кнопку 'Повторить шаг', чтобы запустить его заново. Если это не поможет нажмите 'След. шаг'.
""", log_level='error')

    def on_start(self):
        for puzzle in [ self.room1.proem ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room1.proem.init()

    def open_proem(self):
        self.room1.proem.setup()
        self.room1.proem.open_proem()

    def setup_logic(self):
        #self.scheduler.schedule(self.solved_clbk, 20000, once=True)
        self.open_proem()
        self.PUB.log(u"""
Проем открывается. При стандартном раскладе система сама перейдет к следующему шагу.
Если Проем застрянет - нажмите кнопку 'Повторить шаг'. Если 'Повторить шаг' не поможет, но Проем открылся достаточно для того, чтобы игроки могли пройти, - нажмите 'След. шаг'.
""", log_level='legend')
        self.room1.proem.subscribe('openned', self.solved_clbk)
        self.room1.proem.subscribe(
            'stopped_not_by_sensor', [
                self.stopped_not_by_sensor_clbk,
            ]
        )


    def deactivate(self):
        self.room1.proem.deactivate()

    def repeat(self):
        self.scheduler.unschedule(self.solved_clbk)
        self.deactivate()
        self.setup_logic()


class StepKnopka(Step):
    name = u'Кнопка под столом'
    def __init__(self, step, room2):
        super(Step, self).__init__()
        self.step = step
        self.room2 = room2
        self.hello_dictor_clbk = lambda: self.PUB.sound('hello'+get_config('lang'))

    def on_start(self):
        self.puzzles = [
            self.room2.knopka,
        ]
        for puzzle in self.puzzles:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        for puzzle in self.puzzles:
            puzzle.init()

    def setup_logic(self):
        self.PUB.log(u"""
Сейчас, чтобы открыть Даму с горностаем игрокам нужно будет найти Кнопку, которая находится под столом(под тем краем, где расположена загадка Звезды).
""", log_level='tip')
        self.hello_dictor_clbk()
        self.room2.knopka.setup()
        self.room2.knopka.activate()
        self.room2.knopka.subscribe('pushed',[
                self.solved_clbk
            ],
            once=True
        )

    def deactivate(self):
        self.room2.knopka.deactivate()

    def repeat(self):
        self.room2.knopka.unsubscribe('pushed')
        self.deactivate()
        self.setup_logic()


class StepDama(Step):
    name = u'Дама с горностаем'
    def __init__(self, step, room1):
        super(Step, self).__init__()
        self.step = step
        self.room1 = room1
        self.close_dama_clbk = lambda: self.proxy().close_dama()

    def on_start(self):
        self.puzzles = [
            self.room1.dama,
        ]
        for puzzle in self.puzzles:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        for puzzle in self.puzzles:
            puzzle.init()

    def open_dama(self):
        self.room1.dama.open_lock()
        self.PUB.log(u"""
Портрет с 'Дама с горностаем' открылся. Через 30 секунд откроется первая загадка.
""", log_level='tip')

    def schedule_close_dama(self):
        self.scheduler.schedule(self.close_dama_clbk, 5000, once=True)

    def close_dama(self):
        self.room1.dama.close_lock()
        self.schedule_solv()

    def schedule_solv(self):
        self.scheduler.schedule(lambda: self.proxy().step_solved(), 30000, once=True)

    def setup_logic(self):
        self.room1.dama.setup()
        self.open_dama()
        self.schedule_close_dama()

    def deactivate(self):
        self.room1.dama.deactivate()

    def repeat(self):
        self.scheduler.unschedule(self.close_dama_clbk)
        self.deactivate()
        self.setup_logic()


#step lifecycle
# - on_start()
# - failed.get()
# - log_that_failed()
# - subscribe('solved')
# - init()
# - start_step()
# - - setup_logic()
# ...
# - repeat()
# ...
# - unsubscribe('solved')
# - step_solved()
# - finishing()

class StepTablePuzzles(Step):
    name = u'Загадки в столе'
    def __init__(self, step, chel, tmb4, put, tmb3,
                             zvezdi, tmb2, gorgona, tmb1):
        super(Step, self).__init__()
        #print 'INIT'
        #import pudb; pu.db
        self.step = step
        self.steps = {
            'chel': chel,
            'tmb4': tmb4,
            'put': put,
            'tmb3': tmb3,
            'zvezdi': zvezdi,
            'tmb2': tmb2,
            'gorgona': gorgona,
            'tmb1': tmb1
        }
        self.solved_steps = set()

    def on_start(self):
        #print 'ON_START'
        #import pudb; pu.db
        self.steps['chel'].subscribe('solved', [
            lambda: self.proxy().finish_substep('chel'),
            lambda: self.proxy().setup_substep('tmb1'),
            lambda: self.PUB.log(u"""Загадка Человек решена""", log_level='legend'),
        ])
        self.steps['put'].subscribe('solved', [
            lambda: self.proxy().finish_substep('put'),
            lambda: self.proxy().setup_substep('tmb4'),
            lambda: self.PUB.log(u"""Загадка Путешествие решена""", log_level='legend'),
        ])
        self.steps['zvezdi'].subscribe('solved', [
            lambda: self.proxy().finish_substep('zvezdi'),
            lambda: self.proxy().setup_substep('tmb3'),
            lambda: self.PUB.log(u"""Загадка Звезды решена""", log_level='legend'),
        ])
        self.steps['gorgona'].subscribe('solved', [
            lambda: self.proxy().finish_substep('gorgona'),
            lambda: self.proxy().setup_substep('tmb2'),
            lambda: self.PUB.log(u"""Загадка Горгона решена""", log_level='legend'),
        ])

        self.steps['tmb1'].subscribe('solved', [
            lambda: self.proxy().finish_substep('tmb1'),
            lambda: self.proxy().substep_solved('tmb1'),
        ])
        self.steps['tmb2'].subscribe('solved', [
            lambda: self.proxy().finish_substep('tmb2'),
            lambda: self.proxy().substep_solved('tmb2'),
        ])
        self.steps['tmb3'].subscribe('solved', [
            lambda: self.proxy().finish_substep('tmb3'),
            lambda: self.proxy().substep_solved('tmb3'),
        ])
        self.steps['tmb4'].subscribe('solved', [
            lambda: self.proxy().finish_substep('tmb4'),
            lambda: self.proxy().substep_solved('tmb4'),
        ])

    def substep_solved(self, step):
        self.solved_steps.add(step)
        if self.solved_steps == set(['tmb1','tmb2','tmb3','tmb4']):
            self.trigger('solved')

    def init(self):
        #import pudb; pu.db
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        for step in self.steps.values():
            step.init()

    def setup_logic(self):
        self.PUB.log(u"""Выбрана 'Игра 4+', поэтому будет открыт доступ ко всем загадкам в столе.""", log_level='tip')
        self.steps['chel'].setup_logic()
        self.steps['zvezdi'].setup_logic()
        self.steps['put'].setup_logic()
        self.steps['gorgona'].setup_logic()
        self.PUB.controls('Как бы решить загадку:', [
            {'method': 'room2.chelovek.solve_puzzle', 'descr': u'Человек'},
            {'method': 'room2.put.solve_puzzle', 'descr': u'Путешествие'},
            {'method': 'room2.zvezdi.solve_puzzle', 'descr': u'Звезды'},
            {'method': 'room2.gorgona.solve_puzzle', 'descr': u'Горгона'},
        ])

    def setup_substep(self, substep):
        step = self.steps[substep]
        step.setup_logic()

    def finish_substep(self, substep):
        step = self.steps[substep]
        step.unsubscribe('solved')
        if not step.failed.get():
            step.step_solved()
        step.finishing()


class StepChelovek(Step):
    name = u'Человек'
    def __init__(self, step, room1, room2, mute_dictor=False):
        super(Step, self).__init__()
        self.step = step
        self.room1 = room1
        self.room2 = room2
        self.mute_dictor = mute_dictor

        self.vitragy_light_clbk = lambda: self.proxy().vitragy_light()
        self.chel_dictor_clbk = lambda: self.PUB.sound('chel'+get_config('lang'))
        self.lira_dictor_clbk = lambda: self.PUB.sound('lira'+get_config('lang'))
        self.akkords = [False, False, False, False]

    def on_start(self):
        self.puzzles = [
            self.room2.diafrag4,
            self.room2.chelovek,
            self.room1.lira,
            self.room2.vitrag1, self.room2.vitrag2,
            self.room2.vitrag3, self.room2.vitrag4,
        ]
        for puzzle in self.puzzles:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        for puzzle in self.puzzles:
            puzzle.init()

    def vitragy_light(self):
        self.room2.vitrag1.main_light()
        self.room2.vitrag2.main_light()
        self.room2.vitrag3.main_light()
        self.room2.vitrag4.main_light()

    def akkord(self, clbk, number):
        self.akkords[number-1] = True
        clbk()
        if len(filter(None, self.akkords)) > 1:
            self.room2.chelovek.activate()

    def setup_logic(self):
        self.PUB.log(u"""
Загадка Человек.
""", log_level='legend')
        self.PUB.log(u"""
Чтобы решить загадку Человек, игрокам нужно использовать Лиру и Витражи.
""", log_level='tip')
        self.room2.vitrag1.setup()
        self.room2.vitrag2.setup()
        self.room2.vitrag3.setup()
        self.room2.vitrag4.setup()
        self.room2.chelovek.setup()
        if not self.mute_dictor:
            self.room2.chelovek.subscribe('solved',[
                    self.chel_dictor_clbk,
                    self.solved_clbk,
                ],
                once=True
            )
        else:
            self.room2.chelovek.subscribe('solved',[
                    self.solved_clbk,
                ],
                once=True
            )

        self.room2.diafrag4.open()

        self.room1.lira.setup()
        self.room1.lira.activate()
        lira = self.room1.lira
        v1 = self.room2.vitrag1
        v2 = self.room2.vitrag2
        v3 = self.room2.vitrag3
        v4 = self.room2.vitrag4

        lira.subscribe(
            'akkord_1',[
                lambda: self.proxy().akkord(v1.akkord, 1),
                lambda: self.PUB.log(u"Лира: аккорд 1", log_level='legend'),
            ]
        )
        lira.subscribe(
            'akkord_2',[
                lambda: self.proxy().akkord(v2.akkord, 2),
                lambda: self.PUB.log(u"Лира: аккорд 2", log_level='legend'),
            ]
        )
        lira.subscribe(
            'akkord_3',[
                lambda: self.proxy().akkord(v3.akkord, 3),
                lambda: self.PUB.log(u"Лира: аккорд 3", log_level='legend'),
            ]
        )
        lira.subscribe(
            'akkord_4',[
                lambda: self.proxy().akkord(v4.akkord, 4),
                lambda: self.PUB.log(u"Лира: аккорд 4", log_level='legend'),
            ]
        )

        lira.subscribe(
            'akkord_wrong', [
                lambda: v1.wrong_akkord(),
                lambda: v2.wrong_akkord(),
                lambda: v3.wrong_akkord(),
                lambda: v4.wrong_akkord(),
            ]
        )

        self.scheduler.schedule(self.vitragy_light_clbk, 10000, once=True)
        if not self.mute_dictor:
            self.scheduler.schedule(self.lira_dictor_clbk, 5 * 1000, once=True)

        self.room2.diafr_svet.setup()
        self.room2.diafr_svet.chel()

        self.PUB.controls('Управление Витражем 1', [
            {'method': 'room2.vitrag1.main_light', 'descr': u'(вкл) Подсветка'},
            {'method': 'room2.vitrag1.main_off', 'descr': u'(выкл) Подсветка'},
            {'method': 'room2.vitrag1.akkord', 'descr': u'(вкл) Часть'},
            {'method': 'room2.vitrag1.part_off', 'descr': u'(выкл) Часть'},
        ])
        self.PUB.controls('Управление Витражем 2', [
            {'method': 'room2.vitrag2.main_light', 'descr': u'(вкл) Подсветка'},
            {'method': 'room2.vitrag2.main_off', 'descr': u'(выкл) Подсветка'},
            {'method': 'room2.vitrag2.akkord', 'descr': u'(вкл) Часть'},
            {'method': 'room2.vitrag2.part_off', 'descr': u'(выкл) Часть'},
        ])
        self.PUB.controls('Управление Витражем 3', [
            {'method': 'room2.vitrag3.main_light', 'descr': u'(вкл) Подсветка'},
            {'method': 'room2.vitrag3.main_off', 'descr': u'(выкл) Подсветка'},
            {'method': 'room2.vitrag3.akkord', 'descr': u'(вкл) Часть'},
            {'method': 'room2.vitrag3.part_off', 'descr': u'(выкл) Часть'},
        ])
        self.PUB.controls('Управление Витражем 4', [
            {'method': 'room2.vitrag4.main_light', 'descr': u'(вкл) Подсветка'},
            {'method': 'room2.vitrag4.main_off', 'descr': u'(выкл) Подсветка'},
            {'method': 'room2.vitrag4.akkord', 'descr': u'(вкл) Часть'},
            {'method': 'room2.vitrag4.part_off', 'descr': u'(выкл) Часть'},
        ])
        self.PUB.controls('Эмуляция Лиры', [
            {'method': 'room1.lira.akkord_1', 'descr': u'Аккорд 1'},
            {'method': 'room1.lira.akkord_2', 'descr': u'Аккорд 2'},
            {'method': 'room1.lira.akkord_3', 'descr': u'Аккорд 3'},
            {'method': 'room1.lira.akkord_4', 'descr': u'Аккорд 4'},
            {'method': 'room1.lira.akkord_wrong', 'descr': u'Аккорд (неправильный)'},
        ])

    def deactivate(self):
        self.room2.diafr_svet.chel(None)
        self.room1.lira.deactivate()
        self.room2.chelovek.deactivate()

    def repeat(self):
        self.scheduler.unschedule(self.vitragy_light_clbk)
        self.scheduler.unschedule(self.chel_dictor_clbk)
        self.room2.chelovek.subscribe('solved')
        lira = self.room1.lira
        lira.unsubscribe('akkord_1')
        lira.unsubscribe('akkord_2')
        lira.unsubscribe('akkord_3')
        lira.unsubscribe('akkord_4')
        lira.unsubscribe('akkord_wrong')
        self.deactivate()
        self.setup_logic()


class StepTumba4(Step):
    name = u'Тумбa 4'
    def __init__(self, step, room2):
        super(Step, self).__init__()
        self.step = step
        self.room2 = room2
        self.stopped_not_by_sensor_clbk = lambda: self.PUB.log(u"""
Тумба остановилась не по датчику! Нажмите кнопку 'Повторить шаг', чтобы запустить ее заново (либо кнопку 'Поднять' в 'Управлении Тумбой'). Если это не поможет - нажмите 'След. шаг', но учтите, что если Тумба не открылась, у игроков не будет одного Ключа.
""", log_level='error')

    def on_start(self):
        for puzzle in [
            self.room2.tumba4,
            self.room2.kluch
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room2.tumba4.init()

    def start_tumba_4(self):
        self.room2.tumba4.activate()
        self.room2.tumba4.open()

    def setup_logic(self):
        self.room2.tumba4.setup()
        self.proxy().start_tumba_4()
        self.room2.tumba4.subscribe(
            'stopped_by_sensor', [
                self.solved_clbk,
            ]
        )
        self.room2.tumba4.subscribe(
            'stopped_not_by_sensor', [
                self.stopped_not_by_sensor_clbk,
            ]
        )

        self.PUB.log(u"""
Тумба поднимается.
""", log_level='legend')
        self.PUB.controls('Управление Тумбой (напротив Путешествия)', [
            {'method': 'room2.tumba4.open', 'descr': u'Поднять'},
            {'method': 'room2.tumba4.stop_move', 'descr': u'Остановить'},
            {'method': 'room2.tumba4.close', 'descr': u'Опустить'},
        ])
        self.scheduler.schedule(self.solved_clbk, 7000, once=True)

    def deactivate(self):
        self.room2.tumba4.deactivate()
        self.room2.tumba4.unsubscribe('stopped_by_sensor')
        self.room2.tumba4.unsubscribe('stopped_not_by_sensor')


    def repeat(self):
        self.scheduler.unschedule(self.solved_clbk)
        self.deactivate()
        self.setup_logic()


class StepKluch(Step):
    def __init__(self, step, room2, number):
        super(Step, self).__init__()
        self.step = step
        self.number = number
        self.room2 = room2
        self.name = u'Ключ {0}'.format(self.number)

    def on_start(self):
        for puzzle in [
            self.room2.kluch
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room2.kluch.init()

    def setup_logic(self):
        self.PUB.log(u"""
Ключ, который теперь можно достать у основания Тумбы, нужно вставить в Ключи от танка(восьмигранник под кондиционером)
При этом в него должны быть вставлены все ключи, который уже есть у игроков(если есть)
""", log_level='legend')
        self.room2.kluch.setup()
        self.room2.kluch.activate()

        self.room2.kluch.subscribe(
            'kluch_0', [
                lambda: self.PUB.log(u"Вставлено ключей: 0",log_level='legend' )
            ],
            once=True
        )
        self.room2.kluch.subscribe(
            'kluch_1', [
                lambda: self.PUB.log(u"Вставлено ключей: 1",log_level='legend' )
            ],
            once=True
        )
        self.room2.kluch.subscribe(
            'kluch_2', [
                lambda: self.PUB.log(u"Вставлено ключей: 2",log_level='legend' )
            ],
            once=True
        )
        self.room2.kluch.subscribe(
            'kluch_3', [
                lambda: self.PUB.log(u"Вставлено ключей: 3",log_level='legend' )
            ],
            once=True
        )
        self.room2.kluch.subscribe(
            'kluch_4', [
                lambda: self.PUB.log(u"Вставлено ключей: 4",log_level='legend' )
            ],
            once=True
        )

        self.room2.kluch.subscribe(
            'kluch_{0}'.format(self.number), [
                self.solved_clbk,
            ],
            once=True
        )

    def deactivate(self):
        self.room2.kluch.deactivate()

    def repeat(self):
        self.room2.kluch.unsubscribe('kluch_{0}'.format(self.number))
        self.deactivate()
        self.setup_logic()


class StepPut(Step):
    name = u'Путешествие'
    def __init__(self, step, room2, mute_dictor=False):
        super(Step, self).__init__()
        self.step = step
        self.room2 = room2
        self.mute_dictor = mute_dictor
        self.put_dictor_clbk = lambda: self.PUB.sound('put'+get_config('lang'))

    def on_start(self):
        self.puzzles = [
            self.room2.diafrag3,
            self.room2.put,
        ]
        for puzzle in self.puzzles:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        for puzzle in self.puzzles:
            puzzle.init()

    def setup_logic(self):
        self.PUB.log(u"""
Загадка Путешествие.
""", log_level='legend')
        self.room2.put.setup()
        self.room2.put.reset()
        self.room2.put.activate()
        self.room2.put.subscribe('solved',[
                self.solved_clbk
            ],
            once=True
        )
        self.room2.diafrag3.open()

        self.room2.diafr_svet.put()

        if not self.mute_dictor:
            self.scheduler.schedule(self.put_dictor_clbk, 15 * 1000, once=True)

    def deactivate(self):
        self.room2.diafr_svet.put(None)
        self.room2.put.deactivate()

    def repeat(self):
        self.scheduler.unschedule(self.put_dictor_clbk)
        self.room2.put.unsubscribe('solved')
        self.deactivate()
        self.setup_logic()


class StepTumba3(Step):
    name = u'Тумбa 3'
    def __init__(self, step, room2):
        super(Step, self).__init__()
        self.step = step
        self.room2 = room2
        self.stopped_not_by_sensor_clbk = lambda: self.PUB.log(u"""
Тумба остановилась не по датчику! Нажмите кнопку 'Повторить шаг', чтобы запустить ее заново (либо кнопку 'Поднять' в 'Управлении Тумбой'). Если это не поможет нажмите 'След. шаг', но учтите, что если Тумба не открылась у игроков не будет одного Ключа.
""", log_level='error')

    def on_start(self):
        for puzzle in [
            self.room2.tumba3,
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room2.tumba3.init()

    def start_tumba_3(self):
        self.room2.tumba3.activate()
        self.room2.tumba3.open()


    def setup_logic(self):
        self.room2.tumba3.setup()
        self.proxy().start_tumba_3()
        self.room2.tumba3.subscribe(
            'stopped_by_sensor', [
                self.solved_clbk,
            ]
        )
        self.room2.tumba3.subscribe(
            'stopped_not_by_sensor', [
                self.stopped_not_by_sensor_clbk,
            ]
        )

        self.PUB.log(u"""
Тумба поднимается.
""", log_level='legend')

        self.PUB.controls('Управление Тумбой (напротив Звезд)', [
            {'method': 'room2.tumba3.open', 'descr': u'Поднять'},
            {'method': 'room2.tumba3.stop_move', 'descr': u'Остановить'},
            {'method': 'room2.tumba3.close', 'descr': u'Опустить'},
        ])
        self.scheduler.schedule(self.solved_clbk, 7000, once=True)

    def deactivate(self):
        self.room2.tumba3.deactivate()
        self.room2.tumba3.unsubscribe('stopped_by_sensor')
        self.room2.tumba3.unsubscribe('stopped_not_by_sensor')


    def repeat(self):
        self.scheduler.unschedule(self.solved_clbk)
        self.deactivate()
        self.setup_logic()


class StepZvezdi(Step):
    name = u'Звезды'
    def __init__(self, step, room2, mute_dictor=False):
        super(Step, self).__init__()
        self.step = step
        self.room2 = room2
        self.mute_dictor = mute_dictor
        self.zvezdi_dictor_clbk = lambda: self.PUB.sound('zvezdi'+get_config('lang'))

    def on_start(self):
        self.puzzles = [
            self.room2.diafrag2,
            self.room2.zvezdi,
        ]
        for puzzle in self.puzzles:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        for puzzle in self.puzzles:
            puzzle.init()

    def setup_logic(self):
        self.PUB.log(u"""
Загадка Звезды.
""", log_level='legend')
        self.room2.zvezdi.setup()
        self.room2.zvezdi.activate()
        self.room2.zvezdi.subscribe('solved',[
                self.solved_clbk
            ],
            once=True
        )
        self.room2.diafrag2.open()

        self.room2.diafr_svet.zvezdi()

        if not self.mute_dictor:
            self.scheduler.schedule(self.zvezdi_dictor_clbk, 5 * 1000, once=True)

    def deactivate(self):
        self.room2.diafr_svet.zvezdi(None)
        self.room2.zvezdi.deactivate()

    def repeat(self):
        self.scheduler.unschedule(self.zvezdi_dictor_clbk)
        self.room2.zvezdi.unsubscribe('solved')
        self.deactivate()
        self.setup_logic()


class StepTumba2(Step):
    name = u'Тумбa 2'
    def __init__(self, step, room2):
        super(Step, self).__init__()
        self.step = step
        self.room2 = room2
        self.stopped_not_by_sensor_clbk = lambda: self.PUB.log(u"""
Тумба остановилась не по датчику! Нажмите кнопку 'Повторить шаг', чтобы запустить ее заново (либо кнопку 'Поднять' в 'Управлении Тумбой'). Если это не поможет нажмите 'След. шаг', но учтите, что если Тумба не открылась у игроков не будет одного Ключа.
""", log_level='error')

    def on_start(self):
        for puzzle in [
            self.room2.tumba2,
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room2.tumba2.init()

    def start_tumba_2(self):
        self.room2.tumba2.activate()
        self.room2.tumba2.open()

    def setup_logic(self):
        self.room2.tumba2.setup()
        self.proxy().start_tumba_2()
        self.room2.tumba2.subscribe(
            'stopped_by_sensor', [
                self.solved_clbk,
            ]
        )
        self.room2.tumba2.subscribe(
            'stopped_not_by_sensor', [
                self.stopped_not_by_sensor_clbk,
            ]
        )

        self.PUB.log(u"""
Тумба поднимается.
""", log_level='legend')

        self.PUB.controls('Управление Тумбой (напротив Горгоны)', [
            {'method': 'room2.tumba2.open', 'descr': u'Поднять'},
            {'method': 'room2.tumba2.stop_move', 'descr': u'Остановить'},
            {'method': 'room2.tumba2.close', 'descr': u'Опустить'},
        ])
        self.scheduler.schedule(self.solved_clbk, 7000, once=True)

    def deactivate(self):
        self.room2.tumba2.deactivate()
        self.room2.tumba2.unsubscribe('stopped_by_sensor')
        self.room2.tumba2.unsubscribe('stopped_not_by_sensor')

    def repeat(self):
        self.scheduler.unschedule(self.solved_clbk)
        self.deactivate()
        self.setup_logic()


class StepGorgona(Step):
    name = u'Горгона'
    def __init__(self, step, room2, mute_dictor=False):
        super(Step, self).__init__()
        self.step = step
        self.room2 = room2
        self.mute_dictor = mute_dictor
        self.gorgona_dictor_clbk = lambda: self.PUB.sound('gorgona'+get_config('lang'))

    def on_start(self):
        self.puzzles = [
            self.room2.diafrag1,
            self.room2.gorgona,
        ]
        for puzzle in self.puzzles:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        for puzzle in self.puzzles:
            puzzle.init()

    def setup_logic(self):
        self.PUB.log(u"""
Загадка Горгона.
""", log_level='legend')
        self.room2.gorgona.setup()
        self.room2.gorgona.activate()
        self.room2.gorgona.subscribe('solved',[
                self.solved_clbk
            ],
            once=True
        )
        self.room2.diafrag1.open()

        self.room2.diafr_svet.gorgona()

        if not self.mute_dictor:
            self.scheduler.schedule(self.gorgona_dictor_clbk, 5 * 1000, once=True)

    def deactivate(self):
        self.room2.diafr_svet.gorgona(None)
        self.room2.gorgona.deactivate()

    def repeat(self):
        self.scheduler.unschedule(self.gorgona_dictor_clbk)
        self.room2.zvezdi.unsubscribe('solved')
        self.deactivate()
        self.setup_logic()


class StepTumba1(Step):
    name = u'Тумбa 1'
    def __init__(self, step, room2):
        super(Step, self).__init__()
        self.step = step
        self.room2 = room2
        self.stopped_not_by_sensor_clbk = lambda: self.PUB.log(u"""
Тумба остановилась не по датчику! Нажмите кнопку 'Повторить шаг', чтобы запустить ее заново (либо кнопку 'Поднять' в 'Управлении Тумбой'). Если это не поможет нажмите 'След. шаг', но учтите, что если Тумба не открылась у игроков не будет одного Ключа.
""", log_level='error')

    def on_start(self):
        for puzzle in [
            self.room2.tumba1,
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room2.tumba1.init()

    def start_tumba_1(self):
        self.room2.tumba1.activate()
        self.room2.tumba1.open()


    def setup_logic(self):
        self.room2.tumba1.setup()
        self.proxy().start_tumba_1()
        self.room2.tumba1.subscribe(
            'stopped_by_sensor', [
                self.solved_clbk,
            ]
        )
        self.room2.tumba1.subscribe(
            'stopped_not_by_sensor', [
                self.stopped_not_by_sensor_clbk,
            ]
        )

        self.PUB.log(u"""
Тумба поднимается.
""", log_level='legend')

        self.PUB.controls('Управление Тумбой (напротив Человека)', [
            {'method': 'room2.tumba1.open', 'descr': u'Поднять'},
            {'method': 'room2.tumba1.stop_move', 'descr': u'Остановить'},
            {'method': 'room2.tumba1.close', 'descr': u'Опустить'},
        ])
        self.scheduler.schedule(self.solved_clbk, 7000, once=True)

    def deactivate(self):
        self.room2.tumba1.deactivate()
        self.room2.tumba1.unsubscribe('stopped_by_sensor')
        self.room2.tumba1.unsubscribe('stopped_not_by_sensor')

    def repeat(self):
        self.deactivate()
        self.setup_logic()


class StepRicarDiktor(Step):
    name = u'Рассказ о рыцаре'
    def __init__(self, step):
        super(Step, self).__init__()
        self.step = step
        self.ricar_dictor_clbk = lambda: self.PUB.sound('ricar'+get_config('lang'))

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})

    def setup_logic(self):
        self.PUB.log(u"""
Сейчас диктор будет рассказывать(примерно 16 секунд) игрокам про Рыцаря.
После этого откроется Вход в танк.
""", log_level='legend')
        self.ricar_dictor_clbk()
        self.scheduler.schedule(self.solved_clbk, 16000, once=True)

    def deactivate(self):
        pass

    def repeat(self):
        self.scheduler.unschedule(self.solved_clbk)
        self.deactivate()
        self.setup_logic()



class StepTankVhod(Step):
    name = u'Вход в танк'
    def __init__(self, step, room2):
        super(Step, self).__init__()
        self.step = step
        self.room2 = room2

    def on_start(self):
        for puzzle in [
            self.room2.tank
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room2.tank.init()

    def setup_logic(self):
        self.PUB.log(u"""
Вход в танк открыт.
""", log_level='legend')
        self.room2.tank.setup()
        self.room2.tank.open_vhod()
        self.solved_clbk()

    def deactivate(self):
        pass

    def repeat(self):
        self.scheduler.unschedule(self.solved_clbk)
        self.deactivate()
        self.setup_logic()


class StepRicar(Step):
    name = u'Рыцарь'
    def __init__(self, step, room3, room4):
        super(Step, self).__init__()
        self.step = step
        self.room3 = room3
        self.room4 = room4

    def on_start(self):
        for puzzle in [
            self.room3.fakels,
            self.room3.ricar,
            self.room4.lustra
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room3.fakels.init()
        self.room3.ricar.init()
        self.room4.lustra.init()

    def setup_logic(self):
        self.PUB.log(u"""
Загадка с Рыцарем.
""", log_level='legend')
        self.room3.fakels.setup()
        self.room3.ricar.setup()
        self.room4.lustra.setup()

        self.room3.fakels.subscribe(
            'left', [
                lambda: self.room4.lustra.left()
            ]
        )
        self.room3.fakels.subscribe(
            'rigth', [
                lambda: self.room4.lustra.rigth()
            ]
        )
        self.room3.fakels.subscribe(
            'stop', [
                lambda: self.room4.lustra.move_stop()
            ]
        )
        self.room3.ricar.subscribe(
            'solved', [
                lambda: self.proxy().step_solved(),
            ]
        )
        self.room3.ricar.activate()
        self.room3.fakels.activate()
        self.PUB.controls('Управление Люстрой', [
            {'method': 'room4.lustra.left', 'descr': u'По часовой(левый факел)'},
            {'method': 'room4.lustra.move_stop', 'descr': u'Стоп'},
            {'method': 'room4.lustra.rigth', 'descr': u'Против часовой(правый факел)'},
        ])


    def deactivate(self):
        self.room3.fakels.deactivate()
        self.room3.ricar.deactivate()
        self.room3.fakels.unsubscribe('left')
        self.room3.fakels.unsubscribe('right')
        self.room3.fakels.unsubscribe('stop')
        self.room3.ricar.unsubscribe('solved')

    def repeat(self):
        self.deactivate()
        self.setup_logic()


class StepOkoshko(Step):
    name = u'Окошко'
    def __init__(self, step, room3):
        super(Step, self).__init__()
        self.step = step
        self.room3 = room3

    def on_start(self):
        for puzzle in [
            self.room3.okoshko,
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room3.okoshko.init()

    def setup_logic(self):
        self.PUB.log(u"""
Тайник в Окошке рядом с Рыцарем открыт.
""", log_level='legend')
        self.room3.okoshko.setup()
        self.room3.okoshko.open_tainik(),
        self.scheduler.schedule(self.solved_clbk, 3000, once=True)

    def deactivate(self):
        pass

    def repeat(self):
        self.scheduler.unschedule(self.solved_clbk)
        self.deactivate()
        self.setup_logic()


class StepVelo(Step):
    name = u'Велосипед'
    def __init__(self, step, room1, room2):
        super(Step, self).__init__()
        self.step = step
        self.room1 = room1
        self.room2 = room2

        self.full_charge = False
        self.stopped_not_by_sensor_clbk = lambda: self.PUB.log(u"""
Город остановился не по датчику!
Если Город не поднялся до конца - попросите игрока снова раскрутить велосипед.
Если Город полностью поднялся - нажмите 'След. шаг'.
""", log_level='error')

        #self.charged_clbk = lambda: self.proxy().charged_8()
        #self.charged_clbk_set = False

    def on_start(self):
        for puzzle in [
            self.room1.velo,
            self.room2.gorod
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room1.velo.init()
        self.room2.gorod.init()

    def charged_1(self):
        if not self.full_charge:
            self.room2.gorod.lift_stop()
            self.room2.gorod.svet_off()

    def charged_2(self):
        if not self.full_charge:
            #if not self.charged_clbk_set:
                #self.charged_clbk_set = True
                #self.scheduler.schedule(self.charged_clbk, 10000, once=True)

            self.room2.gorod.lift_up()
            self.room2.gorod.lift_speed(10)
            self.room2.gorod.svet_on([0])
            self.room2.gorod.svet_off([1,2,3,4,5,6,7])

    def charged_3(self):
        if not self.full_charge:
            self.room2.gorod.lift_up()
            self.room2.gorod.lift_speed(30)
            self.room2.gorod.svet_on([0,1])
            self.room2.gorod.svet_off([2,3,4,5,6,7])

    def charged_4(self):
        if not self.full_charge:
            self.room2.gorod.lift_up()
            self.room2.gorod.lift_speed(50)
            self.room2.gorod.svet_on([0,1,2])
            self.room2.gorod.svet_off([3,4,5,6,7])

    def charged_5(self):
        if not self.full_charge:
            self.room2.gorod.lift_up()
            self.room2.gorod.lift_speed(60)
            self.room2.gorod.svet_on([0,1,2,3])
            self.room2.gorod.svet_off([4,5,6,7])

    def charged_6(self):
        if not self.full_charge:
            self.room2.gorod.lift_up()
            self.room2.gorod.lift_speed(70)
            self.room2.gorod.svet_on([0,1,2,3,4])
            self.room2.gorod.svet_off([5,6,7])

    def charged_7(self):
        if not self.full_charge:
            self.room2.gorod.lift_up()
            self.room2.gorod.lift_speed(80)
            self.room2.gorod.svet_on([0,1,2,3,4,5])
            self.room2.gorod.svet_off([6,7])

    def charged_8(self):
        if not self.full_charge:
            self.full_charge = True
            self.solved_clbk()

    def setup_logic(self):
        self.PUB.log(u"""
Чтобы начать подъем Города, нужно раскрутить колесо Велосипеда(используя педаль, которая была в тайнике Окошка).
Чтобы Город продолжил подниматься сам - попросите игрока раскрутить колесо по сильнее. Тогда Город продолжит подниматься сам.
""", log_level='legend')
        self.room1.velo.setup()
        self.room2.gorod.setup()

        self.room1.velo.subscribe(
            'charged_1', [
                 lambda: self.proxy().charged_1(),
            ]
        )
        self.room1.velo.subscribe(
            'charged_2', [
                 lambda: self.proxy().charged_2(),
            ]
        )
        self.room1.velo.subscribe(
            'charged_3', [
                 lambda: self.proxy().charged_3(),
            ]
        )
        self.room1.velo.subscribe(
            'charged_4', [
                 lambda: self.proxy().charged_4(),
            ]
        )
        self.room1.velo.subscribe(
            'charged_5', [
                 lambda: self.proxy().charged_5(),
            ]
        )
        self.room1.velo.subscribe(
            'charged_6', [
                 lambda: self.proxy().charged_6(),
            ]
        )
        self.room1.velo.subscribe(
            'charged_7', [
                 lambda: self.proxy().charged_7(),
            ]
        )
        self.room1.velo.subscribe(
            'charged_8', [
                 lambda: self.proxy().charged_8(),
            ]
        )

        self.room2.gorod.subscribe(
            'stopped_not_by_sensor', [
                self.stopped_not_by_sensor_clbk,
            ]
        )
        self.room2.gorod.activate()
        self.room1.velo.activate()
        self.PUB.controls('Управление подъемом Города', [
            {'method': 'room2.gorod.lift_up_fast', 'descr': u'Поднять'},
            {'method': 'room2.gorod.lift_stop', 'descr': u'Остановить'},
            {'method': 'room2.gorod.lift_down_slow', 'descr': u'Опустить'},
        ])

    def deactivate(self):
        self.full_charge = False
        self.room1.velo.unsubscribe('charged_1')
        self.room1.velo.unsubscribe('charged_2')
        self.room1.velo.unsubscribe('charged_3')
        self.room1.velo.unsubscribe('charged_4')
        self.room1.velo.unsubscribe('charged_5')
        self.room1.velo.unsubscribe('charged_6')
        self.room1.velo.unsubscribe('charged_7')
        self.room1.velo.unsubscribe('charged_8')
        self.room1.velo.deactivate()
        self.room2.gorod.deactivate()

    def repeat(self):
        #self.scheduler.unschedule(self.charged_clbk)
        self.deactivate()
        self.setup_logic()


class StepGorodLift(Step):
    name = u'Подъем города'
    def __init__(self, step, room2):
        super(Step, self).__init__()
        self.step = step
        self.room2 = room2
        self.stopped_not_by_sensor_clbk = lambda: self.PUB.log(u"""
Город остановился не по датчику!
Если Город не поднялся до конца - нажмите 'Повторить шаг' или 'Поднять' в 'Управлении подъемом Города'.
Если Город полностью поднялся - нажмите 'След. шаг'.
""", log_level='error')

    def on_start(self):
        for puzzle in [
            self.room2.gorod
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room2.gorod.init()

    def setup_logic(self):
        self.PUB.log(u"""
Город поднимается. Ничего делать не нужно. Просто ждать, когда он поднимется.
""", log_level='legend')
        self.room2.gorod.setup()
        self.room2.gorod.activate()
        self.room2.gorod.subscribe(
            'solved', [
                 lambda: self.solved_clbk(),
            ]
        )
        self.room2.gorod.subscribe(
            'stopped_not_by_sensor', [
                self.stopped_not_by_sensor_clbk,
            ]
        )
        self.room2.gorod.lift_up()
        self.room2.gorod.lift_speed(80)
        self.room2.gorod.svet_on()

    def deactivate(self):
        self.room2.gorod.unsubscribe('solved')
        self.room2.gorod.deactivate()

    def repeat(self):
        self.deactivate()
        self.setup_logic()


class StepGorodLifted(Step):
    name = u'Город поднят'
    def __init__(self, step, room2):
        super(Step, self).__init__()
        self.step = step
        self.room2 = room2

        self.svet_animation_clbk = lambda: self.room2.gorod.svet_animation()
        self.gorod_dictor_clbk = lambda: self.PUB.sound('gorod'+get_config('lang'))

    def on_start(self):
        for puzzle in [
            self.room2.gorod
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room2.gorod.init()

    def setup_logic(self):
        self.PUB.log(u"""
Город поднялся. Сейчас должны включится механизмы Города и его подсветка.
Диктор будет рассказывать о Городе примерно 30 секунд. После этого откроется Выход из танка.
""", log_level='legend')
        self.room2.gorod.setup()
        self.gorod_solved()

    def gorod_solved(self):
        self.scheduler.schedule(self.svet_animation_clbk, 3100)
        self.room2.gorod.koleso_on()
        self.room2.gorod.kran_on()
        self.room2.gorod.svet_on()
        self.scheduler.schedule(self.gorod_dictor_clbk, 1 * 1000, once=True)
        self.scheduler.schedule(self.solved_clbk, 30000, once=True)

    def stop_svet_animation(self):
        self.scheduler.unschedule(self.svet_animation_clbk)

    def deactivate(self):
        pass

    def repeat(self):
        self.scheduler.unschedule(self.solved_clbk)
        self.deactivate()
        self.setup_logic()


class StepTankVihod(Step):
    name = u'Выход из танка'
    def __init__(self, step, room2):
        super(Step, self).__init__()
        self.step = step
        self.room2 = room2

    def on_start(self):
        for puzzle in [
            self.room2.tank
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room2.tank.init()

    def setup_logic(self):
        self.PUB.log(u"""
Выход из танка открыт.
""", log_level='legend')
        self.room2.tank.setup()
        self.room2.tank.open_vihod()
        self.PUB.controls('Управление светом в Танке', [
            {'method': 'room2.tank.open_vihod', 'descr': u'Зажечь'},
            {'method': 'room2.tank.close_vihod', 'descr': u'Погасить'},
        ])
        self.solved_clbk()

    def deactivate(self):
        pass

    def repeat(self):
        self.deactivate()
        self.setup_logic()


class StepTimeMachine(Step):
    name = u'Машина времени'
    def __init__(self, step, room4):
        super(Step, self).__init__()
        self.step = step
        self.room4 = room4

    def on_start(self):
        for puzzle in [
            self.room4.time_machine,
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room4.time_machine.init()

    def setup_logic(self):
        self.PUB.log(u"""
Загадка с Часами.
""", log_level='legend')
        self.room4.time_machine.setup()

        self.room4.time_machine.subscribe(
            'solved', [
                 lambda: self.proxy().step_solved(),
            ]
        )

        self.room4.time_machine.activate()

    def deactivate(self):
        self.room4.time_machine.deactivate()

    def repeat(self):
        self.room4.time_machine.unsubscribe('solved')
        self.deactivate()
        self.setup_logic()


class StepTimeMachineTainik(Step):
    name = u'Машина времени(тайник)'
    def __init__(self, step, room4):
        super(Step, self).__init__()
        self.step = step
        self.room4 = room4

    def on_start(self):
        for puzzle in [
            self.room4.time_machine,
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room4.time_machine.init()

    def setup_logic(self):
        self.PUB.log(u"""
Тайник в Часах открылся.
""", log_level='legend')
        self.room4.time_machine.setup()
        self.room4.time_machine.open_tainik()
        self.scheduler.schedule(self.solved_clbk, 1000, once=True)

    def deactivate(self):
        pass

    def repeat(self):
        self.scheduler.unschedule(self.solved_clbk)
        self.deactivate()
        self.setup_logic()


#class StepRemoveKluchi(Step):
    #name = u'Вынуть ключи'
    #def __init__(self, step, room2):
        #super(Step, self).__init__()
        #self.step = step
        #self.room2 = room2
        #self.self_destruct_clbk = lambda: self.proxy().self_destruct()

    #def on_start(self):
        #for puzzle in [
            #self.room2.kluch,
        #]:
            #self._set_fail_handler(puzzle)

    #def init(self):
        #self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        #self.room2.kluch.init()

    #def setup_logic(self):
        #self.PUB.log(u"""
#Чтобы перейти к следующему шагу нужно вынуть все ключи из Ключей от танка(восьмигранник под кондиционером).
#""", log_level='legend')
        #self.room2.kluch.setup()
        #self.room2.kluch.activate()

        #self.room2.kluch.subscribe(
            #'kluch_0', [
                #self.solved_clbk,
            #],
            #once=True
        #)

    #def deactivate(self):
        #self.room2.kluch.deactivate()

    #def repeat(self):
        #self.room2.kluch.unsubscribe('kluch_0')
        #self.deactivate()
        #self.setup_logic()


class StepRemoveKluchiCloseTumbi(Step):
    name = u'Закрытие тумб при вынимании ключей'
    def __init__(self, step, room2):
        super(Step, self).__init__()
        self.step = step
        self.room2 = room2
        self.self_destruct_clbk = lambda: self.proxy().self_destruct()

    def on_start(self):
        for puzzle in [
            self.room2.kluch,
            self.room2.tumba4,
            self.room2.tumba3,
            self.room2.tumba2,
            self.room2.tumba1,
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room2.kluch.init()

    def kluchi_3(self):
        #self.room2.tumba1.init()
        #self.room2.tumba1.setup()
        self.room2.tumba1.close(False)
        self.room2.tumba1.close(False)

    def kluchi_2(self):
        #self.room2.tumba1.init()
        #self.room2.tumba1.setup()
        self.room2.tumba1.close(False)
        self.room2.tumba1.close(False)
        #self.room2.tumba2.init()
        #self.room2.tumba2.setup()
        self.room2.tumba2.close(False)
        self.room2.tumba2.close(False)

    def kluchi_1(self):
        #self.room2.tumba1.init()
        #self.room2.tumba1.setup()
        self.room2.tumba1.close(False)
        self.room2.tumba1.close(False)
        #self.room2.tumba2.init()
        #self.room2.tumba2.setup()
        self.room2.tumba2.close(False)
        self.room2.tumba2.close(False)
        #self.room2.tumba3.init()
        #self.room2.tumba3.setup()
        self.room2.tumba3.close(False)
        self.room2.tumba3.close(False)

    def kluchi_0(self):
        #self.room2.tumba1.init()
        #self.room2.tumba1.setup()
        self.room2.tumba1.close(False)
        self.room2.tumba1.close(False)
        #self.room2.tumba2.init()
        #self.room2.tumba2.setup()
        self.room2.tumba2.close(False)
        self.room2.tumba2.close(False)
        #self.room2.tumba3.init()
        #self.room2.tumba3.setup()
        self.room2.tumba3.close(False)
        self.room2.tumba3.close(False)
        #self.room2.tumba4.init()
        #self.room2.tumba4.setup()
        self.room2.tumba4.close(False)
        self.room2.tumba4.close(False)

    def setup_logic(self):
        self.PUB.log(u"""
Чтобы перейти к следующему шагу нужно вынуть все ключи из Ключей от танка(восьмигранник под кондиционером).
""", log_level='legend')
        self.room2.kluch.setup()
        self.room2.kluch.activate()

        self.room2.kluch.subscribe(
            'kluch_3', [
                self.kluchi_3,
            ],
            once=True
        )
        self.room2.kluch.subscribe(
            'kluch_2', [
                self.kluchi_2,
            ],
            once=True
        )
        self.room2.kluch.subscribe(
            'kluch_1', [
                self.kluchi_1,
            ],
            once=True
        )
        self.room2.kluch.subscribe(
            'kluch_0', [
                self.kluchi_0,
                self.solved_clbk,
            ],
            once=True
        )
        self.PUB.controls('Управление Тумбой (напротив Человека)', [
            {'method': 'room2.tumba1.open', 'descr': u'Поднять'},
            {'method': 'room2.tumba1.stop_move', 'descr': u'Остановить'},
            {'method': 'room2.tumba1.close', 'descr': u'Опустить'},
        ])
        self.PUB.controls('Управление Тумбой (напротив Горгоны)', [
            {'method': 'room2.tumba2.open', 'descr': u'Поднять'},
            {'method': 'room2.tumba2.stop_move', 'descr': u'Остановить'},
            {'method': 'room2.tumba2.close', 'descr': u'Опустить'},
        ])
        self.PUB.controls('Управление Тумбой (напротив Звезд)', [
            {'method': 'room2.tumba3.open', 'descr': u'Поднять'},
            {'method': 'room2.tumba3.stop_move', 'descr': u'Остановить'},
            {'method': 'room2.tumba3.close', 'descr': u'Опустить'},
        ])
        self.PUB.controls('Управление Тумбой (напротив Путешествия)', [
            {'method': 'room2.tumba4.open', 'descr': u'Поднять'},
            {'method': 'room2.tumba4.stop_move', 'descr': u'Остановить'},
            {'method': 'room2.tumba4.close', 'descr': u'Опустить'},
        ])

    def deactivate(self):
        self.room2.kluch.deactivate()
        self.room2.tumba1.deactivate()
        self.room2.tumba2.deactivate()
        self.room2.tumba3.deactivate()
        self.room2.tumba4.deactivate()

    def repeat(self):
        self.room2.kluch.unsubscribe('kluch_0')
        self.room2.kluch.unsubscribe('kluch_1')
        self.room2.kluch.unsubscribe('kluch_2')
        self.room2.kluch.unsubscribe('kluch_3')
        self.deactivate()
        self.setup_logic()


class StepCloseTumbi(Step):
    name = u'Закрываем тумбы'
    def __init__(self, step, room2):
        super(Step, self).__init__()
        self.step = step
        self.room2 = room2
        self.self_destruct_clbk = lambda: self.proxy().self_destruct()

    def on_start(self):
        for puzzle in [
            self.room2.kluch,
            self.room2.tumba4,
            self.room2.tumba3,
            self.room2.tumba2,
            self.room2.tumba1,
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room2.kluch.init()

    def setup_logic(self):
        self.PUB.log(u"""
Чтобы перейти к следующему шагу нужно вынуть все ключи из Ключей от танка(восьмигранник под кондиционером).
""", log_level='legend')
        #self.room2.tumba1.init()
        #self.room2.tumba1.setup()
        self.room2.tumba1.close(False)
        self.room2.tumba1.close(False)
        #self.room2.tumba2.init()
        #self.room2.tumba2.setup()
        self.room2.tumba2.close(False)
        self.room2.tumba2.close(False)
        #self.room2.tumba3.init()
        #self.room2.tumba3.setup()
        self.room2.tumba3.close(False)
        self.room2.tumba3.close(False)
        #self.room2.tumba4.init()
        #self.room2.tumba4.setup()
        self.room2.tumba4.close(False)
        self.room2.tumba4.close(False)

        self.PUB.controls('Управление Тумбой (напротив Человека)', [
            {'method': 'room2.tumba1.open', 'descr': u'Поднять'},
            {'method': 'room2.tumba1.stop_move', 'descr': u'Остановить'},
            {'method': 'room2.tumba1.close', 'descr': u'Опустить'},
        ])
        self.PUB.controls('Управление Тумбой (напротив Горгоны)', [
            {'method': 'room2.tumba2.open', 'descr': u'Поднять'},
            {'method': 'room2.tumba2.stop_move', 'descr': u'Остановить'},
            {'method': 'room2.tumba2.close', 'descr': u'Опустить'},
        ])
        self.PUB.controls('Управление Тумбой (напротив Звезд)', [
            {'method': 'room2.tumba3.open', 'descr': u'Поднять'},
            {'method': 'room2.tumba3.stop_move', 'descr': u'Остановить'},
            {'method': 'room2.tumba3.close', 'descr': u'Опустить'},
        ])
        self.PUB.controls('Управление Тумбой (напротив Путешествия)', [
            {'method': 'room2.tumba4.open', 'descr': u'Поднять'},
            {'method': 'room2.tumba4.stop_move', 'descr': u'Остановить'},
            {'method': 'room2.tumba4.close', 'descr': u'Опустить'},
        ])
        self.solved_clbk()


    def deactivate(self):
        self.room2.tumba1.deactivate()
        self.room2.tumba2.deactivate()
        self.room2.tumba3.deactivate()
        self.room2.tumba4.deactivate()

    def repeat(self):
        self.deactivate()
        self.setup_logic()



#class StepSelfDestruct(Step):
    #name = u'Финальное скрытие загадок'
    #def __init__(self, step, room2):
        #super(Step, self).__init__()
        #self.step = step
        #self.room2 = room2
        #self.self_destruct_clbk = lambda: self.proxy().self_destruct()

    #def on_start(self):
        #for puzzle in [
            #self.room2.diafrag4,
            #self.room2.tumba4,
            #self.room2.diafrag3,
            #self.room2.put,
            #self.room2.tumba3,
            #self.room2.diafrag2,
            #self.room2.tumba2,
            #self.room2.diafrag1,
            #self.room2.tumba1,
            #self.room2.gorod
        #]:
            #self._set_fail_handler(puzzle)

    #def init(self):
        #self.PUB.step_event({'id': self.step, 'status': 'inprogress'})

    #def setup_logic(self):
        #self.PUB.log(u"""
#Комната сворачивается.
#""", log_level='legend')
        #self.room2.put.init()
        #self.room2.put.setup()

        ##self.room2.diafr_svet.gorgona(False)
        ##self.room2.diafr_svet.chel(False)
        ##self.room2.diafr_svet.zvezdi(False)
        ##self.room2.diafr_svet.put(False)

        ##self.room2.tumba1.init()
        ##self.room2.tumba1.setup()
        #self.room2.tumba1.close(False)
        #self.room2.tumba1.close(False)

        ##self.room2.tumba2.init()
        ##self.room2.tumba2.setup()
        #self.room2.tumba2.close(False)
        #self.room2.tumba2.close(False)

        ##self.room2.tumba3.init()
        ##self.room2.tumba3.setup()
        #self.room2.tumba3.close(False)
        #self.room2.tumba3.close(False)

        ##self.room2.tumba4.init()
        ##self.room2.tumba4.setup()
        #self.room2.tumba4.close(False)
        #self.room2.tumba4.close(False)

        #self.room2.tumba1.deactivate()
        #self.room2.tumba2.deactivate()
        #self.room2.tumba3.deactivate()
        #self.room2.tumba4.deactivate()

        #self.diafrag1_close_clbk = lambda: self.room2.diafrag1.close()
        #self.diafrag2_close_clbk = lambda: self.room2.diafrag2.close()
        #self.diafrag3_close_clbk = lambda: self.room2.diafrag3.close()
        #self.diafrag4_close_clbk = lambda: self.room2.diafrag4.close()
        #self.scheduler.schedule(self.diafrag1_close_clbk, 5000, once=True)
        #self.scheduler.schedule(self.diafrag2_close_clbk, 5000, once=True)
        #self.scheduler.schedule(self.diafrag3_close_clbk, 5000, once=True)
        #self.scheduler.schedule(self.diafrag4_close_clbk, 5000, once=True)


        ##self.room2.kluch.init()
        ##self.room2.kluch.setup()
        #self.room2.kluch.svet(False)

        ##self.room2.gorod.init()
        ##self.room2.gorod.setup()
        ##self.room2.gorod.lift_down()

        #def svet_off():
            #self.room2.gorod.svet_off()
            #self.room2.gorod.kran_off()
            #self.room2.gorod.koleso_off()
            #self.room2.tumba1.svet(False)
            #self.room2.tumba2.svet(False)
            #self.room2.tumba3.svet(False)
            #self.room2.tumba4.svet(False)
        #self.scheduler.schedule(svet_off, 15000, once=True)

        #self.solved_clbk()

    #def deactivate(self):
        #self.room2.tumba1.deactivate()
        #self.room2.tumba2.deactivate()
        #self.room2.tumba3.deactivate()
        #self.room2.tumba4.deactivate()

    #def repeat(self):
        #self.scheduler.unschedule(getattr(self, 'diafrag1_close_clbk', lambda: None))
        #self.scheduler.unschedule(getattr(self, 'diafrag2_close_clbk', lambda: None))
        #self.scheduler.unschedule(getattr(self, 'diafrag3_close_clbk', lambda: None))
        #self.scheduler.unschedule(getattr(self, 'diafrag4_close_clbk', lambda: None))
        #self.deactivate()
        #self.setup_logic()


class StepVihod(Step):
    name = u'Выход'
    def __init__(self, step, room1):
        super(Step, self).__init__()
        self.step = step
        self.room1 = room1

    def on_start(self):
        for puzzle in [
            self.room1.dveri
        ]:
            self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.room1.dveri.init()

    def setup_logic(self):
        self.PUB.log(u"""
Выход и вход открыты.
""", log_level='legend')
        self.room1.dveri.setup()
        self.room1.dveri.open_vihod()
        self.room1.dveri.open_vhod()
        self.solved_clbk()

    def deactivate(self):
        pass

    def repeat(self):
        self.deactivate()
        self.setup_logic()

class StepWin(Step):
    name = u'Успех!'
    def __init__(self, step):
        super(Step, self).__init__()
        self.step = step

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})

    def setup_logic(self):
        self.PUB.log(u"""
Игра окончена.
""", log_level='legend')
        GAME = Resources().require('game')
        GAME.game_win()
        self.solved_clbk()

    def deactivate(self):
        pass

    def repeat(self):
        pass




class StepStopHuman(Step):
    name = u'Готовим комнату к новой игре(ручной этап)'
    def __init__(self, step, room1, room2, room3, room4):
        super(Step, self).__init__()
        self.step = step
        self.room1 = room1
        self.room2 = room2
        self.room3 = room3
        self.room4 = room4

    def on_start(self):
        pass
        #FIXME: подписывать только на нужные загадки
        #for puzzle in self.room3.puzzles:
            #self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.PUB.log(u"""
Что нужно сейчас сделать:
     - закрыть картину 'Дама с горностаем'
     - прокрутить диски Автопортрета
     - вынуть ключи(деревянные цилиндрики) из 'Ключи от танка' и вставить их в тумбы
     - пощелкать змейками 'Горгоны'
     - перевести в верхнее крайнее положение ползунки 'Человека'
     - поставить в центр фишку 'Путешествия'
     - прокрутить диски 'Звезд'
     - положить педаль от Велосипеда в тайник Окошка(который в комнате с рыцарем)
     - закрыть этот тайник в окошке
     - положить шестерни рыцаря в нижний ящик
     - провернуть щиты над нижним ящиком
     - закрыть ящик(если ящик закрыт, сыграть с рыцарем(выставить щиты правильно))
     - с помощью Факелов повернуть зеркало на Люстре к выходу, чтобы его не было видно через решетку из команты с Рыцарем
     - провернуть кольца у часов(которые в последней команте)
     - закрыть ящик у часов(которые в последней команте)
     - закрыть вход танка(который ведет в главную комнату со столом)
     - закрыть выход танка(который ведет в последнюю комнату)

Если все выше перечисленное выполнено, можно переходить с следующему шагу(кнопка 'След. шаг')
""", log_level='tip')

    def setup_logic(self):

        self.room1.dveri.init()
        self.room1.dveri.setup()
        self.room1.dveri.open_vhod()
        self.room1.dveri.open_vihod()

        self.room1.dama.init()
        self.room1.dama.setup()
        self.room1.dama.close_lock()

        self.room2.tank.init()
        self.room2.tank.setup()
        self.room2.tank.close_vhod()
        self.room2.tank.close_vihod()

        self.room3.okoshko.init()
        self.room3.okoshko.setup()
        self.room3.okoshko.close_tainik()
        self.room3.okoshko.svet_on()

        self.room3.fakels.init()
        self.room4.lustra.init()
        self.room3.fakels.setup()
        self.room4.lustra.setup()
        self.room3.fakels.subscribe(
            'left', [
                lambda: self.room4.lustra.left()
            ]
        )
        self.room3.fakels.subscribe(
            'rigth', [
                lambda: self.room4.lustra.rigth()
            ]
        )
        self.room3.fakels.subscribe(
            'stop', [
                lambda: self.room4.lustra.move_stop()
            ]
        )
        self.room3.fakels.activate()

        self.room4.time_machine.init()
        self.room4.time_machine.setup()
        self.room4.time_machine.close_tainik()

        self.room2.diafrag1.open()
        self.room2.diafrag2.open()
        self.room2.diafrag3.open()
        self.room2.diafrag4.open()

        self.room2.tumba1.init()
        self.room2.tumba1.setup()
        self.room2.tumba1.open()

        self.room2.tumba2.init()
        self.room2.tumba2.setup()
        self.room2.tumba2.open()

        self.room2.tumba3.init()
        self.room2.tumba3.setup()
        self.room2.tumba3.open()

        self.room2.tumba4.init()
        self.room2.tumba4.setup()
        self.room2.tumba4.open()


        #self.PUB.event({'enable': 'start_game'})

    def deactivate(self):
        self.room3.fakels.deactivate()
        self.room3.fakels.unsubscribe('left')
        self.room3.fakels.unsubscribe('right')
        self.room3.fakels.unsubscribe('stop')
        self.room2.tumba1.deactivate()
        self.room2.tumba2.deactivate()
        self.room2.tumba3.deactivate()
        self.room2.tumba4.deactivate()

    def repeat(self):
        self.deactivate()
        self.setup_logic()


class StepStopAuto(Step):
    name = u'Готовим комнату к новой игре(автоматический этап)'
    def __init__(self, step, room1, room2, room3, room4):
        super(Step, self).__init__()
        self.step = step
        self.room1 = room1
        self.room2 = room2
        self.room3 = room3
        self.room4 = room4

    def on_start(self):
        pass
        #FIXME: подписывать только на нужные загадки
        #for puzzle in self.room3.puzzles:
            #self._set_fail_handler(puzzle)

    def init(self):
        self.PUB.step_event({'id': self.step, 'status': 'inprogress'})
        self.PUB.log(u"""
На этом шаге в исходное состояние переходит все, что может сделать это само(в автоматическом режиме)

Что нужно сейчас сделать(используя систему видеонаблюдения):
     - удостоверится, что Проем закрылся;
     - удостоверится, что все Диафрагмы загадок закрылись;
     - удостоверится, что все Тумбы опустились;
     - удостоверится, что все Витражи погасли;
     - удостоверится, что все Город опустился.

Если все выше перечисленное выполнено, можно переходить с следующему этапу (кнопка 'Страт игры', она станет доступна примерно через 10 секунд после начала этого шага)

Если что-то застряло или не закрылось до конца - нажмите 'Повторить шаг'
""", log_level='tip')

    def setup_logic(self):
        #pass

        self.room1.dveri.init()
        self.room1.dveri.setup()
        self.room1.dveri.open_vhod()
        self.room1.dveri.close_vihod()

        self.room1.dama.init()
        self.room1.dama.setup()
        self.room1.dama.close_lock()

        self.room1.proem.init()
        self.room1.proem.setup()
        self.room1.proem.close_proem()

        self.room2.put.init()
        self.room2.put.setup()

        self.room2.zvezdi.init()
        self.room2.zvezdi.setup()
        self.room2.zvezdi.reset()

        self.room2.diafrag1.close()
        self.room2.diafrag2.close()
         #закрываем диафрагму с задержкой, чтобы путешествие успело повернутся
         #в исходное положение
        self.diafrag3_close_clbk = lambda: self.room2.diafrag3.close()
        self.scheduler.schedule(self.diafrag3_close_clbk, 5000, once=True)
        self.room2.diafrag4.close()

        self.room2.diafr_svet.setup()
        self.room2.diafr_svet.gorgona(False)
        self.room2.diafr_svet.chel(False)
        self.room2.diafr_svet.zvezdi(False)
        self.room2.diafr_svet.put(False)

        self.room2.tumba1.init()
        self.room2.tumba1.setup()
        self.room2.tumba1.close()
        self.room2.tumba1.close()

        self.room2.tumba2.init()
        self.room2.tumba2.setup()
        self.room2.tumba2.close()
        self.room2.tumba2.close()

        self.room2.tumba3.init()
        self.room2.tumba3.setup()
        self.room2.tumba3.close()
        self.room2.tumba3.close()

        self.room2.tumba4.init()
        self.room2.tumba4.setup()
        self.room2.tumba4.close()
        self.room2.tumba4.close()

        self.room2.tumba1.deactivate()
        self.room2.tumba2.deactivate()
        self.room2.tumba3.deactivate()
        self.room2.tumba4.deactivate()

        self.room2.vitrag1.init()
        self.room2.vitrag1.setup()
        self.room2.vitrag1.main(0, 0)

        self.room2.vitrag2.init()
        self.room2.vitrag2.setup()
        self.room2.vitrag2.main(0, 0)

        self.room2.vitrag3.init()
        self.room2.vitrag3.setup()
        self.room2.vitrag3.main(0, 0)

        self.room2.vitrag4.init()
        self.room2.vitrag4.setup()
        self.room2.vitrag4.main(0, 0)

        self.room2.kluch.init()
        self.room2.kluch.setup()
        self.room2.kluch.svet(False)

        self.room2.tank.init()
        self.room2.tank.setup()
        self.room2.tank.close_vhod()
        self.room2.tank.close_vihod()

        self.room3.okoshko.init()
        self.room3.okoshko.setup()
        self.room3.okoshko.close_tainik()
        self.room3.okoshko.svet_on()

        self.room4.time_machine.init()
        self.room4.time_machine.setup()
        self.room4.time_machine.close_tainik()

        self.room2.gorod.init()
        self.room2.gorod.setup()
        self.room2.gorod.lift_down()
        self.room2.gorod.svet_off()
        self.room2.gorod.kran_off()
        self.room2.gorod.koleso_off()
        self.scheduler.schedule(lambda: self.PUB.event({'enable': 'start_game'}), 10000, once=True)

    def deactivate(self):
        self.room2.tumba1.deactivate()
        self.room2.tumba2.deactivate()
        self.room2.tumba3.deactivate()
        self.room2.tumba4.deactivate()

    def repeat(self):
        self.scheduler.unschedule(self.diafrag3_close_clbk)
        self.setup_logic()


def test_step():
    room = TestRoom_1()
    step = Step_test.start(room).proxy()
    step.init().get()
    step.start_step().get()


# игра - содержить в себе всю логику
# инициализирует все загадки и определяет
# взаимоотношения между ними
class Game(Actor):

    def __init__(self):
        super(Game, self).__init__()
        self.steps = None
        self.active_step = None
        self.solved_steps = []
        self.started = None
        self.PUB = Resources().require('PUB')
        self.vhod_opened = None
        self.start_time = None
        self.last_next_step = None

        self.game_config = {
            'lang': 'rus',
        }

    def proxy(self):
        return self.actor_ref.proxy()

    def init(self):
        self.started = True
        self.room1 = Room1()
        self.room2 = Room2()
        self.room3 = Room3()
        self.room4 = Room4()
        self.steps = []
        self.init_steps = [
            StepInit.start(0, self.room1, self.room2,
                                self.room3, self.room4).proxy(),
            #StepTest.start(0, self.room1, self.room2,
                                #self.room3, self.room4).proxy(),
        ]

        self.prepare_steps = [
            StepStopHuman.start(50, self.room1, self.room2,
                           self.room3, self.room4).proxy(),
            StepStopAuto.start(51, self.room1, self.room2,
                           self.room3, self.room4).proxy(),
        ]

        self.game_steps = [
            StepIdle.start(1, self.room1).proxy(),
            StepVhod.start(2, self.room1).proxy(),
            StepIdle.start(3, self.room1).proxy(),
            StepStartTimer.start(4).proxy(),
            StepAvtoportret.start(5, self.room1).proxy(),
            StepProem.start(6, self.room1).proxy(),
            StepKnopka.start(7, self.room2).proxy(),
            StepDama.start(8, self.room1).proxy(),
            StepChelovek.start(9, self.room1, self.room2).proxy(),
            StepTumba4.start(10, self.room2).proxy(),
            StepKluch.start(11, self.room2, number=1).proxy(),
            StepPut.start(12, self.room2).proxy(),
            StepTumba3.start(13, self.room2).proxy(),
            StepKluch.start(14, self.room2, number=2).proxy(),
            StepZvezdi.start(15, self.room2).proxy(),
            StepTumba2.start(16, self.room2).proxy(),
            StepKluch.start(17, self.room2, number=3).proxy(),
            StepGorgona.start(18, self.room2).proxy(),
            StepTumba1.start(19, self.room2).proxy(),
            StepKluch.start(20, self.room2, number=4).proxy(),
            StepRicarDiktor.start(21).proxy(),
            StepTankVhod.start(22, self.room2).proxy(),
            StepRicar.start(23, self.room3, self.room4).proxy(),
            StepOkoshko.start(24, self.room3).proxy(),
            StepVelo.start(25, self.room1, self.room2).proxy(),
            StepGorodLift.start(26, self.room2).proxy(),
            StepGorodLifted.start(27, self.room2).proxy(),
            StepTankVihod.start(28, self.room2).proxy(),
            StepTimeMachine.start(29, self.room4).proxy(),
            StepTimeMachineTainik.start(30, self.room4).proxy(),
            #StepRemoveKluchi.start(31, self.room2).proxy(),
            StepRemoveKluchiCloseTumbi.start(31, self.room2).proxy(),
            #StepSelfDestruct.start(32, self.room2).proxy(),
            StepCloseTumbi.start(32, self.room2).proxy(),
            StepVihod.start(33, self.room1).proxy(),
            StepWin.start(34).proxy(),
        ]

        self.simple_game_steps = [
            StepIdle.start(1, self.room1).proxy(),
            StepVhod.start(2, self.room1).proxy(),
            StepIdle.start(3, self.room1).proxy(),
            StepStartTimer.start(4).proxy(),
            StepAvtoportret.start(5, self.room1).proxy(),
            StepProem.start(6, self.room1).proxy(),
            StepKnopka.start(7, self.room2).proxy(),
            StepDama.start(8, self.room1).proxy(),
            StepTablePuzzles.start(9,
                StepChelovek.start(901, self.room1, self.room2, mute_dictor=True).proxy(),
                StepTumba4.start(902, self.room2).proxy(),
                StepPut.start(903, self.room2, mute_dictor=True).proxy(),
                StepTumba3.start(904, self.room2).proxy(),
                StepZvezdi.start(905, self.room2, mute_dictor=True).proxy(),
                StepTumba2.start(906, self.room2).proxy(),
                StepGorgona.start(907, self.room2, mute_dictor=True).proxy(),
                StepTumba1.start(908, self.room2).proxy(),
            ).proxy(),
            StepKluch.start(20, self.room2, number=4).proxy(),
            StepRicarDiktor.start(21).proxy(),
            StepTankVhod.start(22, self.room2).proxy(),
            StepRicar.start(23, self.room3, self.room4).proxy(),
            StepOkoshko.start(24, self.room3).proxy(),
            StepVelo.start(25, self.room1, self.room2).proxy(),
            StepGorodLift.start(26, self.room2).proxy(),
            StepGorodLifted.start(27, self.room2).proxy(),
            StepTankVihod.start(28, self.room2).proxy(),
            StepTimeMachine.start(29, self.room4).proxy(),
            StepTimeMachineTainik.start(30, self.room4).proxy(),
                    #StepRemoveKluchi.start(31, self.room2).proxy(),
            StepRemoveKluchiCloseTumbi.start(31, self.room2).proxy(),
                    #StepSelfDestruct.start(32, self.room2).proxy(),
            StepCloseTumbi.start(32, self.room2).proxy(),
            StepVihod.start(33, self.room1).proxy(),
            StepWin.start(34).proxy(),
        ]

    def game_init(self):
        self.init()
        self.steps = self.init_steps
        self.proxy().next_step()
        return self.steps[:]

    def prepare_new_game(self):
        if self.active_step:
            self.active_step.unsubscribe('fail')
            self.active_step.unsubscribe('solved')
            if not self.active_step.failed.get():
                self.active_step.step_solved()
            self.solved_steps.append(self.active_step)

        self.steps = self.prepare_steps
        self.proxy().next_step()
        return self.steps[:]

    def game_start(self):
        self.steps = self.game_steps
        self.proxy().next_step()
        return self.steps[:]

    def game_start_simple(self):
        #import pudb; pu.db
        self.steps = self.simple_game_steps
        #import pudb; pu.db
        self.proxy().next_step()
        #import pudb; pu.db
        return self.steps[:]

    def game_over(self):
        #автоматически не прекращать игру!
        #self.steps = []
        #self.active_step.deactivate()
        self.PUB.log(u"""
Прошел час игры. Игроки не успели выбраться за отведенное время.
Но если им осталось совсем чуть-чуть до выхода, можно не завершать игру и немного подождать.
Чтобы не портить людям удовольствие.
""", log_level='legend')

    def game_win(self):
        self.end_time = datetime.now()
        self.PUB.log(u"""
Успех. Игроки успели выбраться из команты.
""", log_level='legend')
        if self.start_time:
            self.PUB.log(u"""
Игра длилась: {0}.
""".format(strfdelta(self.end_time - self.start_time)), log_level='legend')


    def next_step(self, debounce=False):
        if debounce:
            if self.last_next_step:
                if ( (datetime.now() - self.last_next_step) < timedelta(seconds=1) ):
                    return

        self.last_next_step = datetime.now()

        self.PUB.event({'temp_disable': 'next_step'})
        if self.active_step:
            #self.active_step.unsubscribe('fail')
            self.active_step.unsubscribe('solved')
            if not self.active_step.failed.get():
                self.active_step.step_solved()
            self.active_step.finishing()
            self.solved_steps.append(self.active_step)

        if not len(self.steps):
            return

        self.active_step = self.steps.pop(0)
        #self.PUB.log(u'Переход к следующем шагу: {0}'.format(self.active_step.name.get()))
        if self.active_step.failed.get():
            self.active_step.log_that_failed()

        #self.active_step.subscribe('fail', lambda: self.proxy().next_step())
        self.active_step.subscribe('solved', lambda: self.proxy().next_step())
        self.active_step.init().get()
        self.active_step.start_step().get()

    def repeat_step(self):
        self.active_step.repeat()

    def open_close_vhod(self):
        if not self.vhod_opened:
            self.room1.dveri.open_vhod()
            self.vhod_opened = True
            self.PUB.log(u"""
Входная дверь открыта.
""", log_level='legend')
        else:
            self.room1.dveri.close_vhod()
            self.vhod_opened = False
            self.PUB.log(u"""
Входная дверь закрыта.
""", log_level='legend')

    def control(self, data):
        control = data['control']
        splited = control.split('.')
        if len(splited) == 3:
            room, puzzle, method = splited
            puzzle = getattr(getattr(self, room), puzzle)
            getattr(puzzle, method)()
        elif len(splited) == 1:
            method = splited[0]
            getattr(self, method)()

    def set_game_config_lang_rus(self):
        self.game_config['lang'] = 'rus'
        self.PUB.log(u"Настройка изменена. Язык: Русский")

    def set_game_config_lang_eng(self):
        self.game_config['lang'] = 'eng'
        self.PUB.log(u"Настройка изменена. Язык: Английский")

    def get_game_config(self):
        return getattr(self, 'game_config', {})

    def play_chel(self):
        self.PUB.sound('chel'+get_config('lang'))

    def play_gorgona(self):
        self.PUB.sound('gorgona'+get_config('lang'))

    def play_gorod(self):
        self.PUB.sound('gorod'+get_config('lang'))

    def play_hello(self):
        self.PUB.sound('hello'+get_config('lang'))

    def play_lira(self):
        self.PUB.sound('lira'+get_config('lang'))

    def play_put(self):
        self.PUB.sound('put'+get_config('lang'))

    def play_ricar(self):
        self.PUB.sound('ricar'+get_config('lang'))

    def play_zvezdi(self):
        self.PUB.sound('zvezdi'+get_config('lang'))

    def open_all(self):
        self.room1.dveri.open_vhod()
        self.room1.dveri.close_vhod()
        self.room1.proem.open_proem()
        self.room2.tumba1.open()
        self.room2.tumba2.open()
        self.room2.tumba3.open()
        self.room2.tumba4.open()
        self.room2.tumba1.deactivate()
        self.room2.tumba2.deactivate()
        self.room2.tumba3.deactivate()
        self.room2.tumba4.deactivate()
        self.room2.diafrag1.open()
        self.room2.diafrag2.open()
        self.room2.diafrag3.open()
        self.room2.diafrag4.open()
        self.room2.gorod.lift_up()
        self.room2.tank.open_vhod()
        self.room2.tank.open_vihod()
        self.room3.okoshko.open_tainik()
        self.room4.time_machine.open_tainik()
        self.PUB.log(u"""
Все что можно было теперь открыто. Чтобы закрыть все, начните все заново. На этапе 'Подготовка игры', все закроется.
""", log_level='legend')

    def emerge_stop(self):
        #self.room1.dveri.open_vhod()
        #self.room1.dveri.close_vhod()
        self.room1.proem.move_stop()
        self.room2.tumba1.stop_move()
        self.room2.tumba2.stop_move()
        self.room2.tumba3.stop_move()
        self.room2.tumba4.stop_move()
        self.room2.diafrag1.stop_move()
        self.room2.diafrag2.stop_move()
        self.room2.diafrag3.stop_move()
        self.room2.diafrag4.stop_move()
        self.room2.gorod.lift_stop()
        #self.room2.tank.open_vhod()
        #self.room2.tank.open_vihod()
        #self.room3.okoshko.open_tainik()
        #self.room4.time_machine.open_tainik()
        self.PUB.log(u"""
Все движение остановлено. Чтобы продолжить игру нажмите кнопку 'Повторить шаг'.
""", log_level='legend')

    def stop_game(self):
        pass
        #TODO: сделать остановку Steps
        #if self.started:
            #self.room1.stop()
            #self.room2.stop()
            #self.room3.stop()
            #self.room4.stop()

    def set_start_time(self, start_time):
        self.start_time = start_time


def get_config(name):
    GAME = Resources().require('game')
    config = GAME._actor.get_game_config()
    lang = '' if config['lang'] == 'rus' else '_eng'

    config = {
        'lang': lang,
    }
    return config.get(name)


if __name__ == '__main__':
    run_game(Game)
