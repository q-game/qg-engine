#!/bin/bash

# Разворачиваетнеобходимую файловую структуру
# Исходные данные: каталог source c кодом из репозитория
# Выходные данные: каталоги data, cache и locals со всем необходимым содержимым
# Дальнейшие действия: настроить local_settings.py, синхронизировать СУБД и радоваться жизни

cd `dirname "$0"`;
BRANCH_ROOT="../../";
USER_NAME=$USER;

make_dirs(){
    (
        cd $BRANCH_ROOT;
        #echo -n "Creating folders...";
        ## внутренние НЕ создадутся автоматически из-за fileupload приложения
        #mkdir media \
            #&& echo "  ok" \
            #|| echo "  ERROR!" 1>&2;
    )
}

local_settings(){
    touch $BRANCH_ROOT/local_settings.py \
        && echo "  ok" \
        || echo "  ERROR!" 1>&2;
}

if [ "$1" ]; then
    make_dirs;
    local_settings;
    echo "Creating virtual python environment...";
    ./env_update.sh "$1" \
        && echo "  ok" \
        || echo "  ERROR!" 1>&2;
else
   echo "Error: python version requires as first argument" 1>&2;
fi
