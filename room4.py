# encoding: utf-8
from functools import partial

import gevent

from engine import Resources, shex
from engine import Puzzle
from engine import DummyDevice

class Lustra(Puzzle):
    # 42
    glossary_name = u"Люстра"
    glossary_code = u""

    def __init__(self, device_addr, *args, **kwargs):
        super(Lustra, self).__init__(*args, **kwargs)
        self.device_addr = device_addr

        self.port_start = 7
        self.port_dir = 6

    def on_start(self):
        self.device = DummyDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])


    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.as_output().get()

    def move(self, move=True):
        if move:
            self.set_one(self.port_start)
        else:
            self.set_zero(self.port_start)

    def move_stop(self):
        self.move(False)

    def left(self):
        self.set_one(self.port_dir)
        self.move()

    def rigth(self):
        self.set_zero(self.port_dir)
        self.move()

    def set_zero(self, number):
        data = [
            shex(0),
            shex(0),
        ]
        self.device.set_port(number, data).get()

    def set_one(self, number):
        data = [
            shex(0),
            shex(1),
        ]
        self.device.set_port(number, data).get()

    def stopall(self):
        self.device.stop()


class TimeMachine(Puzzle):
    ##18
    ##port4 0 - solved
    ##port5 0 - solved 
    ##port6 out 1
    ##port7 out 1
    glossary_name = u"Часы"
    glossary_code = u""

    def __init__(self, device_addr, *args, **kwargs):
        super(TimeMachine, self).__init__(*args, **kwargs)
        self.device_addr = device_addr

    def on_start(self):
        self.device = DummyDevice.start(self.device_addr).proxy()
        self.device.subscribe('fail', [
            lambda data: self.PUB.log(u"Девайс c адресом {0} ({1}) провалился.".format(
                self.device_addr,
                unicode(self)
            )),
            lambda data: self.proxy().trigger('fail', data)
        ])


    def init(self):
        self.device.init().get()

    def setup(self):
        self.device.as_input().get()

    def activate(self):
        self.device.subscribe(
            'stat.inputs',
            lambda data: self.proxy().inputs(data[0]),
            200
        ).get()

    def inputs(self, state):
        print bin(state)
        if state == 0:
            self.open_tainik()
            self.trigger('solved')

    def close_lock(self, number):
        data = [
            shex(0),
            shex(0),
        ]
        self.device.set_port(number, data).get()

    def open_lock(self, number):
        data = [
            shex(0),
            shex(1),
        ]
        self.device.set_port(number, data).get()

    def open_tainik(self):
        self.device.unsubscribe('stat.inputs')
        self.device.as_output().get()
        self.open_lock(6)
        self.open_lock(7)

    def close_tainik(self):
        self.device.unsubscribe('stat.inputs')
        self.device.as_output().get()
        self.close_lock(6)
        self.close_lock(7)

    def deactivate(self):
        self.device.unsubscribe('stat.inputs')

    def stopall(self):
        self.device.stop()
