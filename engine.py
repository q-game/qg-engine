# encoding: utf-8
from pprint import pprint
import json
import signal
import sys
from datetime import datetime
import copy
import types

import pykka
import pykka.debug
import gevent
import zmq.green as zmq
from pykka.gevent import GeventActor as Actor
from pykka.gevent import GeventFuture as Future

from net import Requester, Responder, Publisher
from net import zmq_ctx

from eng_settings import ADRS

from utils import Resources, Scheduler, shex

import pudb
pudb.DEFAULT_SIGNAL = None

from utils import ModifyFilter, PUBHandler, log_formatter
import logging
log = logging.getLogger('ENG')
log.setLevel(logging.DEBUG)
log.addFilter(ModifyFilter())

pykka_log = logging.getLogger('pykka')
pykka_log.setLevel(logging.DEBUG)
pykka_log.addFilter(ModifyFilter())

rm1_log = logging.getLogger('[rm1]')
rm1_log.setLevel(logging.DEBUG)
rm1_log.addFilter(ModifyFilter(prefix='[rm1]', name='ENG'))

rm2_log = logging.getLogger('[rm2]')
rm2_log.setLevel(logging.DEBUG)
rm2_log.addFilter(ModifyFilter(prefix='[rm2]', name='ENG'))

#logger_sock = zmq_ctx.socket(zmq.PUB)
#logger_sock.connect('tcp://{ip}:{port}'.format(**ADRS['LOGS']))
#handler = PUBHandler(logger_sock)
#handler.setFormatter(log_formatter)

#log.addHandler(handler)
#rm1_log.addHandler(handler)
#pykka_log.addHandler(handler)

console_handler = logging.StreamHandler()
console_handler.setFormatter(log_formatter)
#log.addHandler(console_handler)
#rm1_log.addHandler(console_handler)
pykka_log.addHandler(console_handler)

log.info(u'PING!')


class Events(object):
    def __init__(self):
        self.subs = {}
        self.once = []

    def subscribe(self, event, handlers, once=False):
        self.subs.setdefault(event, [])
        if type(handlers) is list:
            self.subs[event] += handlers
        else:
            self.subs[event].append(handlers)

        if once:
            self.once.append(event)

        return handlers # чтобы можно было удалить конкретный handler события

    def unsubscribe(self, event=None, handlers=None):

        def unsub(handlers_list, handler):
            try:
                handlers_list.remove(handler)
            except ValueError:
                pass

        if event is None:
            self.subs = {}

        if handlers:
            if isinstance(handlers, types.FunctionType):
                callback = handlers
                handlers = []
                handlers.append(callback)
            for handler in list(handlers):
                unsub(self.subs[event], handler)
        else:
            self.subs[event] = []

    def trigger(self, event, data=None):
        #import pudb; pu.db
        #print unicode(self)
        handlers = self.subs.get(event, None)
        if not handlers:
            log.warn('No handlers for such event: %s' % event)
            return

        if event in self.once:
            self.unsubscribe(event)

        for handle in handlers:
            if data:
                handle(data)
            else:
                handle()


# представление девайса в движке
# определяет поведение девайса
# берет на себя всю ответственность за связь с ХАЛ,
# и своим представление в ХАЛ
class ModbusDevice(Actor, Events):

    _REG = {
        'stat': {
            '0x0000': 'device_type',
            '0x0001': 'serial_number',
            '0x0002': 'hardware_revision',
            '0x0003': 'software_version',
            '0x0004': 'global_status',
            '0x0005': 'power_voltage',
        },
        'conf': {
        }
    }
    _CMD = {
        '0x0000': 'reset',
        '0x0001': 'emergency_stop',
        '0x0002': 'beep',
    }

    device_type = None

    def __init__(self, device_addr, hal=None):
        super(ModbusDevice, self).__init__()
        Events.__init__(self)

        if hal is None:
            hal = Resources().require('HAL.default_modbus')
        self.hal =  hal
        self.device_addr = device_addr
        self.inited = None

    def proxy(self):
        return self.actor_ref.proxy()

    def init(self):
        #if self.device_type == "0x534C":
            #import pudb; pu.db
        if self.inited:
            return True

        resp = self.hal.request({
            'type': 'system',
            'cmd': 'init',
            'kwargs': {
                'device_addr': self.device_addr,
                'device_type': self.device_type
            }
        }).get()
        if resp.get('status') == 'success':
            self.my_address = "tcp://{0}:{1}".format(ADRS['ENG']['ip'], resp.get('eng_device_port'))
            self.my_responder = Responder.start(self.my_address, self.actor_ref).proxy()

            self.hal_device_address = "tcp://{0}:{1}".format(ADRS['VHAL']['ip'], resp.get('hal_device_port'))
            self.hal_device = Requester.start(self.hal_device_address).proxy()
            print 'Device inited. Dv: {0} {1}'.format(self.device_type, self.device_addr)
            self.preconfig()
            self.inited = True
            return True
        elif resp.get('status') == 'fail':
            self.trigger('fail', [self, None])
            print 'Device failed! Dv: {0} {1}'.format(self.device_type, self.device_addr)
            print resp
            self.inited = False
            return False
        else:
            self.inited = False
            print 'DEVICE TOTALY FAILED!!! Dv: {0} {1}'.format(self.device_type, self.device_addr)

    def preconfig(self):
        pass


    def reg(self, register_name):
        group, register = register_name.split('.')
        return self.REG[group][register]

    def cmd(self, cmd_name):
        return self.CMD[cmd_name]

    def read_regs(self, register_name, registers_number=1, **kwargs):
        return self.mb_read(
            self.reg(register_name),
            registers_number=registers_number,
            **kwargs
        )

    def write_regs(self, register_name, data=None, **kwargs):
        return self.mb_write(
            self.reg(register_name),
            data=data,
            **kwargs
        )

    def send_cmd(self, cmd_name):
        return self.mb_command(self.cmd(cmd_name))

    #FIXME: зачем здесь этот метод?
    def configure(self, register_name, data):
        return self.write_regs(register_name, data)

    def send2hal(self, msg):
        if not hasattr(self, 'hal_device'):
            return {'status': 'fail', 'info': 'Mock response because device init failed.'}
        resp = self.hal_device.request(msg).get()
        if resp.get('status', None) == u'fail':
            self.trigger('fail', [self, resp])
        return resp


    def mb_read(self, register_addr=None, registers_number=1, **kwargs):
        if register_addr is None:
            raise Exception("'register_addr' must not be None")

        resp = self.send2hal({
            'type': 'cmd',
            'cmd': 'read',
            'device_addr': self.device_addr,
            'register_addr': register_addr,
            'registers_number': registers_number
        })
        if resp.get('status', None) == u'success':
            return resp.get('data', None)
        else:
            raise Exception('READ operation failed.')

    def mb_write(self, register_addr=None, data=None, **kwargs):
        if register_addr is None:
            raise Exception("'register_addr' must not be None")
        if data is None:
            raise Exception("'data' must not be None")

        resp = self.send2hal({
            'type': 'cmd',
            'cmd': 'write',
            'device_addr': self.device_addr,
            'register_addr': register_addr,
            'registers_number': len(data),
            'data': data
        })

        #import pudb; pu.db
        if resp.get('status', None) == u'success':
            return True
        else:
            raise Exception('WRITE operation failed.')

    def mb_command(self, command_code=None, **kwargs):
        if command_code is None:
            raise Exception("'command_code' must not be None")

        resp =  self.send2hal({
            'type': 'cmd',
            'cmd': 'command',
            'device_addr': self.device_addr,
            'command_code': command_code,
            'device_type': self.device_type
        })

        if resp.get('status', None) == u'success':
            return True
        else:
            raise Exception('COMMAND operation failed.')

    #def mb_change_addr(self, new_addr=None, **kwargs):
        #if new_addr is None:
            #raise Exception("'new_addr' must not be None")

        #return self.send2hal({
            #'type': 'cmd',
            #'cmd': 'change_addr',
            #'device_addr': self.device_addr,
            #'new_addr': new_addr
        #})


    def subscribe(self, register_addr, handlers, time_interval=1000, **kwargs):
        #import pudb; pu.db
        try:
            int(register_addr, 16)
        except ValueError:
            try:
                register_addr = self.reg(register_addr)
            except ValueError:
                return super(ModbusDevice, self).subscribe(register_addr, handlers)

        self.send2hal({
            'type': 'sub',
            'device_addr': self.device_addr,
            'register_addr': register_addr,
            'time_interval': time_interval
        })
        return super(ModbusDevice, self).subscribe(register_addr, handlers)


    def unsubscribe(self, register_addr=None, handlers=None, **kwargs):
        if not (register_addr is None):
            try:
                int(register_addr, 16)
            except ValueError:
                try:
                    register_addr = self.reg(register_addr)
                except ValueError:
                    return super(ModbusDevice, self).unsubscribe(register_addr, handlers)

        super(ModbusDevice, self).unsubscribe(register_addr, handlers)
        if not self.subs[register_addr]:
            self.send2hal({
                'type': 'unsub',
                'device_addr': self.device_addr,
                'register_addr': register_addr,
            })


    def on_receive(self, msg):
        if msg.get('request', None):
            return self.handle(msg['request'])

    def handle(self, request):
        if request.get('type') == 'event':
            self.trigger(request.get('register_addr'), request.get('data', None))
        elif request.get('type') == 'fail':
            #import pudb; pu.db
            self.trigger('fail', [self, None])


    def stopall(self):
        if not self.inited:
            return
        self.subs = None
        self.hal_device.stop()
        resp = self.hal.request({
            'type': 'system',
            'cmd': 'destroy',
            'kwargs': {
                'device_addr': self.device_addr,
            }
        }).get()
        #FIXME: почему в ответ приходит None
        #if resp['status'] == 'fail':
            #log.info(resp['info'])

        self.my_responder.stop()

    def on_stop(self):
        log.debug(u'Stop ModbusDevice: {0}'.format(self.device_addr))
        self.stopall()

    def on_failure(self, exception_type, exception_value, traceback):
        #import pudb; pu.db
        log.debug(u"ModbusDevice %s failed: %s: %s" % (self.device_addr, exception_type, exception_value))
        self.stopall()


def merge_regs(*args):
    merged_reg = dict()
    for reg in args:
        for addr, key in reg.iteritems():
            merged_reg[addr] = key

    return merged_reg


class StekloliftDevice(ModbusDevice):
    _REG =  {
        'stat': merge_regs(ModbusDevice._REG['stat'], {
            '0xC000': 'sensor_1',
            '0xC001': 'sensor_2',
            '0xC002': 'stop_time',
            '0xC003': 'shim',
            '0xC004': 'current',
            '0xC005': 'stop_reason',
            '0xC006': 'lock',
            '0xC007': 'temperature',
        }),
        'conf': merge_regs(ModbusDevice._REG['stat'], {
            '0xE000': 'direction',
            '0xE001': 'profile_number',
            '0xE002': 'sensor_1',
            '0xE003': 'sensor_2',
            '0xE004': 'overload_restriction',
            '0xE005': 'max_timeout',
            '0xE040': 'profiles',
        })
    }
    _CMD = merge_regs(ModbusDevice._CMD, {
        '0x8000': 'start',
        '0x8001': 'stop',
        '0x8002': 'lock_on',
        '0x8003': 'lock_off',
    })

    device_type = "0x534C" # SL - Steklo Lift


    def __init__(self, device_addr, hal=None):
        super(StekloliftDevice, self).__init__(device_addr, hal)
        self.REG = dict()
        self.REG['stat'] = { key: addr for addr, key in self._REG['stat'].iteritems() }
        self.REG['conf'] = { key: addr for addr, key in self._REG['conf'].iteritems() }
        self.CMD = { key: cmd for cmd, key in self._CMD.iteritems() }

    def set_profile(self, number, data):
        # data = [
        #   shex(..) # время действия сегмента
        #   shex(..) # начальная скорость
        #   shex(..) # конечная скрорость
        #
        #   ...
        # ]

        log.info(u'Set profile №{0}'.format(number))
        pprint(data)


        if len(data) % 3:
            log.warning(u'Malformed profile! {0} is not multipe of 3'.format(len(data)))
            data = data[:-(len(data)%3)]
        segments_number = shex(len(data)/3)

        profiles_start = self.reg('conf.profiles')
        profile = int(profiles_start, 16) + number*int('0x40', 16)
        self.mb_write(hex(profile), [segments_number] + data)

    def get_profile(self, number):
        # data = [
        #   shex(..) # время действия сегмента
        #   shex(..) # начальная скорость
        #   shex(..) # конечная скрорость
        #
        #   ...
        # ]

        log.info(u'Get profile №{0}'.format(number))

        #if len(data) % 3:
            #log.warning('Malformed profile! {0} is not multipe of 3'.format(len(data)))
            #data = data[:-(len(data)%3)]
        #segments_number = shex(len(data)/3)
        #FIXME: почему-то не работет

        profiles_start = self.reg('conf.profiles')
        profile = int(profiles_start, 16) + number*int('0x40', 16)
        return self.mb_read(hex(profile), int('0x40', 16))



class DummyDevice(ModbusDevice):
    _REG =  {
        'stat': merge_regs(ModbusDevice._REG['stat'], {
            '0xC000': 'inputs',

            '0xC001': 'port0_input',
            '0xC002': 'port0_count',
            '0xC003': 'port0_shim',
            '0xC004': 'port0_lasttime',

            '0xC005': 'port1_input',
            '0xC006': 'port1_count',
            '0xC007': 'port1_shim',
            '0xC008': 'port1_lasttime',

            '0xC009': 'port2_input',
            '0xC00A': 'port2_count',
            '0xC00B': 'port2_shim',
            '0xC00C': 'port2_lasttime',

            '0xC00D': 'port3_input',
            '0xC00E': 'port3_count',
            '0xC00F': 'port3_shim',
            '0xC010': 'port3_lasttime',

            '0xC011': 'port4_input',
            '0xC012': 'port4_count',
            '0xC013': 'port4_shim',
            '0xC014': 'port4_lasttime',

            '0xC015': 'port5_input',
            '0xC016': 'port5_count',
            '0xC017': 'port5_shim',
            '0xC018': 'port5_lasttime',

            '0xC019': 'port6_input',
            '0xC01A': 'port6_count',
            '0xC01B': 'port6_shim',
            '0xC01C': 'port6_lasttime',

            '0xC01D': 'port7_input',
            '0xC01E': 'port7_count',
            '0xC01F': 'port7_shim',
            '0xC020': 'port7_lasttime',
        }),
        'conf': merge_regs(ModbusDevice._REG['conf'], {
            '0xE000': 'direction',
                        #shex(...) направление портов
                        #   0 - 0 на вход
                        #   1 - 1 на выход
            '0xE001': 'port0_type',
                        #shex(...)
                        #   0 - на выход
                        #   1 - на ШИМ`
            '0xE002': 'port0_output',
            '0xE003': 'port0_time',
                        #shex(...)
                        #   value*0.1c
            '0xE004': 'port0_alg',
                        #shex(...)
                        #   0 - лиш
                        #   1 - лог

            '0xE005': 'port1_type',
            '0xE006': 'port1_output',
            '0xE007': 'port1_time',
            '0xE008': 'port1_alg',

            '0xE009': 'port2_type',
            '0xE00A': 'port2_output',
            '0xE00B': 'port2_time',
            '0xE00C': 'port2_alg',

            '0xE00D': 'port3_type',
            '0xE00E': 'port3_output',
            '0xE00F': 'port3_time',
            '0xE010': 'port3_alg',

            '0xE011': 'port4_type',
            '0xE012': 'port4_output',
            '0xE013': 'port4_time',
            '0xE014': 'port4_alg',

            '0xE015': 'port5_type',
            '0xE016': 'port5_output',
            '0xE017': 'port5_time',
            '0xE018': 'port5_alg',

            '0xE019': 'port6_type',
            '0xE01A': 'port6_output',
            '0xE01B': 'port6_time',
            '0xE01C': 'port6_alg',

            '0xE01D': 'port7_type',
            '0xE01E': 'port7_output',
            '0xE01F': 'port7_time',
            '0xE020': 'port7_alg',
        })
    }
    _CMD = merge_regs(ModbusDevice._CMD, {
        '0x8000': 'all_shim_start',
        '0x8001': 'all_shim_stop',

        '0x8002': 'port0_shim_start',
        '0x8003': 'port0_shim_stop',
        '0x8004': 'port1_shim_start',
        '0x8005': 'port1_shim_stop',
        '0x8006': 'port2_shim_start',
        '0x8007': 'port2_shim_stop',
        '0x8008': 'port3_shim_start',
        '0x8009': 'port3_shim_stop',
        '0x800A': 'port4_shim_start',
        '0x800B': 'port4_shim_stop',
        '0x800C': 'port5_shim_start',
        '0x800D': 'port5_shim_stop',
        '0x800E': 'port6_shim_start',
        '0x800F': 'port6_shim_stop',
        '0x8010': 'port7_shim_start',
        '0x8011': 'port7_shim_stop',

    })

    device_type = "0x444D" # DM - Dummy Module


    def __init__(self, device_addr, hal=None):
        super(DummyDevice, self).__init__(device_addr, hal)
        self.REG = dict()
        self.REG['stat'] = { key: addr for addr, key in self._REG['stat'].iteritems() }
        self.REG['conf'] = { key: addr for addr, key in self._REG['conf'].iteritems() }
        self.CMD = { key: cmd for cmd, key in self._CMD.iteritems() }

    def port_reg(self, number):
        ports_start = self.reg('conf.port0_type')
        return int(ports_start, 16) + number*int('0x04', 16)

    def as_input(self):
        self.write_regs('conf.direction', [shex(0)])

    def as_output(self):
        self.write_regs('conf.direction', [shex(1)])

    def set_port(self, number, data):
        # data = [
            #shex(...) - тип выхода
            #   0 - на выход
            #   1 - на ШИМ`
            #shex(...) - значение на выходе
            #shex(...) - время изменения
            #   value*0.1c
            #shex(...) - закон изменения
            #   0 - лиш
            #   1 - лог
        # ]

        log.info(u'Set port №{0}. I/O: {1}. Output: {2}. DvAddr: {3}'.format(number, data[0], data[1], self.device_addr))
        port = self.port_reg(number)
        self.mb_write(hex(port), data)

    #def set_step(self, number, data):
        ## data = [
        ##   shex(..) # количество шагов за один поворот
        ##   shex(..) # вреся одного поворота(0.1c)
        ##   shex(..) # направление поврота
        ##
        ##   ...
        ## ]

        #log.info(u'Set stepper'.format(number))

        #steps_start = self.reg('conf.steps')
        #step = int(steps_start, 16) + number*int('0x03', 16)
        #self.mb_write(hex(port), data)


class KluchDevice(ModbusDevice):
    _REG =  {
        'stat': merge_regs(ModbusDevice._REG['stat'], {
            '0xC000': 'ch1_period',
            '0xC001': 'ch2_period',
            '0xC002': 'ch3_period',
            '0xC003': 'ch4_period',
            '0xC004': 'ch5_period',
            '0xC005': 'ch6_period',
            '0xC006': 'ch7_period',
            '0xC007': 'ch8_period',
            '0xC008': 'ch1_stat',
            '0xC009': 'ch2_stat',
            '0xC00A': 'ch3_stat',
            '0xC00B': 'ch4_stat',
            '0xC00C': 'ch5_stat',
            '0xC00D': 'ch6_stat',
            '0xC00E': 'ch7_stat',
            '0xC00F': 'ch8_stat',
            '0xC010': 'all_stat',
        }),
        'conf': merge_regs(ModbusDevice._REG['stat'], {
            '0xE000': 'ch1_period',
            '0xE001': 'ch2_period',
            '0xE002': 'ch3_period',
            '0xE003': 'ch4_period',
            '0xE004': 'ch5_period',
            '0xE005': 'ch6_period',
            '0xE006': 'ch7_period',
            '0xE007': 'ch8_period',
            '0xE008': 'svet',
        })
    }
    _CMD = merge_regs(ModbusDevice._CMD, {})

    device_type = "0x494D" # IM - Induction Measurment


    def __init__(self, device_addr, hal=None):
        super(KluchDevice, self).__init__(device_addr, hal)
        self.REG = dict()
        self.REG['stat'] = { key: addr for addr, key in self._REG['stat'].iteritems() }
        self.REG['conf'] = { key: addr for addr, key in self._REG['conf'].iteritems() }
        self.CMD = { key: cmd for cmd, key in self._CMD.iteritems() }

    def set_channel(self, number, period ):
        assert 20 <= period <= 160, "period must be between 20 and 160"

        log.info(u'Set channel №{0}: {1}'.format(number, period))
        self.write_regs('conf.ch{0}_period' % number, [shex(period),])

    def svet(self, svet=True):
        log.info(u'Svet is: {0}'.format(svet))
        self.write_regs('conf.svet', [shex(svet),])


class ZvezdyDevice(ModbusDevice):
    _REG =  {
        'stat': merge_regs(ModbusDevice._REG['stat'], {
            '0xC000': 'sensor_1', #1 - датчик автивирован
            '0xC001': 'sensor_2',
            '0xC002': 'sensor_3',
            '0xC003': 'stage',  # 0 - игра не активны
                                # 1 - игра активна
                                # 2 - игра завершена
        }),
        'conf': merge_regs(ModbusDevice._REG['stat'], {
            '0xE000': 'god_1_sveto',
            '0xE001': 'god_2_sveto',
            '0xE002': 'god_3_sveto',
            '0xE003': 'sozv_1_sveto',
            '0xE004': 'sozv_2_sveto',
            '0xE005': 'sozv_3_sveto',
            '0xE006': 'time_sveto_on',
            '0xE007': 'time_sveto_change',
            '0xE008': 'time_sveto_off',
            '0xE009': 'god_color_r',
            '0xE00A': 'god_color_g',
            '0xE00B': 'god_color_b',
            '0xE00C': 'sozv_color_r',
            '0xE00D': 'sozv_color_g',
            '0xE00E': 'sozv_color_b',
        })
    }
    _CMD = merge_regs(ModbusDevice._CMD, {
        '0x8000': 'off',
        '0x8001': 'start',
        '0x8002': 'finish',
    })
    device_type = "0x5A56" # ZV - ZVezdy


    def __init__(self, device_addr, hal=None):
        super(ZvezdyDevice, self).__init__(device_addr, hal)
        self.REG = dict()
        self.REG['stat'] = { key: addr for addr, key in self._REG['stat'].iteritems() }
        self.REG['conf'] = { key: addr for addr, key in self._REG['conf'].iteritems() }
        self.CMD = { key: cmd for cmd, key in self._CMD.iteritems() }


class PutDevice(ModbusDevice):
    #TODO registers
    _REG =  {
        'stat': merge_regs(ModbusDevice._REG['stat'], {
            '0xC001': 'solved',
        }),
        'conf': merge_regs(ModbusDevice._REG['stat'], {
        })
    }
    _CMD = merge_regs(ModbusDevice._CMD, {
        '0x8000': 'off',
        '0x8001': 'reset',
        '0x8002': 'setup',
        '0x8003': 'start',
    })
    device_type = "0x5054" # ZV - ZVezdy


    def __init__(self, device_addr, hal=None):
        super(PutDevice, self).__init__(device_addr, hal)
        self.REG = dict()
        self.REG['stat'] = { key: addr for addr, key in self._REG['stat'].iteritems() }
        self.REG['conf'] = { key: addr for addr, key in self._REG['conf'].iteritems() }
        self.CMD = { key: cmd for cmd, key in self._CMD.iteritems() }



class Puzzle(Actor, Events):
    glossary_name = None
    glossary_code = None

    def __init__(self, *args, **kwargs):
        super(Puzzle, self).__init__(*args, **kwargs)
        Events.__init__(self)
        self.PUB = Resources().require('PUB')

    def init(self):
        pass

    def activate(self):
        pass

    def proxy(self):
        return self.actor_ref.proxy()

    def stopall(self):
        pass

    def on_stop(self):
        log.debug(u"Stop Puzzle %s" % self)
        self.stopall()

    def on_failure(self, exception_type, exception_value, traceback):
        #import pudb; pu.db
        log.debug(u"Puzzle %s failed: %s: %s" % (self, exception_type, exception_value))
        self.stopall()

    def __str__(self):
        return u"%s %s" % (unicode(self.glossary_code), unicode(self.glossary_name))


# обработчик запросов от клиентов
# вероятно будет содержать ссылку на игру
#TODO
class Handler(Actor):
    def __init__(self, status):
        super(Handler, self).__init__()
        self.status = status
        self.GAME = Resources().require('game')
        self.game_started = None

    def on_receive(self, msg):
        if msg.get('request', None):
            return self.handle(msg['request'])

    def handle(self, request):
        print "Handle request: {0}".format(request)
        if request.get("type", None) == "cmd":
            func_name = request.get("cmd").replace('::', '_')
            func = getattr(self, func_name, None)
            if not func:
                print "Error! No such func"
                return {
                    "status": "fail",
                    "time": str(datetime.now())
                }

            data = request.get('data', None)
            result = None
            if data:
                result = func(data)
            else:
                result = func()

        return {
            "status": "success",
            "time": str(datetime.now()),
            "result": result

        }

    def system_ping(self):
        return 'pong'

    def game_init(self):
        #print "game_init"
        self.game_started = True
        steps = self.GAME.game_init().get()
        result = []
        for step in steps:
            result.append({
                'id': step.step.get(),
                'name': unicode(step.name.get())
            })

        return result

    def game_prepare_new(self):
        steps = self.GAME.prepare_new_game().get()
        result = []
        for step in steps:
            result.append({
                'id': step.step.get(),
                'name': unicode(step.name.get())
            })
        return result

    def game_start(self):
        steps = self.GAME.game_start().get()
        result = []
        for step in steps:
            result.append({
                'id': step.step.get(),
                'name': unicode(step.name.get())
            })
        return result

    def game_start_simple(self):
        #import pudb; pu.db
        steps = self.GAME.game_start_simple().get()
        result = []
        #import pudb; pu.db
        for step in steps:
            #import pudb; pu.db
            #print step.step.get()
            #print step.name.get()
            result.append({
                'id': step.step.get(),
                'name': unicode(step.name.get())
            })
        #import pudb; pu.db
        return result

    def game_repeat_step(self):
        self.GAME.repeat_step()

    def game_next_step(self):
        self.GAME.next_step(debounce=True)

    def game_open_close_vhod(self):
        self.GAME.open_close_vhod()

    def game_open_all(self):
        self.GAME.open_all()

    def game_control(self, data):
        self.GAME.control(data)

    def game_emerge_stop(self):
        self.GAME.emerge_stop()


    def game_stop(self):
        #print "game_stop"
        if self.game_started:
            self.GAME.stop_game()
            self.status['stopped'] = True
            #print "status stopped"
            print self.status['stopped']

def sigint_stop(status):
    status['stopped'] = True

def shutdown():
    g = gevent.spawn_later(5, lambda: zmq_ctx.destroy())
    pykka.ActorRegistry.stop_all(True)
    g.kill()
    print "Termination of zmq context..."
    zmq_ctx.destroy()
    sys.exit(0)


def run_game(game):
    try:
        Resources().register('scheduler', Scheduler())
        scheduler = Resources().require('scheduler')

        address = "tcp://{ip}:{port}".format(**ADRS['VHAL'])
        Resources().register('HAL.default_modbus', Requester.start(address).proxy())
        HAL = Resources().require('HAL.default_modbus')
        HAL.request({
            'type': 'system',
            'cmd': 'config',
            'kwargs': {
                'engine_ip': ADRS['ENG']['ip'],
            }
        })
        address = "tcp://{ip}:{port}".format(**ADRS['CLI_PUB'])
        PUB = Publisher.start(address).proxy()
        Resources().register('PUB', PUB)
        Resources().register('game', game.start().proxy())
        status = {"stopped": False}
        handler = Handler.start(status)
        address = "tcp://{ip}:{port}".format(**ADRS['CLI_SRV'])
        srv = Responder.start(address, handler).proxy()

        print 'Registering signal handler...'
        gevent.signal(signal.SIGINT, sigint_stop, status)
        gevent.signal(signal.SIGTERM, shutdown)
        while not status.get('stopped'):
            gevent.sleep(0.05)
            #print pykka.ActorRegistry.get_all()

        #TODO: gevent_spawn который через 5 сек вызывает zmq_ctx.destroy
        # чтобы программа не блочилась

        HAL = Resources().require('HAL.default_modbus')
        HAL.request({'type': 'system', 'cmd': 'shutdown'})
        HAL.stop()

        PUB.log("Game stopping.")
        PUB.stop()
        srv.stop()
        handler.stop()

        Resources().require('game').stop()

    except (KeyboardInterrupt, SystemExit):
        print "System interrupt..."
    finally:
        try:
            HAL = Resources().require('HAL.default_modbus')
            HAL.request({'type': 'system', 'cmd': 'shutdown'})
            HAL.stop()

            PUB.log("Game stopping.")
            PUB.stop()
            srv.stop()
            handler.stop()

            Resources().require('game').stop()
        finally:
            g = gevent.spawn_later(5, lambda: zmq_ctx.destroy())
            pykka.ActorRegistry.stop_all(True)
            g.kill()
            zmq_ctx.destroy()
            print "Termination of zmq context..."
            zmq_ctx.destroy()


# основной скрипт, вынести в отдельный файл
if __name__ == '__main__':
    run_game(Game)




