# encoding: utf-8
import socket
#host = socket.gethostbyname
def host(*args):
    return '127.0.0.1'

ADRS = {
    'VBAL': {
        'ip': host('qg_bal_modbus'),
        'port': '25000'
    },
    'VHAL': {
        'ip': host('qg_hal'),
        'port': '20000'
    },
    'LOGS': {
        'ip': host('qg_logger'),
        'port': '25001'
    }
}
